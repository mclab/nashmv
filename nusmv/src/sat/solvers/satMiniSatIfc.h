/**************************************************************************************************

Solver_C.h

C-wrapper for Solver.h

**************************************************************************************************/

#ifndef SOLVER_C_h
#define SOLVER_C_h

//=================================================================================================
// Solver -- the main class:

#define MiniSat_ptr void *

MiniSat_ptr MiniSat_Create();
void MiniSat_Delete(MiniSat_ptr);
int MiniSat_Nof_Variables(MiniSat_ptr);
int MiniSat_Nof_Clauses(MiniSat_ptr);
int MiniSat_New_Variable(MiniSat_ptr);
int MiniSat_Add_Clause(MiniSat_ptr, int *clause_lits, int num_lits);
int MiniSat_Solve(MiniSat_ptr);
int MiniSat_Solve_Assume(MiniSat_ptr, int nof_assumed_lits, int *assumed_lits);
int MiniSat_simplifyDB(MiniSat_ptr);
int MiniSat_Get_Value(MiniSat_ptr, int var_num);


//=================================================================================================
#endif
