/**CFile**********************************************************************

  FileName    [agentsReadComments.c]

  PackageName [agents]

  Synopsis    [Routines to manage comments on the NuSMV.bar input file.]

  Description [Routines to manage comments on the NuSMV.bar input file.]

  Author      [Igor Melatti, Federico Mari]

  Copyright   []

******************************************************************************/

#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include "agentsReadComments.h"
#include "util.h"
#include "utils/ustring.h" 
#include "opt/opt.h"
#include "parser/symbols.h"
#include "node/node.h"

/*---------------------------------------------------------------------------*/
/* Constant declarations                                                     */
/*---------------------------------------------------------------------------*/


/*---------------------------------------------------------------------------*/
/* Type declarations                                                         */
/*---------------------------------------------------------------------------*/


/*---------------------------------------------------------------------------*/
/* Structure declarations                                                    */
/*---------------------------------------------------------------------------*/


/*---------------------------------------------------------------------------*/
/* Variable declarations                                                     */
/*---------------------------------------------------------------------------*/
EXTERN FILE *nusmv_stderr;
static all_agents_info *agents_info;


/*---------------------------------------------------------------------------*/
/* Macro declarations                                                        */
/*---------------------------------------------------------------------------*/
#ifndef BUFFER_LENGTH 
#define BUFFER_LENGTH 1024
#endif
#define AGENTS_COMMENTS_IDE "-- COMMENT FOR AGENTS: "
#define AGENTS_COMMENTS_SEP_AGENT ": " /* separates the agent identificator from the list of variables */
#define AGENTS_COMMENTS_VARS_SEP ','
#define AGENTS_COMMENTS_VARS_TERM '\n'
#define AGENTS_COMMENTS_ACTIONS_IDE "ACTIONS VECTOR" /* cannot be in the same line of an agent vars comment or a costs var comment */
#define AGENTS_COMMENTS_BYZANTINE_IDE "BYZANTINE VECTOR" /* cannot be in the same line of an agent vars comment or a costs var comment */
#define AGENTS_COMMENTS_COALITION_IDE "COALITION VECTOR" /* cannot be in the same line of an agent vars comment or a costs var comment */
#define AGENTS_COMMENTS_COSTS_IDE "COSTS VECTOR" /* cannot be in the same line of an agent comment or a byzantine var comment */
#define AGENTS_COMMENTS_COAL_COST_IDE "COST" /* must be in the initial comment */
#define AGENTS_NUMBER "AGENTS NUMBER"

/**AutomaticStart*************************************************************/

/*---------------------------------------------------------------------------*/
/* Static function prototypes                                                */
/*---------------------------------------------------------------------------*/
static agent_var_list head_insert_agent_var_list ARGS((agent_var_list L, char *var_name, unsigned agent));
static indexes_list head_insert_indexes_list ARGS((indexes_list L, int index));
static char *read_var_from_string ARGS((char *from, char *to));
static all_agents_info *read_comments ARGS((char *filename, boolean without_coal));
static int get_agent_from_variable ARGS((all_agents_info *agents_info, char *variable_name));
static int get_agent_from_variable_h ARGS((all_agents_info *agents_info, char *variable_name));
static int get_agent_from_variable_b ARGS((all_agents_info *agents_info, char *variable_name));
static int get_agent_from_variable_q ARGS((all_agents_info *agents_info, char *variable_name));
static int get_agent_from_variable_a ARGS((all_agents_info *agents_info, char *variable_name));
static void get_string_from_node(node_ptr name, char *to_be_cfr);

/**AutomaticEnd***************************************************************/

/*---------------------------------------------------------------------------*/
/* Definition of exported functions                                          */
/*---------------------------------------------------------------------------*/

/**Function********************************************************************

  Synopsis           [Inserts info about agents variables in the global 
  structure agents_info.]

  Description        [Inserts info about agents variables in the global 
  structure agents_info. Does not consider "normal" variables, i.e. it only 
  handles payoff, action and byzantine variables.]

  SideEffects        [agents_info]

  SeeAlso            []

******************************************************************************/
int Agents_insert_variable_index_from_node(node_ptr name, a_qbh which, options_ptr options, int curr_index, int next_index)
{
  char to_be_cfr[1024];
  int agent_index;
  all_agents_info *agents_info;

  agents_info = Agents_get_agents_info(options);

  get_string_from_node(name, to_be_cfr);
  switch (which) {
    case handle_a:
      if ((agent_index = get_agent_from_variable_a(agents_info, to_be_cfr)) != -1)
	agents_info->action_indexes[agent_index] = head_insert_indexes_list(agents_info->action_indexes[agent_index], curr_index);
      break;
    case handle_qbh:
      if ((agent_index = get_agent_from_variable_b(agents_info, to_be_cfr)) != -1) {
        agents_info->byz_indexes[2*agent_index] = curr_index;
        agents_info->byz_indexes[2*agent_index + 1] = next_index;
      }
      else if ((agent_index = get_agent_from_variable_q(agents_info, to_be_cfr)) != -1) {
        agents_info->coal_indexes[2*agent_index] = curr_index;
        agents_info->coal_indexes[2*agent_index + 1] = next_index;
      }
      else if ((agent_index = get_agent_from_variable_h(agents_info, to_be_cfr)) != -1) {
        if (agents_info->h_indexes) { /* i.e., not coalition mode */
	  agents_info->h_indexes[agent_index] = head_insert_indexes_list(agents_info->h_indexes[agent_index], next_index);
	  agents_info->h_indexes[agent_index] = head_insert_indexes_list(agents_info->h_indexes[agent_index], curr_index);
	}
        else {
	  agents_info->h_coal_indexes = head_insert_indexes_list(agents_info->h_coal_indexes, next_index);
	  agents_info->h_coal_indexes = head_insert_indexes_list(agents_info->h_coal_indexes, curr_index);
        }
      }
      break;
    default:
      fprintf(nusmv_stderr, "Internal error: which is %d\n", which);
      nusmv_exit(1);
  }
}

/**Function********************************************************************

  Synopsis           [Retrieves from the global structure agents_info the index
  of the agent owning the given payoff variable]

  Description        []

  SideEffects        []

  SeeAlso            []

******************************************************************************/
int Agents_get_agent_from_variable_node_h(node_ptr expr, options_ptr options)
{
  char to_be_cfr[1024];

  get_string_from_node(expr, to_be_cfr);
  return get_agent_from_variable_h(Agents_get_agents_info(options), to_be_cfr);
}

/**Function********************************************************************

  Synopsis           [Retrieves from the global structure agents_info the index
  of the agent owning the given variable (does not consider action variables).]

  Description        []

  SideEffects        []

  SeeAlso            []

******************************************************************************/
char Agents_get_agent_from_variable_node(node_ptr expr, options_ptr options, int *index)
{
  char to_be_cfr[1024];

  get_string_from_node(expr, to_be_cfr);

  *index = get_agent_from_variable(Agents_get_agents_info(options), to_be_cfr);
  if (*index >= 0)
    return 'a';
  
  *index = get_agent_from_variable_h(Agents_get_agents_info(options), to_be_cfr);
  if (*index >= 0)
    return 'h';
  
  *index = get_agent_from_variable_b(Agents_get_agents_info(options), to_be_cfr);
  if (*index >= 0)
    return 'b';
  
  *index = get_agent_from_variable_q(Agents_get_agents_info(options), to_be_cfr);
  if (*index >= 0)
    return 'q';
  
  fprintf(nusmv_stderr, 
    "Variable %s is neither an agent variable, nor a cost variable, nor a byzantine variable, nor a coalition variable!\n", 
    to_be_cfr);
  return 'n';
}

/**Function********************************************************************

  Synopsis           []

  Description        []

  SideEffects        []

  SeeAlso            []

******************************************************************************/
void Agents_set_null_agents_info()
{
  agents_info = (all_agents_info *)NULL;
}

/**Function********************************************************************

  Synopsis           [Returns the global structure agents_info; if it is not
  initialized, function read_comments is invoked.]

  Description        []

  SideEffects        []

  SeeAlso            []

******************************************************************************/
all_agents_info *Agents_get_agents_info(options_ptr options)
{
  if (agents_info == (all_agents_info *)NULL) {
    if (get_input_file(options) == NULL)
      return NULL;
    else
      return (agents_info = read_comments(get_input_file(options), !opt_bar_agents_coal_ICBFT(options)));
  }
  else
    return agents_info;
}

/**Function********************************************************************

  Synopsis           [Returns the global structure agents_info.]

  Description        []

  SideEffects        []

  SeeAlso            []

******************************************************************************/
all_agents_info *Agents_only_get_agents_info(options_ptr options)
{
  return agents_info;
}

/**Function********************************************************************

  Synopsis           [Destructor for structure agents_info.]

  Description        []

  SideEffects        []

  SeeAlso            []

******************************************************************************/
void Agents_agents_info_destroy() 
{
  int i, j;
  agent_var_list L;
  indexes_list L2;
  
  if (agents_info == (all_agents_info *)NULL)
    return;
  for (i = 0; i < agents_info->num_agents + agents_info->num_glob_agents; i++) {
    FREE(agents_info->action_names[i]);
    FREE(agents_info->byz_names[i]);
    if (agents_info->h_names)
      FREE(agents_info->h_names[i]);
  }
  FREE(agents_info->byz_names);
  FREE(agents_info->byz_indexes);
  FREE(agents_info->action_names);
  FREE(agents_info->h_names);
  L = agents_info->L; 
  while (L) {
    agent_var_list tmp = L;
    L = L->next;
    FREE(tmp->var_name);
    FREE(tmp);
  }
  for (i = 0; i < 2; i++) {
    for (j = 0; j < agents_info->num_agents + agents_info->num_glob_agents; j++) {
      if (i) {
	if (agents_info->h_indexes)
	  L2 = agents_info->h_indexes[j]; 
	else if (j == 0)
	  L2 = agents_info->h_coal_indexes; 
	else
	  L2 = (indexes_list)NULL;
      }
      else
	L2 = agents_info->action_indexes[j]; 
      while (L2) {
	indexes_list tmp = L2;
	L2 = L2->next;
	FREE(tmp);
      }
    }
  }
}

void PutTheRealValue(node_ptr *n)
{
  nusmv_double n_d;
  nusmv_ptrint n_i;

  if (node_get_type((*n)) != NUMBER)
    error_not_proper_number("PutTheRealValue", *n);
  n_i = MY_NODE_TO_INT(car(*n));
  n_d = (nusmv_double)n_i;
  CPY_DOUBLE_INT(*n, n_d);
}

nusmv_double GetTheRealValue(node_ptr n)
{
  if (node_get_type(n) != NUMBER)
    error_not_proper_number("PutTheRealValue", n);
  return (nusmv_double)MY_NODE_TO_INT(car(n));
}

/*---------------------------------------------------------------------------*/
/* Definition of static functions                                            */
/*---------------------------------------------------------------------------*/

/**Function********************************************************************

  Synopsis           []

  Description        []

  SideEffects        []

  SeeAlso            []

******************************************************************************/
static agent_var_list head_insert_agent_var_list(agent_var_list L, char *var_name, unsigned agent) {
  agent_var_list ret = ALLOC(agent_var_list_node, 1);
  ret->var_name = var_name;
  ret->agent = agent;
  ret->next = L;
  return ret;
}

/**Function********************************************************************

  Synopsis           []

  Description        []

  SideEffects        []

  SeeAlso            []

******************************************************************************/
static indexes_list head_insert_indexes_list(indexes_list L, int index) {
  indexes_list ret = ALLOC(indexes_list_node, 1);
  ret->index = index;
  ret->next = L;
  return ret;
}

/**Function********************************************************************

  Synopsis           []

  Description        []

  SideEffects        []

  SeeAlso            []

******************************************************************************/
static char *read_var_from_string(char *from, char *to) {
  char *ret = ALLOC(char, to - from + 1);
  strncpy(ret, from, to - from);
  ret[to - from] = '\0';
  if (strchr(ret, '.') != 0) {
    fprintf(nusmv_stderr, "Variable %s is declared with a dot inside it\n", ret);
    nusmv_exit(1);
  }
  return ret;
}

/**Function********************************************************************

  Synopsis           []

  Description        []

  SideEffects        []

  SeeAlso            []

******************************************************************************/
static all_agents_info *read_comments(char *filename, boolean without_coal) {
  char buffer[BUFFER_LENGTH];
  FILE *stream = fopen(filename, "r");
  all_agents_info *ret = ALLOC(all_agents_info, 1);
  ret->L = NULL;
  ret->num_agents = 0;
  ret->num_glob_agents = 0;

  if (stream != NULL) {
    while (fgets(buffer, BUFFER_LENGTH, stream) != NULL) {
      char *tmp;
      
      assert(strlen(buffer) < BUFFER_LENGTH); /* BUFFER_LENGTH always enough to read one line */
      if ((tmp = strstr(buffer, AGENTS_COMMENTS_IDE)) != NULL) {
        char *type, *tmp2;
	unsigned agent;

        tmp += strlen(AGENTS_COMMENTS_IDE);
	sscanf(tmp, "%u", &agent);
	tmp = strstr(tmp, AGENTS_COMMENTS_SEP_AGENT) + strlen(AGENTS_COMMENTS_SEP_AGENT);
	if ((type = strstr(tmp, AGENTS_NUMBER)) != NULL) {	/* number of agents: 
								   WARNING, must be declared *before* byz and h comments */
	  ret->num_agents = agent;
	  type += strlen(AGENTS_NUMBER) + 1 + strlen(AGENTS_COMMENTS_SEP_AGENT);	/* 1 is a space */
	  sscanf(type, "%u", &(ret->num_glob_agents));
	  if (ret->num_glob_agents > 1 || ret->num_glob_agents < 0) {
	    fprintf(nusmv_stderr, "Declared %d global agents, must be 0 or 1\n", ret->num_glob_agents);
	    nusmv_exit(1);
	  }
	  if (without_coal) {
	    ret->h_names = ALLOC(char *, ret->num_agents + ret->num_glob_agents);
	    ret->h_indexes = ALLOC(indexes_list, ret->num_agents + ret->num_glob_agents);
	    memset(ret->h_indexes, 0, sizeof(indexes_list) * (ret->num_agents + ret->num_glob_agents));
	    ret->h_coal_name = (char *)NULL;
	    ret->h_coal_indexes = (indexes_list)NULL; /* useless */
	    ret->coal_names = (char **)NULL;
	    ret->coal_indexes = (int *)NULL;
	  }
	  else {
	    if ((tmp = strstr(type, AGENTS_COMMENTS_COAL_COST_IDE)) == NULL) {
	      fprintf(nusmv_stderr, "Cost variable for coalitions must be commented at the beginning\n");
	      nusmv_exit(1);
	    }
	    tmp += strlen(AGENTS_COMMENTS_COAL_COST_IDE) + 1;	/* 1 is a space */
	    tmp2 = strchr(tmp, AGENTS_COMMENTS_VARS_TERM);
	    ret->h_names = (char **)NULL;
	    ret->h_indexes = (indexes_list *)NULL;
	    ret->h_coal_name = read_var_from_string(tmp, tmp2);
	    ret->h_coal_indexes = (indexes_list)NULL;
	    ret->coal_names = ALLOC(char *, ret->num_agents + ret->num_glob_agents);
	    ret->coal_indexes = ALLOC(int, 2*(ret->num_agents + ret->num_glob_agents));
	  }
	  ret->byz_names = ALLOC(char *, ret->num_agents + ret->num_glob_agents);
	  ret->byz_indexes = ALLOC(int, 2*(ret->num_agents + ret->num_glob_agents));
	  memset(ret->byz_indexes, 0, sizeof(int) * (ret->num_agents + ret->num_glob_agents));
	  ret->action_names = ALLOC(char *, ret->num_agents + ret->num_glob_agents);
	  ret->action_indexes = ALLOC(indexes_list, ret->num_agents + ret->num_glob_agents);
	  memset(ret->action_indexes, 0, sizeof(indexes_list) * (ret->num_agents + ret->num_glob_agents));
	}

	else if ((type = strstr(tmp, AGENTS_COMMENTS_COSTS_IDE)) != NULL) {	/* comment for the costs variable */
	  if (!without_coal) {
	    fprintf(nusmv_stderr, "Can't have individual payoff functions in coalition mode\n", ret->num_glob_agents);
	    nusmv_exit(1);
	  }
	  assert(ret->num_agents != 0);
          tmp += strlen(AGENTS_COMMENTS_COSTS_IDE) + 1;	/* 1 is a space */
	  tmp2 = strchr(tmp, AGENTS_COMMENTS_VARS_TERM);
	  assert(tmp2 != NULL);
	  ret->h_names[agent - 1 + ret->num_glob_agents] = read_var_from_string(tmp, tmp2);
	}
	else if ((type = strstr(tmp, AGENTS_COMMENTS_BYZANTINE_IDE)) != NULL) {	/* comment for the byzantine variable */
	  assert(ret->num_agents != 0);
          tmp += strlen(AGENTS_COMMENTS_BYZANTINE_IDE) + 1;	/* 1 is a space */
	  tmp2 = strchr(tmp, AGENTS_COMMENTS_VARS_TERM);
	  assert(tmp2 != NULL);
	  ret->byz_names[agent - 1 + ret->num_glob_agents] = read_var_from_string(tmp, tmp2);
	}
	else if ((type = strstr(tmp, AGENTS_COMMENTS_COALITION_IDE)) != NULL) {	/* comment for the coalition variable */
	  assert(ret->num_agents != 0);
          tmp += strlen(AGENTS_COMMENTS_COALITION_IDE) + 1;	/* 1 is a space */
	  tmp2 = strchr(tmp, AGENTS_COMMENTS_VARS_TERM);
	  assert(tmp2 != NULL);
	  ret->coal_names[agent - 1 + ret->num_glob_agents] = read_var_from_string(tmp, tmp2);
	}
	else if ((type = strstr(tmp, AGENTS_COMMENTS_ACTIONS_IDE)) != NULL) {	/* comment for the actions variable */
	  assert(ret->num_agents != 0);
          tmp += strlen(AGENTS_COMMENTS_ACTIONS_IDE) + 1;	/* 1 is a space */
	  tmp2 = strchr(tmp, AGENTS_COMMENTS_VARS_TERM);
	  assert(tmp2 != NULL);
	  ret->action_names[agent - 1 + ret->num_glob_agents] = read_var_from_string(tmp, tmp2);
	}
	else {
	  while ((tmp2 = strchr(tmp, AGENTS_COMMENTS_VARS_SEP)) != NULL) {	/* comment for agents variables */
	    ret->L = head_insert_agent_var_list(ret->L, read_var_from_string(tmp, tmp2), agent - 1 + ret->num_glob_agents);
	    tmp = tmp2 + 2; /* always a space after ',' */
	  }
	  tmp2 = strchr(tmp, AGENTS_COMMENTS_VARS_TERM);
	  assert(tmp2 != NULL);
	  ret->L = head_insert_agent_var_list(ret->L, read_var_from_string(tmp, tmp2), agent - 1 + ret->num_glob_agents);
	}
      }
    }
    fclose(stream);
  }
  return ret;
}

/**Function********************************************************************

  Synopsis           [returns -1 if the variable was not found in agents variables]

  Description        []

  SideEffects        []

  SeeAlso            []

******************************************************************************/
static int get_agent_from_variable(all_agents_info *agents_info, char *variable_name)
{
  unsigned agent;
  agent_var_list ptr;
  static char actual_var_name[BUFFER_LENGTH];
  char *vp;
  char *avp;
  
  /* pre: the input file has no variables containing the '.' character */
  /* parse 'variable_name' until you reach a '.' or the end of the string */
  vp = variable_name;
  avp = actual_var_name;
  while ((*vp != '.') && (*vp != '\0'))
    *(avp++) = *(vp++);
  *avp = '\0';
  
  /* parse the list of variable names to find the corresponding agent */
  ptr = agents_info->L;
  while (ptr) {
    if (strcmp(actual_var_name, ptr->var_name) == 0)
      return ptr->agent;
    ptr = ptr->next;
  }
  
  return -1;
}

/**Function********************************************************************

  Synopsis           [returns -1 if the variable was not found in costs variables]

  Description        []

  SideEffects        []

  SeeAlso            []

******************************************************************************/
static int get_agent_from_variable_h(all_agents_info *agents_info, char *variable_name)
{
  unsigned agent;
  agent_var_list ptr;
  static char actual_var_name[BUFFER_LENGTH];
  char *vp;
  char *avp;
  int i;
  
  /* pre: the input file has no variables containing the '.' character */
  /* parse 'variable_name' until you reach a '.' or the end of the string */
  vp = variable_name;
  avp = actual_var_name;
  while ((*vp != '.') && (*vp != '\0'))
    *(avp++) = *(vp++);
  *avp = '\0';
  
  if (agents_info->h_names) {
    /* parse the list of variable names to find the corresponding agent */
    for (i = 0; i < agents_info->num_agents + agents_info->num_glob_agents; i++) {
      if (strcmp(actual_var_name, agents_info->h_names[i]) == 0)
	return i;
    }
  }
  else if (strcmp(actual_var_name, agents_info->h_coal_name) == 0)
    return 0;

  return -1;
}

/**Function********************************************************************

  Synopsis           [returns -1 if the variable was not found in byzantine variables]

  Description        []

  SideEffects        []

  SeeAlso            []

******************************************************************************/
static int get_agent_from_variable_b(all_agents_info *agents_info, char *variable_name)
{
  unsigned agent;
  agent_var_list ptr;
  static char actual_var_name[BUFFER_LENGTH];
  char *vp;
  char *avp;
  int i;
  
  /* pre: the input file has no variables containing the '.' character */
  /* parse 'variable_name' until you reach a '.' or the end of the string */
  vp = variable_name;
  avp = actual_var_name;
  while ((*vp != '.') && (*vp != '\0'))
    *(avp++) = *(vp++);
  *avp = '\0';
  
  /* parse the list of variable names to find the corresponding agent */
  for (i = 0; i < agents_info->num_agents + agents_info->num_glob_agents; i++) {
    if (strcmp(actual_var_name, agents_info->byz_names[i]) == 0)
      return i;
  }

  return -1;
}

/**Function********************************************************************

  Synopsis           [returns -1 if the variable was not found in coalition variables]

  Description        []

  SideEffects        []

  SeeAlso            []

******************************************************************************/
static int get_agent_from_variable_q(all_agents_info *agents_info, char *variable_name)
{
  unsigned agent;
  agent_var_list ptr;
  static char actual_var_name[BUFFER_LENGTH];
  char *vp;
  char *avp;
  int i;
  
  if (!agents_info->coal_names)
    return -1;

  /* pre: the input file has no variables containing the '.' character */
  /* parse 'variable_name' until you reach a '.' or the end of the string */
  vp = variable_name;
  avp = actual_var_name;
  while ((*vp != '.') && (*vp != '\0'))
    *(avp++) = *(vp++);
  *avp = '\0';
  
  /* parse the list of variable names to find the corresponding agent */
  for (i = 0; i < agents_info->num_agents + agents_info->num_glob_agents; i++) {
    if (strcmp(actual_var_name, agents_info->coal_names[i]) == 0)
      return i;
  }

  return -1;
}

/**Function********************************************************************

  Synopsis           [returns -1 if the variable was not found in action variables]

  Description        []

  SideEffects        []

  SeeAlso            []

******************************************************************************/
static int get_agent_from_variable_a(all_agents_info *agents_info, char *variable_name)
{
  unsigned agent;
  agent_var_list ptr;
  static char actual_var_name[BUFFER_LENGTH];
  char *vp;
  char *avp;
  int i;
  
  /* pre: the input file has no variables containing the '.' character */
  /* parse 'variable_name' until you reach a '.' or the end of the string */
  vp = variable_name;
  avp = actual_var_name;
  while ((*vp != '.') && (*vp != '\0'))
    *(avp++) = *(vp++);
  *avp = '\0';
  
  /* parse the list of variable names to find the corresponding agent */
  for (i = 0; i < agents_info->num_agents + agents_info->num_glob_agents; i++) {
    if (strcmp(actual_var_name, agents_info->action_names[i]) == 0)
      return i;
  }

  return -1;
}

/**Function********************************************************************

  Synopsis           []

  Description        []

  SideEffects        []

  SeeAlso            []

******************************************************************************/
static void get_string_from_node(node_ptr name, char *to_be_cfr)
{
  char *str;
  node_ptr node2, node3;

  for (node2 = name, node3 = node2; car(node2) != NULL; node3 = node2, node2 = car(node2));
  str = get_text((string_ptr)car(cdr(node2)));
  /* if it is an array, appends the index */
  if (node2 != node3 /* should be always true */ && node_get_type(node3) == ARRAY)
    sprintf(to_be_cfr, "%s[%d]", str, (long)car(cdr(node3)));
  else
    strcpy(to_be_cfr, str);
}

