/**CHeaderFile*****************************************************************

  FileName    [agentsUtils.c]

  PackageName [agents]

  Synopsis    [The public interface to the bar agents utilities]

  Description []

  SeeAlso     []

  Author      [Igor Melatti, Federico Mari]

  Copyright   []

  Revision    []

******************************************************************************/

#include "enc/bdd/BddEnc.h" 
#include "agentsUtils.h"
#include "agentsMechanism.h"
#include "enc/bdd/bddInt.h"
#include "enc/operators.h"
#include "prop/prop.h"
#include <math.h>
#define PLUS_INFTY (1e30)
#define MINUS_INFTY (-1e30)
#include "compile/symb_table/SymbTable.h"
#include "compile/compile.h"
#include "utils/NodeList.h"
#include "enc/enc.h"
#include "enc/bdd/BddEnc.h"
#include "cudd.h"
#include <assert.h>

/*---------------------------------------------------------------------------*/
/* Constant declarations                                                     */
/*---------------------------------------------------------------------------*/

/* ---------------------------------------------------------------------- */
/* You can define your own symbols for these: */
/* ---------------------------------------------------------------------- */

/*---------------------------------------------------------------------------*/
/* Type declarations                                                         */
/*---------------------------------------------------------------------------*/
typedef enum operation { MAXIMUM, MINIMUM } operation;

/*---------------------------------------------------------------------------*/
/* Structure declarations                                                    */
/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/* Variable declarations                                                     */
/*---------------------------------------------------------------------------*/
int glob_eval_as_double = 0;
int glob_eval_as_double_leaf = 0;

/*---------------------------------------------------------------------------*/
/* Macro declarations                                                        */
/*---------------------------------------------------------------------------*/


/**AutomaticStart*************************************************************/

/*---------------------------------------------------------------------------*/
/* Static function prototypes                                                */
/*---------------------------------------------------------------------------*/
static bdd_ptr cube_a_i ARGS((DdManager *dd, int i, all_agents_info *));
static bdd_ptr cube_a_minus_i ARGS((DdManager *dd, int i, int agents, all_agents_info *));
static bdd_ptr reuse_vars_and_return_cube_b ARGS((DdManager *dd, bdd_ptr *b, int agents, int to_be_jumped, all_agents_info *));
static bdd_ptr reuse_vars_and_return_cube_q ARGS((DdManager *dd, bdd_ptr *q, int dim, all_agents_info *agents_info));
static void create_vars ARGS((DdManager *dd, bdd_ptr *b, int agents, int to_be_jumped));
static void add_create_vars ARGS((DdManager *dd, add_ptr *a, int dim));
static bdd_ptr compute_N_i ARGS((bdd_ptr *b, Mechanism_ptr, int, bdd_ptr));
static bdd_ptr compute_N ARGS((bdd_ptr *b, bdd_ptr *q, Mechanism_ptr mech, bdd_ptr *BT));
static bdd_ptr compute_C_i ARGS((DdManager *, bdd_ptr *, int , int , int, int, bdd_ptr *, int, int, int));
static bdd_ptr compute_C ARGS((DdManager *dd, bdd_ptr *b, bdd_ptr *q, int max_byz, int max_coal, int agents, int num_bits_sum,
			       bdd_ptr *word_plus_bit, int index_w_from, int index_w_to, int b_local_index));
static bdd_ptr *bdd_word_plus_bit ARGS((DdManager *dd, int, int *, int *, int *));
static bdd_ptr bdd_equivalence ARGS((DdManager *dd, bdd_ptr v1, bdd_ptr v2));
static bdd_ptr bdd_word_int_leq ARGS((DdManager *dd, bdd_ptr *y, int m, int num_bits));
static void needed_bdds_adds ARGS((DdManager *, BddVarSet_ptr *, BddVarSet_ptr *, BddVarSet_ptr *, int *, int **, int **, int,
                                   bdd_ptr **, int *, int *, int *, int, bdd_ptr *, bdd_ptr *, add_ptr **, all_agents_info *));
static void needed_bdds_adds_coal ARGS((DdManager *dd, BddVarSet_ptr *presents_cube, BddVarSet_ptr *inputs_cube, BddVarSet_ptr *nexts_cube, 
					int *nexts_num, int *inputs_num, int *max_nexts_inputs_num, int **support_presents_cube_incr, 
					int **support_nexts_cube_incr, int agents, bdd_ptr **word_plus_bit, int *index_w_from, 
					int *index_w_to, int *b_local_index, int num_bits_sum, bdd_ptr *b_cube, bdd_ptr *q_cube, 
					bdd_ptr *b, bdd_ptr *q, add_ptr **fresh_vars, 
					bdd_ptr **action_fresh_vars, all_agents_info *agents_info));
static void compute_F_i ARGS((DdManager *dd, bdd_ptr N, bdd_ptr *F_i, BddVarSet_ptr next_cube, int, int *));
static void compute_F ARGS((DdManager *dd, bdd_ptr N, bdd_ptr *F_i, BddVarSet_ptr next_cube, int, int *));
static add_ptr compute_H_i ARGS((DdManager *dd, add_ptr h, add_ptr V, bdd_ptr *F_i, BddVarSet_ptr, int, int *, nusmv_double, bdd_ptr *));
static add_ptr compute_H ARGS((DdManager *dd, add_ptr h, add_ptr UV, bdd_ptr *F_i, BddVarSet_ptr next_cube, int nexts_num, 
			       int *support_presents_cube_incr, nusmv_double beta_coal, add_ptr *fresh_vars));
static nusmv_double compute_Delta_i ARGS((DdManager *dd, add_ptr V_i, add_ptr U_i, bdd_ptr, BddVarSet_ptr state_vars_cube, bdd_ptr b_cube));
static add_ptr compute_Delta ARGS((DdManager *dd, add_ptr V, add_ptr U, bdd_ptr I, BddVarSet_ptr state_vars_cube, bdd_ptr b_cube));
static nusmv_double compute_M_i ARGS((int i, Mechanism_ptr mech, BddVarSet_ptr state_vars_cube, BddVarSet_ptr input_vars_cube));
static add_ptr compute_M_coal ARGS((Mechanism_ptr mech, BddVarSet_ptr state_vars_cube, BddVarSet_ptr input_vars_cube));
static add_ptr add_max_min ARGS((DdManager *dd, bdd_ptr x_cube, add_ptr F, operation));
static add_ptr add_max_min_recur ARGS((DdManager *dd, bdd_ptr x_cube, add_ptr F, operation));
static add_ptr add_max_min_conditioned ARGS((DdManager *dd, bdd_ptr x_cube, bdd_ptr C, add_ptr F, operation which));
static add_ptr add_max_min_conditioned_recur_noenhanced ARGS((DdManager *dd, bdd_ptr x_cube, bdd_ptr C, add_ptr F, operation which));
static add_ptr add_max_min_together_conditioned ARGS((DdManager *dd, bdd_ptr x_M, bdd_ptr x_m, bdd_ptr C, add_ptr F));
static add_ptr add_max_min_together_conditioned_recur ARGS((DdManager *dd, bdd_ptr x_M, bdd_ptr x_m, bdd_ptr C, add_ptr F));
static void compute_G ARGS((DdManager *dd, add_ptr *G, add_ptr *q, bdd_ptr inputs_cube, bdd_ptr *inputs_tilde_cube, 
                            bdd_ptr *action_fresh_vars, all_agents_info *agents_info, int agents, int **support_inputs_cube_incr));
static bdd_ptr apply_G ARGS((DdManager *dd, bdd_ptr *G, bdd_ptr O, int inputs_num, int *support_inputs_cube_incr));
#if 1
static add_ptr apply_G_add ARGS((DdManager *dd, bdd_ptr *G, add_ptr A, int inputs_num, int *support_inputs_cube_incr));
#else
static add_ptr apply_G_add ARGS((DdManager *dd, bdd_ptr *G, add_ptr A, bdd_ptr *q, all_agents_info *agents_info, int agents, 
                                 int *support_inputs_cube_incr));
#endif
static bdd_ptr compute_D ARGS((DdManager *dd, bdd_ptr *q, int max_coal, int agents, int num_bits_sum,
                               bdd_ptr *word_plus_bit, int index_w_from, int index_w_to, int b_local_index));

/**AutomaticEnd***************************************************************/


/*---------------------------------------------------------------------------*/
/* Definition of exported functions                                          */
/*---------------------------------------------------------------------------*/


/**Function********************************************************************

  Synopsis           []

  Description        []

  SideEffects        []

  SeeAlso            []

******************************************************************************/
int CheckRationals(int max_byz, nusmv_double *beta, nusmv_double epsilon, nusmv_double *epsilon_1, nusmv_double *epsilon_2)
{
  int i, j, t, num_bits_sum, k_i, nexts_num, index_w_from, index_w_to, b_local_index;
  int *support_nexts_cube_incr, *support_presents_cube_incr;
  nusmv_double M_i, Delta_i;
  bdd_ptr tmp, tmp2, N_i, C_i, b_cube, NT_i, NB_i, exists_NB_i, exists_NT_i;
  BddVarSet_ptr nexts_cube, presents_cube, inputs_cube;
  bdd_ptr *FB_i, *word_plus_bit, *b;
  add_ptr HT_i, HB_i, V_i, U_i, tmp_add;
  add_ptr *fresh_vars;
  all_agents_info *agents_info;
  Mechanism_ptr mech;

  mech = Prop_master_get_mechanism();
  b = ALLOC(bdd_ptr, mech->agents);
  agents_info = Agents_get_agents_info(options);
  num_bits_sum = (int)floor(log(mech->agents)/log(2)) + 1; /* will only be used for \sum_{j = 0, j != i}^n b_i, which is at most n */
  needed_bdds_adds(mech->dd, &presents_cube, &inputs_cube, &nexts_cube, &nexts_num, 
                   &support_presents_cube_incr, &support_nexts_cube_incr, mech->agents,
		   &word_plus_bit, &index_w_from, &index_w_to, &b_local_index, num_bits_sum,
		   &b_cube, b, &fresh_vars, agents_info);
  if (opt_verbose_level_gt(options, 0))
    printf("Number of state bits: %d\nNumber of action bits: %d\nNumber of total bits: %d\n", 
           nexts_num*2, Cudd_SupportSize(mech->dd, inputs_cube), dd_get_size(mech->dd));
  FB_i = ALLOC(bdd_ptr, nexts_num);
  for (i = 0; i < mech->agents; i++) {
    bdd_ptr a_minus_i, a_i;

    M_i = compute_M_i(i, mech, presents_cube, inputs_cube);
    if (M_i == (nusmv_double)0) {
      epsilon_1[i] = (nusmv_double)0;
      epsilon_2[i] = (nusmv_double)0;
      continue;
    }
    a_minus_i = cube_a_minus_i(mech->dd, i, mech->agents, agents_info);
    a_i = cube_a_i(mech->dd, i, agents_info);
    NB_i = compute_N_i(b, mech, i, mech->B[i]);
    compute_F_i(mech->dd, NB_i, FB_i, nexts_cube, nexts_num, support_nexts_cube_incr);
    C_i = compute_C_i(mech->dd, b, max_byz, i, mech->agents, num_bits_sum, word_plus_bit, index_w_from, index_w_to, b_local_index);
    exists_NB_i = bdd_forsome(mech->dd, NB_i, nexts_cube);
    bdd_free(mech->dd, NB_i);
    bdd_and_accumulate(mech->dd, &exists_NB_i, C_i);
    NT_i = compute_N_i(b, mech, i, mech->T[i]);
    exists_NT_i = bdd_forsome(mech->dd, NT_i, nexts_cube);
    bdd_free(mech->dd, NT_i);
    bdd_and_accumulate(mech->dd, &exists_NT_i, C_i);
    k_i = (int)floor(log(epsilon*(1 - beta[i])/(20*M_i))/log(beta[i])) + 1;
    if (opt_verbose_level_gt(options, 0)) {
      printf("\n%d loops for agent %d\n", k_i, i + 1 - agents_info->num_glob_agents);
      PrintTop("before loop on t");
    }
    V_i = add_zero_double(mech->dd);
    U_i = add_zero_double(mech->dd);
    for (t = 0; t < k_i; t++) {
      if (opt_verbose_level_gt(options, 0))
	printf("\nIteration %d for agent %d\n", t, i + 1 - agents_info->num_glob_agents);
      HB_i = compute_H_i(mech->dd, mech->h[i], V_i, FB_i, nexts_cube, nexts_num, support_presents_cube_incr, beta[i], fresh_vars);
      add_free(mech->dd, V_i);
      if (opt_verbose_level_gt(options, 2)) {
	printf("Iteration %d on agent %d, HB_i is:\n", t, i + 1 - agents_info->num_glob_agents);
	cuddPdoubles(mech->dd, HB_i);
      }
      tmp_add = add_max_min_together_conditioned(mech->dd, a_i, a_minus_i, exists_NB_i, HB_i);
      add_free(mech->dd, HB_i);
      V_i = tmp_add;
      if (opt_verbose_level_gt(options, 1)) {
	printf("Iteration %d on agent %d, V_i is:\n", t, i + 1 - agents_info->num_glob_agents);
	cuddPdoubles(mech->dd, V_i);
      }
      HT_i = compute_H_i(mech->dd, mech->h[i], U_i, FB_i, nexts_cube, nexts_num, support_presents_cube_incr, beta[i], fresh_vars);
      add_free(mech->dd, U_i);
      if (opt_verbose_level_gt(options, 2)) {
	printf("Iteration %d on agent %d, HT_i is:\n", t, i + 1 - agents_info->num_glob_agents);
	cuddPdoubles(mech->dd, HT_i);
      }
      tmp_add = add_max_min_conditioned(mech->dd, inputs_cube, exists_NT_i, HT_i, MINIMUM);
      add_free(mech->dd, HT_i);
      U_i = tmp_add;
      if (opt_verbose_level_gt(options, 1)) {
	printf("Iteration %d on agent %d, U_i is:\n", t, i + 1 - agents_info->num_glob_agents);
	cuddPdoubles(mech->dd, U_i);
      }
    }
    bdd_free(mech->dd, exists_NT_i);
    bdd_free(mech->dd, exists_NB_i);
    for (j = 0; j < mech->agents; j++)
      bdd_free(mech->dd, FB_i[j]);
    bdd_free(mech->dd, a_minus_i);
    bdd_free(mech->dd, a_i);
    bdd_and_accumulate(mech->dd, &C_i, mech->I);
    Delta_i = compute_Delta_i(mech->dd, V_i, U_i, C_i, presents_cube, b_cube);
    bdd_free(mech->dd, C_i);
    add_free(mech->dd, V_i);
    add_free(mech->dd, U_i);
    epsilon_1[i] = Delta_i - 10*pow(beta[i], k_i)*M_i/(1 - beta[i]);
    epsilon_2[i] = Delta_i + 10*pow(beta[i], k_i)*M_i/(1 - beta[i]);
    if (opt_verbose_level_gt(options, 0))
      PrintTop("end of agent computation");
  }
  bdd_free(mech->dd, nexts_cube);
  bdd_free(mech->dd, presents_cube);
  bdd_free(mech->dd, inputs_cube);
  bdd_free(mech->dd, b_cube);
  FREE(FB_i);
  FREE(support_presents_cube_incr);
  FREE(support_nexts_cube_incr);
  for (i = 0; i < num_bits_sum; i++)
    bdd_free(mech->dd, word_plus_bit[i]);
  FREE(word_plus_bit);
  for (i = 0; i < nexts_num; i++)
    add_free(mech->dd, fresh_vars[i]);
  FREE(fresh_vars);
  FREE(b);
  return 0;
}

int CheckRationalsCoal(int max_byz, int max_coal, nusmv_double beta_coal, nusmv_double delta, 
                       nusmv_double epsilon, nusmv_double *res_double)
{
  int i, j, t, num_bits_sum, k, nexts_num, inputs_num, max_nexts_inputs_num, index_w_from, index_w_to, b_local_index;
  int *support_nexts_cube_incr, *support_presents_cube_incr, *support_inputs_cube_incr;
  bdd_ptr tmp, tmp2, N, C, D, b_cube, q_cube, NT, NB, NT_tilde, NB_tilde, exists_NB, exists_NT;
  BddVarSet_ptr nexts_cube, presents_cube, inputs_cube, inputs_tilde_cube, all_inputs_cube;
  bdd_ptr *FB, *word_plus_bit, *action_fresh_vars, *b, *q, *G;
  add_ptr HT, HB, V, U, tmp_add, tmp_add2, M_q, h_tilde, zeta, theta, Delta_q;
  add_ptr *fresh_vars;
  all_agents_info *agents_info;
  Mechanism_ptr mech;
  nusmv_ptrint M_q_int;
  nusmv_double M_q_double;

  mech = Prop_master_get_mechanism();
  b = ALLOC(bdd_ptr, mech->agents);
  q = ALLOC(bdd_ptr, mech->agents);
  agents_info = Agents_get_agents_info(options);
  num_bits_sum = (int)floor(log(mech->agents)/log(2)) + 1; /* will only be used for summation on agents, which results at most in n */
  needed_bdds_adds_coal(mech->dd, &presents_cube, &inputs_cube, &nexts_cube, &nexts_num, &inputs_num, &max_nexts_inputs_num,
			&support_presents_cube_incr, &support_nexts_cube_incr, mech->agents,
			&word_plus_bit, &index_w_from, &index_w_to, &b_local_index, num_bits_sum,
			&b_cube, &q_cube, b, q, &fresh_vars, &action_fresh_vars, agents_info);
  if (opt_verbose_level_gt(options, 0))
    printf("Number of state bits: %d\nNumber of action bits: %d\nNumber of total bits: %d\n", 
           nexts_num*2, Cudd_SupportSize(mech->dd, inputs_cube), dd_get_size(mech->dd));

  C = compute_C(mech->dd, b, q, max_byz, max_coal, mech->agents, num_bits_sum, word_plus_bit, index_w_from, index_w_to, b_local_index);

  G = ALLOC(bdd_ptr, inputs_num);
  compute_G(mech->dd, G, q, inputs_cube, &inputs_tilde_cube, action_fresh_vars, agents_info, mech->agents, &support_inputs_cube_incr);

  NB = compute_N(b, q, mech, mech->B);
  NB_tilde = apply_G(mech->dd, G, NB, inputs_num, support_inputs_cube_incr);
  if (opt_verbose_level_gt(options, 2)) {
    printf("NB_tilde is:\n");
    cuddP(mech->dd, NB_tilde);
  }
  bdd_free(mech->dd, NB);
  FB = ALLOC(bdd_ptr, nexts_num);
  compute_F(mech->dd, NB_tilde, FB, nexts_cube, nexts_num, support_nexts_cube_incr);
  FREE(support_nexts_cube_incr);
  exists_NB = bdd_forsome(mech->dd, NB_tilde, nexts_cube);
  bdd_free(mech->dd, NB_tilde);
  bdd_and_accumulate(mech->dd, &exists_NB, C);
  NT = compute_N(b, q, mech, mech->T);
  FREE(b);
  NT_tilde = apply_G(mech->dd, G, NT, inputs_num, support_inputs_cube_incr);
  bdd_free(mech->dd, NT);
  exists_NT = bdd_forsome(mech->dd, NT_tilde, nexts_cube);
  bdd_free(mech->dd, NT_tilde);
  bdd_and_accumulate(mech->dd, &exists_NT, C);

  M_q = compute_M_coal(mech, presents_cube, inputs_cube);
  if (add_isleaf(M_q)) {
    M_q_int = MY_NODE_TO_INT(add_get_leaf(mech->dd, M_q)); 
    CPY_DOUBLE_INT(M_q_double, M_q_int);
    if (M_q_double == (nusmv_double)0)
      return -1;
  }
  /* to maximize a log with a base < 1, you should minimize its argument, which is 1/M... */
  tmp_add = add_max_min(mech->dd, q_cube, M_q, MAXIMUM); 
  if (add_isleaf(tmp_add)) {
    nusmv_ptrint M_q_int = MY_NODE_TO_INT(add_get_leaf(mech->dd, tmp_add)); 
    CPY_DOUBLE_INT(M_q_double, M_q_int);
  }
  else
    return -1;
  k = (int)floor(log(delta*(1 - beta_coal)/(20*M_q_double))/log(beta_coal)) + 1;

  if (opt_verbose_level_gt(options, 2)) {
    printf("h_coal is:\n");
    cuddPdoubles(mech->dd, mech->h_coal);
  }
#if 1
  h_tilde = apply_G_add(mech->dd, G, mech->h_coal, inputs_num, support_inputs_cube_incr); 
#else
  h_tilde = apply_G_add(mech->dd, G, mech->h_coal, q, agents_info, mech->agents, support_inputs_cube_incr); 
#endif
  if (opt_verbose_level_gt(options, 2)) {
    printf("h_tilde is:\n");
    cuddPdoubles(mech->dd, h_tilde);
  }
  /* apply_G_add also bdd_to_adds and add_frees each G[i] */
  FREE(G);

  if (opt_verbose_level_gt(options, 0)) {
    printf("\n%d loops\n", k);
    PrintTop("before loop on t");
  }
  V = add_zero_double(mech->dd);
  U = add_zero_double(mech->dd);

  all_inputs_cube = bdd_and(mech->dd, inputs_cube, inputs_tilde_cube);

  for (t = 0; t < k; t++) {

    if (opt_verbose_level_gt(options, 0))
      printf("\nIteration %d\n", t);

    HB = compute_H(mech->dd, h_tilde, V, FB, nexts_cube, nexts_num, support_presents_cube_incr, beta_coal, fresh_vars);
    add_free(mech->dd, V);
    if (opt_verbose_level_gt(options, 2)) {
      printf("Iteration %d, HB is:\n", t);
      cuddPdoubles(mech->dd, HB);
    }
    tmp_add = add_max_min_together_conditioned(mech->dd, inputs_cube, inputs_tilde_cube, exists_NB, HB);
    add_free(mech->dd, HB);
    V = tmp_add;
    if (opt_verbose_level_gt(options, 1)) {
      printf("Iteration %d, V is:\n", t);
      cuddPdoubles(mech->dd, V);
    }

    HT = compute_H(mech->dd, h_tilde, U, FB, nexts_cube, nexts_num, support_presents_cube_incr, beta_coal, fresh_vars);
    add_free(mech->dd, U);
    if (opt_verbose_level_gt(options, 2)) {
      printf("Iteration %d, HT is:\n", t);
      cuddPdoubles(mech->dd, HT);
    }
    tmp_add = add_max_min_conditioned(mech->dd, all_inputs_cube, exists_NT, HT, MINIMUM);
    add_free(mech->dd, HT);
    U = tmp_add;
    if (opt_verbose_level_gt(options, 1)) {
      printf("Iteration %d, U is:\n", t);
      cuddPdoubles(mech->dd, U);
    }

  }
  
  if (opt_verbose_level_gt(options, 0))
    PrintTop("end of loop");

  FREE(support_presents_cube_incr);
  bdd_free(mech->dd, exists_NT);
  bdd_free(mech->dd, exists_NB);
  for (j = 0; j < mech->agents; j++)
    bdd_free(mech->dd, FB[j]);
  FREE(FB);
  for (i = 0; i < max_nexts_inputs_num; i++)
    add_free(mech->dd, fresh_vars[i]);
  FREE(fresh_vars);
  bdd_free(mech->dd, nexts_cube);
  bdd_free(mech->dd, inputs_cube);
  bdd_free(mech->dd, all_inputs_cube);
  bdd_and_accumulate(mech->dd, &C, mech->I);
  Delta_q = compute_Delta(mech->dd, V, U, C, presents_cube, b_cube);
  bdd_free(mech->dd, presents_cube);
  bdd_free(mech->dd, C);
  add_free(mech->dd, V);
  add_free(mech->dd, U);
  bdd_free(mech->dd, b_cube);

  M_q_double = 10*pow(beta_coal, k)/(1 - beta_coal);
  CPY_DOUBLE_INT(M_q_int, M_q_double);
  glob_eval_as_double_leaf = 1;
  theta = add_leaf(mech->dd, NODE_FROM_INT(M_q_int));
  tmp_add = add_apply(mech->dd, node_times, theta, M_q);
  glob_eval_as_double_leaf = 0;
  add_free(mech->dd, M_q);
  add_free(mech->dd, theta);
  glob_eval_as_double_leaf = 1;
  zeta = add_apply(mech->dd, node_minus, Delta_q, tmp_add);
  theta = add_apply(mech->dd, node_plus, Delta_q, tmp_add);
  glob_eval_as_double_leaf = 0;

  if (opt_verbose_level_gt(options, 1)) {
    printf("zeta is:\n");
    cuddPdoubles(mech->dd, zeta);
    printf("theta is:\n");
    cuddPdoubles(mech->dd, theta);
  }

  add_free(mech->dd, Delta_q);
  add_free(mech->dd, tmp_add);
  D = compute_D(mech->dd, q, max_coal, mech->agents, num_bits_sum, word_plus_bit, index_w_from, index_w_to, b_local_index);
  for (i = 0; i < num_bits_sum; i++)
    bdd_free(mech->dd, word_plus_bit[i]);
  FREE(word_plus_bit);
  FREE(q);
  
  CPY_DOUBLE_INT(M_q_int, epsilon);
  glob_eval_as_double_leaf = 1;
  tmp_add = add_leaf(mech->dd, NODE_FROM_INT(M_q_int));
  tmp_add2 = add_apply(mech->dd, node_gt, zeta, tmp_add);
  glob_eval_as_double_leaf = 0;
  add_free(mech->dd, zeta);
  add_free(mech->dd, tmp_add);
  tmp2 = add_to_bdd(mech->dd, tmp_add2);
  add_free(mech->dd, tmp_add2);
  bdd_and_accumulate(mech->dd, &tmp2, D);
  tmp = bdd_forsome(mech->dd, tmp2, q_cube);
  bdd_free(mech->dd, tmp2);
  assert(tmp == bdd_zero(mech->dd) || tmp == bdd_one(mech->dd));
  if (tmp == bdd_one(mech->dd)) {
    i = 0;
    bdd_free(mech->dd, q_cube);
    bdd_free(mech->dd, D);
  }
  else {
    bdd_free(mech->dd, tmp);
    CPY_DOUBLE_INT(M_q_int, epsilon);
    glob_eval_as_double_leaf = 1;
    tmp_add = add_leaf(mech->dd, NODE_FROM_INT(M_q_int));
    tmp_add2 = add_apply(mech->dd, node_gt, tmp_add, theta);
    glob_eval_as_double_leaf = 0;
    add_free(mech->dd, theta);
    add_free(mech->dd, tmp_add);
    tmp2 = add_to_bdd(mech->dd, tmp_add2);
    add_free(mech->dd, tmp_add2);
    tmp = bdd_not(mech->dd, D);
    bdd_free(mech->dd, D);
    bdd_or_accumulate(mech->dd, &tmp2, tmp);
    bdd_free(mech->dd, tmp);
    tmp = bdd_forall(mech->dd, tmp2, q_cube);
    bdd_free(mech->dd, q_cube);
    bdd_free(mech->dd, tmp2);
    assert(tmp == bdd_zero(mech->dd) || tmp == bdd_one(mech->dd));
    i = (tmp == bdd_one(mech->dd))? 1 : 2;
  }

  bdd_free(mech->dd, tmp);
  if (opt_verbose_level_gt(options, 0))
    PrintTop("end of computation");

  return i;
}

char ** GetVariableNames(DdManager *dd)
{
  NodeList_ptr vars;
  ListIter_ptr iter;
  SymbTable_ptr symb_table;
  BddEnc_ptr bdd_enc;
  char **res = NULL;
  int index = 0;
  int i;
  int n;

  symb_table = Compile_get_global_symb_table();
  if (!symb_table)
    return res;

  n = SymbTable_get_vars_num(symb_table);
  res = (char **) malloc(2 * n * sizeof(char *));
  bdd_enc = Enc_get_bdd_encoding();
  vars = BddEnc_get_var_ordering(bdd_enc, DUMP_BITS);

  for (iter = NodeList_get_first_iter(vars); 
       ! ListIter_is_end(iter); iter = ListIter_get_next(iter)) {

    /* present or input */
    node_ptr var = NodeList_get_elem_at(vars, iter);

    i = BddEnc_get_var_index_from_name(bdd_enc, var);
    char *tmp = strdup(sprint_node(var));
    /*fprintf(nusmv_stdout, "%s %d (%d)\n", tmp, i, Cudd_ReadPerm(dd, i));*/
    res[Cudd_ReadPerm(dd, i)] = strdup(tmp);

    if (SymbTable_is_symbol_state_var(symb_table, var)) {
      tmp = (char *) malloc((strlen(res[Cudd_ReadPerm(dd, i)]) + 10) * sizeof(char));
      sprintf(tmp, "%s'", res[Cudd_ReadPerm(dd, i)]);
      /* hyp: the next var is the one after the corresponding state var */
      res[Cudd_ReadPerm(dd, i + 1)] = strdup(tmp);
      /*fprintf(nusmv_stdout, "%s %d (%d)\n", tmp, i + 1, Cudd_ReadPerm(dd, i + 1));*/
    }

    free(tmp);
  }

#if 0
  fprintf(nusmv_stdout, "n = %d\n", n);
  for (i = 0; i < 2 * n; i++) 
    fprintf(nusmv_stdout, "[ %d %s ]\n", i, res[i]);
#endif

  return res;
}

int DefinitelyDump(DdManager *dd, int (*dump_fn)(DdManager *, int, DdNode **, char **, char **, FILE *), 
                   DdNode **node, char *node_string, char *filename, int with_varnames)
{
  char **wrap_name = (char **) malloc(sizeof(char *));
  char **vars;
  FILE *filed = fopen(filename, "w+");

  wrap_name[0] = strdup(node_string);

  if (!filed) {
    fprintf(nusmv_stderr, "DefinitelyDump: Problems in opening file %s.\n", filename);
    return 0;
  }

  vars = GetVariableNames(dd);
  assert(vars != NULL);
  if (with_varnames)
    (*dump_fn)(dd, 1, node, vars, wrap_name, filed);
  else
    (*dump_fn)(dd, 1, node, NULL, NULL, filed);

  fclose(filed);
  free(wrap_name[0]);
  return 1;
}

/*---------------------------------------------------------------------------*/
/* Definition of static functions                                            */
/*---------------------------------------------------------------------------*/

static bdd_ptr cube_a_i(DdManager *dd, int i, all_agents_info *agents_info)
{
  bdd_ptr a_i;
  indexes_list L;
  int *vars_array;

  vars_array = ALLOC(int, dd_get_size(dd));
  memset(vars_array, 2, sizeof(int)*dd_get_size(dd));      
  for (L = agents_info->action_indexes[i]; L; L = L->next)
    vars_array[L->index] = 1;
  a_i = bdd_array2bdd(dd, vars_array);
  FREE(vars_array);
  return a_i;
}

static bdd_ptr cube_a_minus_i(DdManager *dd, int i, int agents, all_agents_info *agents_info)
{
  bdd_ptr a_minus_i;
  int j, *vars_array;

  vars_array = ALLOC(int, dd_get_size(dd));
  memset(vars_array, 2, sizeof(int)*dd_get_size(dd));
  for (j = 0; j < agents; j++) {
    if (j != i) {
      indexes_list L;
      
      for (L = agents_info->action_indexes[j]; L; L = L->next)
	vars_array[L->index] = 1;
    }
  }
  a_minus_i = bdd_array2bdd(dd, vars_array);
  FREE(vars_array);
  return a_minus_i;
}

static bdd_ptr reuse_vars_and_return_cube_b(DdManager *dd, bdd_ptr *b, int dim, int to_be_jumped, all_agents_info *agents_info)
{
  bdd_ptr b_cube = bdd_one(dd), tmp, tmp2;
  int i;

  for (i = 0; i < dim; i++) {
    if (i == to_be_jumped)
      b[i] = bdd_zero(dd);
    else {
      b[i] = bdd_new_var_with_index(dd, agents_info->byz_indexes[2*i]);
      tmp = bdd_and(dd, b_cube, b[i]);
      bdd_free(dd, b_cube);
      b_cube = tmp;
    }
  }
  return b_cube;
}

static bdd_ptr reuse_vars_and_return_cube_q(DdManager *dd, bdd_ptr *q, int dim, all_agents_info *agents_info)
{
  bdd_ptr q_cube = bdd_one(dd), tmp, tmp2;
  int i;

  for (i = 0; i < dim; i++) {
    q[i] = bdd_new_var_with_index(dd, agents_info->coal_indexes[2*i]);
    tmp = bdd_and(dd, q_cube, q[i]);
    bdd_free(dd, q_cube);
    q_cube = tmp;
  }
  return q_cube;
}

static void create_vars(DdManager *dd, bdd_ptr *b, int dim, int to_be_jumped)
{
  int i;

  for (i = 0; i < dim; i++) {
    if (i == to_be_jumped)
      b[0] = bdd_zero(dd);
    else
      b[i] = Cudd_bddNewVar(dd);
  }
}

static void add_create_vars(DdManager *dd, add_ptr *a, int dim)
{
  int i;

  for (i = 0; i < dim; i++) {
    a[i] = Cudd_addNewVar(dd);
    add_ref(a[i]);
  }
}

static bdd_ptr compute_N_i(bdd_ptr *b, Mechanism_ptr mech, int i, bdd_ptr BT)
{
  bdd_ptr N_i = bdd_one(mech->dd), tmp;
  int j;

  for (j = 0; j < mech->agents; j++) {
    if (j != i)
      tmp = bdd_ite(mech->dd, b[j], mech->B[j], mech->T[j]);
    else
      tmp = bdd_dup(BT);
    bdd_and_accumulate(mech->dd, &N_i, tmp);
    bdd_free(mech->dd, tmp);
  }
  return N_i;
}

static bdd_ptr compute_N(bdd_ptr *b, bdd_ptr *q, Mechanism_ptr mech, bdd_ptr *BT)
{
  bdd_ptr N = bdd_one(mech->dd), tmp, tmp2;
  int j;

  for (j = 0; j < mech->agents; j++) {
    tmp = bdd_ite(mech->dd, b[j], mech->B[j], mech->T[j]);
    tmp2 = bdd_ite(mech->dd, q[j], BT[j], tmp);
    bdd_free(mech->dd, tmp);
    bdd_and_accumulate(mech->dd, &N, tmp2);
    bdd_free(mech->dd, tmp2);
  }
  return N;
}

static bdd_ptr compute_C_i(DdManager *dd, bdd_ptr *b, int max_byz, int j, int agents, int num_bits_sum,
                           bdd_ptr *word_plus_bit, int index_w_from, int index_w_to, int b_local_index)
{
  int i, k, index_cfr_from, index_cfr_to, l;
  bdd_ptr tmp, tmp2, tmp3, *sum, *sum_next;
  
  sum = ALLOC(bdd_ptr, num_bits_sum);
  sum_next = ALLOC(bdd_ptr, num_bits_sum);
  for (i = 0; i < num_bits_sum; i++)
    sum[i] = bdd_zero(dd);
  for (i = 0; i < agents; i++) {
    if (i != j) {
      for (l = 0; l < num_bits_sum; l++) {
        /* the following part computes the l-th bit of sum + b_i... */
      	tmp = bdd_compose(dd, word_plus_bit[l], b[i], b_local_index);
	for (k = index_w_from; k <= index_w_to; k++) {
	  tmp2 = bdd_compose(dd, tmp, sum[k - index_w_from], k);
	  bdd_free(dd, tmp);
	  tmp = tmp2;
	}
	/* ...and puts it in sum_next[l]... */
	sum_next[l] = tmp;
      }
      /* ...and then back to sum[l] */
      for (l = 0; l < num_bits_sum; l++) {
	bdd_free(dd, sum[l]);
	sum[l] = sum_next[l];
      }
    }
  }
  tmp = bdd_word_int_leq(dd, sum, max_byz, num_bits_sum);
  tmp3 = bdd_zero(dd);
  tmp2 = bdd_equivalence(dd, b[j], tmp3);
  bdd_free(dd, tmp3);
  bdd_and_accumulate(dd, &tmp, tmp2);
  bdd_free(dd, tmp2);
  for (l = 0; l < num_bits_sum; l++)
    bdd_free(dd, sum[l]);
  FREE(sum);
  FREE(sum_next);
  return tmp;
}

static bdd_ptr compute_C(DdManager *dd, bdd_ptr *b, bdd_ptr *q, int max_byz, int max_coal, int agents, int num_bits_sum,
                         bdd_ptr *word_plus_bit, int index_w_from, int index_w_to, int b_local_index)
{
  int i, k, index_cfr_from, index_cfr_to, l;
  bdd_ptr tmp, tmp2, tmp3, *sum, *sum_next, res;
  
  sum = ALLOC(bdd_ptr, num_bits_sum);
  sum_next = ALLOC(bdd_ptr, num_bits_sum);
  for (i = 0; i < num_bits_sum; i++)
    sum[i] = bdd_zero(dd);
  for (i = 0; i < agents; i++) {
    for (l = 0; l < num_bits_sum; l++) {
      /* the following part computes the l-th bit of sum + not(q_i)*b_i... */
      tmp3 = bdd_not(dd, q[i]);
      tmp2 = bdd_and(dd, b[i], tmp3);
      bdd_free(dd, tmp3);
      tmp = bdd_compose(dd, word_plus_bit[l], tmp2, b_local_index);
      bdd_free(dd, tmp2);
      for (k = index_w_from; k <= index_w_to; k++) {
	tmp2 = bdd_compose(dd, tmp, sum[k - index_w_from], k);
	bdd_free(dd, tmp);
	tmp = tmp2;
      }
      /* ...and puts it in sum_next[l]... */
      sum_next[l] = tmp;
    }
    /* ...and then back to sum[l] */
    for (l = 0; l < num_bits_sum; l++) {
      bdd_free(dd, sum[l]);
      sum[l] = sum_next[l];
    }
  }
  res = bdd_word_int_leq(dd, sum, max_byz, num_bits_sum);
  for (l = 0; l < num_bits_sum; l++)
    bdd_free(dd, sum[l]);

  for (i = 0; i < num_bits_sum; i++)
    sum[i] = bdd_zero(dd);
  for (i = 0; i < agents; i++) {
    for (l = 0; l < num_bits_sum; l++) {
      /* the following part computes the l-th bit of sum + q_i*b_i... */
      tmp2 = bdd_and(dd, b[i], q[i]);
      tmp = bdd_compose(dd, word_plus_bit[l], tmp2, b_local_index);
      bdd_free(dd, tmp2);
      for (k = index_w_from; k <= index_w_to; k++) {
	tmp2 = bdd_compose(dd, tmp, sum[k - index_w_from], k);
	bdd_free(dd, tmp);
	tmp = tmp2;
      }
      /* ...and puts it in sum_next[l]... */
      sum_next[l] = tmp;
    }
    /* ...and then back to sum[l] */
    for (l = 0; l < num_bits_sum; l++) {
      bdd_free(dd, sum[l]);
      sum[l] = sum_next[l];
    }
  }
  tmp3 = bdd_zero(dd);
  for (i = 0; i < num_bits_sum; i++) {
    tmp2 = bdd_equivalence(dd, sum[i], tmp3);
    bdd_and_accumulate(dd, &res, tmp2);
    bdd_free(dd, tmp2);
    bdd_free(dd, sum[i]);
  }
  bdd_free(dd, tmp3);
    
  for (i = 0; i < num_bits_sum; i++)
    sum[i] = bdd_zero(dd);
  for (i = 0; i < agents; i++) {
    for (l = 0; l < num_bits_sum; l++) {
      /* the following part computes the l-th bit of sum + q_i... */
      tmp = bdd_compose(dd, word_plus_bit[l], q[i], b_local_index);
      for (k = index_w_from; k <= index_w_to; k++) {
	tmp2 = bdd_compose(dd, tmp, sum[k - index_w_from], k);
	bdd_free(dd, tmp);
	tmp = tmp2;
      }
      /* ...and puts it in sum_next[l]... */
      sum_next[l] = tmp;
    }
    /* ...and then back to sum[l] */
    for (l = 0; l < num_bits_sum; l++) {
      bdd_free(dd, sum[l]);
      sum[l] = sum_next[l];
    }
  }
  tmp = bdd_word_int_leq(dd, sum, max_coal, num_bits_sum);
  bdd_and_accumulate(dd, &res, tmp);
  bdd_free(dd, tmp);
  tmp3 = bdd_one(dd);
  tmp = bdd_zero(dd);
  for (l = 0; l < num_bits_sum; l++) {
    tmp2 = bdd_equivalence(dd, sum[l], tmp3);
    bdd_or_accumulate(dd, &tmp, tmp2);
    bdd_free(dd, tmp2);
    bdd_free(dd, sum[l]);
  }
  bdd_free(dd, tmp3);
  bdd_and_accumulate(dd, &res, tmp);
  bdd_free(dd, tmp);

  FREE(sum);
  FREE(sum_next);
  return res;
}

static bdd_ptr compute_D(DdManager *dd, bdd_ptr *q, int max_coal, int agents, int num_bits_sum,
                         bdd_ptr *word_plus_bit, int index_w_from, int index_w_to, int b_local_index)
{
  int i, k, index_cfr_from, index_cfr_to, l;
  bdd_ptr tmp, tmp2, tmp3, *sum, *sum_next, res;
  
  sum = ALLOC(bdd_ptr, num_bits_sum);
  sum_next = ALLOC(bdd_ptr, num_bits_sum);
  for (i = 0; i < num_bits_sum; i++)
    sum[i] = bdd_zero(dd);
  for (i = 0; i < agents; i++) {
    for (l = 0; l < num_bits_sum; l++) {
      /* the following part computes the l-th bit of sum + q_i... */
      tmp = bdd_compose(dd, word_plus_bit[l], q[i], b_local_index);
      for (k = index_w_from; k <= index_w_to; k++) {
	tmp2 = bdd_compose(dd, tmp, sum[k - index_w_from], k);
	bdd_free(dd, tmp);
	tmp = tmp2;
      }
      /* ...and puts it in sum_next[l]... */
      sum_next[l] = tmp;
    }
    /* ...and then back to sum[l] */
    for (l = 0; l < num_bits_sum; l++) {
      bdd_free(dd, sum[l]);
      sum[l] = sum_next[l];
    }
  }
  res = bdd_word_int_leq(dd, sum, max_coal, num_bits_sum);
  tmp3 = bdd_one(dd);
  tmp = bdd_zero(dd);
  for (l = 0; l < num_bits_sum; l++) {
    tmp2 = bdd_equivalence(dd, sum[l], tmp3);
    bdd_or_accumulate(dd, &tmp, tmp2);
    bdd_free(dd, tmp2);
    bdd_free(dd, sum[l]);
  }
  bdd_free(dd, tmp3);
  bdd_and_accumulate(dd, &res, tmp);
  bdd_free(dd, tmp);

  FREE(sum);
  FREE(sum_next);
  return res;
}

static bdd_ptr *bdd_word_plus_bit(DdManager *dd, int num_bits_sum, int *index_w_from, int *index_w_to, int *index_b_local)
{
  int i, k;
  bdd_ptr tmp, tmp2;
  bdd_ptr *w, *b_local;	/* inputs */
  bdd_ptr *compute_bdd_word_plus_bit = (bdd_ptr *)NULL;
  /* for 0 <= i <= num_bits_sum - 1, compute_bdd_word_plus_bit[i] = i-th bit of w[num_bits_sum - 1]...w[0] + b_local[0] */
  /* computed as: s_0 = w_0 xor b_0, s_1 = w_1 xor w_0b_0, s_2 = w_2 xor w_1w_0b_0... */
  
  compute_bdd_word_plus_bit = ALLOC(bdd_ptr, num_bits_sum);
  w = ALLOC(bdd_ptr, num_bits_sum);
  *index_w_from = dd_get_size(dd);
  create_vars(dd, w, num_bits_sum, -1);
  *index_w_to = dd_get_size(dd) - 1;
  b_local = ALLOC(bdd_ptr, 1);
  create_vars(dd, b_local, 1, -1);
  *index_b_local = dd_get_size(dd) - 1;
  for (i = 0; i < num_bits_sum; i++) {
    tmp2 = bdd_dup(b_local[0]);
    for (k = 0; k < i; k++) {
      tmp = bdd_and(dd, tmp2, w[k]);
      bdd_free(dd, tmp2);
      tmp2 = tmp;
    }
    compute_bdd_word_plus_bit[i] = bdd_xor(dd, tmp2, w[i]);
    bdd_free(dd, tmp2);
  }
  /*bdd_free(dd, b_local[0]);
  for (i = 0; i < num_bits_sum; i++)
    bdd_free(dd, w[i]);*/
  FREE(w);
  FREE(b_local);
  
  return compute_bdd_word_plus_bit;
}

static bdd_ptr bdd_equivalence(DdManager *dd, bdd_ptr v1, bdd_ptr v2)
{
  bdd_ptr tmp, tmp2;
  bdd_ptr ret;

  tmp = bdd_imply(dd, v1, v2);
  tmp2 = bdd_imply(dd, v2, v1);
  ret = bdd_and(dd, tmp, tmp2);
  bdd_free(dd, tmp);
  bdd_free(dd, tmp2);
  return ret;
}

static bdd_ptr bdd_word_int_leq(DdManager *dd, bdd_ptr *y, int m, int num_bits)
{
  bdd_ptr tmp, tmp2, tmp3;
  int i, j;
  /* returns the BDD which is true iff the word represented by y[num_bits - 1]...y[0] <= m[num_bits - 1]...m[0] */
  /* computed as: (AND_{i = n}^0 (AND_{j=n}^{i + 1} y[j] <-> m[j] AND y[i] -> m[i])) */
  /* since m[h] are known constants, we compute y[j] <-> m[j] as y[j] if m[j] = 1, and \neg y[j] otherwise */
  /* similarly, we compute y[j] -> m[j] as 1 if m[j] = 1, and \neg y[j] otherwise */
  
  tmp = bdd_one(dd); /* will accumulate the outer AND */
  for (i = num_bits - 1; i >= 0; i--) {
    tmp2 = bdd_one(dd); /* will accumulate the inner AND */
    for (j = num_bits - 1; j >= i + 1; j--) {
      if (m & (1 << j))
	tmp3 = bdd_dup(y[j]);
      else
	tmp3 = bdd_not(dd, y[j]);
      bdd_and_accumulate(dd, &tmp2, tmp3);
      bdd_free(dd, tmp3);
    }
    if (m & (1 << i))
      tmp3 = bdd_one(dd);
    else
      tmp3 = bdd_not(dd, y[j]);
    bdd_and_accumulate(dd, &tmp2, tmp3);
    bdd_free(dd, tmp3);
    bdd_and_accumulate(dd, &tmp, tmp2);
    bdd_free(dd, tmp2);
  }
  return tmp;
}

static void needed_bdds_adds(DdManager *dd, BddVarSet_ptr *presents_cube, BddVarSet_ptr *inputs_cube, BddVarSet_ptr *nexts_cube, 
                             int *nexts_num, int **support_presents_cube_incr, int **support_nexts_cube_incr, int agents,
			     bdd_ptr **word_plus_bit, int *index_w_from, int *index_w_to, int *b_local_index, int num_bits_sum,
			     bdd_ptr *b_cube, bdd_ptr *b, add_ptr **fresh_vars, all_agents_info *agents_info)
{
  int next_first, i, *support_all_nexts_cube, *support_all_presents_cube;
  BddVarSet_ptr all_presents_cube, all_nexts_cube;

  *b_cube = reuse_vars_and_return_cube_b(dd, b, agents, agents_info->num_glob_agents? 0 : -1, agents_info);
  *inputs_cube = BddEnc_get_input_vars_cube(Enc_get_bdd_encoding());
  all_presents_cube = BddEnc_get_state_vars_cube(Enc_get_bdd_encoding());
  all_nexts_cube = BddEnc_get_next_state_vars_cube(Enc_get_bdd_encoding());
  support_all_nexts_cube = Cudd_SupportIndex(dd, all_nexts_cube); 
  support_all_presents_cube = Cudd_SupportIndex(dd, all_presents_cube); 
  for (i = 0; i < agents; i++) {
    indexes_list L;
    unsigned count;
    for (L = agents_info->h_indexes[i], count = 0; L; count++, L = L->next) {
      if (count%2)
	support_all_nexts_cube[L->index] = 2;
      else
	support_all_presents_cube[L->index] = 2;
    }
    for (count = 0; count < 2*agents; count++) {
      if (count%2)
	support_all_nexts_cube[agents_info->byz_indexes[count]] = 2;
      else
	support_all_presents_cube[agents_info->byz_indexes[count]] = 2;
    }
  }
  *nexts_num = dd_get_size(dd);
  for (i = 0; i < (*nexts_num); i++) {
    if (support_all_nexts_cube[i] == 0) support_all_nexts_cube[i] = 2;
    if (support_all_presents_cube[i] == 0) support_all_presents_cube[i] = 2;
  }
  *presents_cube = bdd_array2bdd(dd, support_all_presents_cube);
  *nexts_cube = bdd_array2bdd(dd, support_all_nexts_cube);
  *nexts_num = Cudd_SupportSize(dd, *nexts_cube);
  *support_nexts_cube_incr = ALLOC(int, *nexts_num);
  *support_presents_cube_incr = ALLOC(int, *nexts_num);
  for (i = 0, next_first = 0; i < (*nexts_num); i++) {
    for (; support_all_nexts_cube[next_first] != 1; next_first++);
    (*support_nexts_cube_incr)[i] = next_first++;
  }
  for (i = 0, next_first = 0; i < (*nexts_num); i++) {
    for (; support_all_presents_cube[next_first] != 1; next_first++);
    (*support_presents_cube_incr)[i] = next_first++;
  }
  FREE(support_all_presents_cube);
  FREE(support_all_nexts_cube);
  *word_plus_bit = bdd_word_plus_bit(dd, num_bits_sum, index_w_from, index_w_to, b_local_index);
  *fresh_vars = ALLOC(add_ptr, *nexts_num);
  add_create_vars(dd, *fresh_vars, *nexts_num);
}

static void compute_F_i(DdManager *dd, bdd_ptr N, add_ptr *F_i, BddVarSet_ptr next_cube, int nexts_num, int *support_nexts_cube_incr)
{
  bdd_ptr next_cube_tmp, tmp, tmp2;
  int i, j;

  for (i = 0; i < nexts_num; i++) {
    F_i[i] = bdd_dup(N);
    tmp = bdd_one(dd);
    tmp2 = bdd_compose(dd, F_i[i], tmp, support_nexts_cube_incr[i]);
    bdd_free(dd, F_i[i]);
    bdd_free(dd, tmp);
    F_i[i] = tmp2;
    tmp = bdd_one(dd);
    tmp2 = bdd_compose(dd, next_cube, tmp, support_nexts_cube_incr[i]);
    bdd_free(dd, tmp);
    next_cube_tmp = tmp2;
    tmp = bdd_forsome(dd, F_i[i], next_cube_tmp);
    bdd_free(dd, next_cube_tmp);
    bdd_free(dd, F_i[i]);
    F_i[i] = bdd_to_add(dd, tmp);
    bdd_free(dd, tmp);
  }
}

static void compute_F(DdManager *dd, bdd_ptr N, add_ptr *F_i, BddVarSet_ptr next_cube, int nexts_num, int *support_nexts_cube_incr)
{
  compute_F_i(dd, N, F_i, next_cube, nexts_num, support_nexts_cube_incr);
}

static add_ptr compute_H_i(DdManager *dd, add_ptr h, add_ptr UV, bdd_ptr *F_i, BddVarSet_ptr next_cube, int nexts_num, 
                           int *support_presents_cube_incr, nusmv_double beta, add_ptr *fresh_vars)
{
  add_ptr res, tmp, add_beta;
  int i, count;
  nusmv_ptrint beta_i;
  
  res = bdd_dup(UV);
  if (!add_isleaf(UV)) {
    for (i = 0; i < nexts_num; i++) {
      tmp = add_compose(dd, res, fresh_vars[i], support_presents_cube_incr[i]);
      add_free(dd, res);
      res = tmp;
    }
    for (i = 0; i < nexts_num; i++) {
      tmp = add_compose(dd, res, F_i[i], add_index(dd, fresh_vars[i]));
      add_free(dd, res);
      res = tmp;
    }
  }
  CPY_DOUBLE_INT(beta_i, beta);
  glob_eval_as_double_leaf = 1;
  add_beta = add_leaf(dd, NODE_FROM_INT(beta_i));
  tmp = add_apply(dd, node_times, res, add_beta);
  glob_eval_as_double_leaf = 0;
  add_free(dd, res);
  add_free(dd, add_beta);
  res = tmp;
  glob_eval_as_double_leaf = 1;
  tmp = add_apply(dd, node_plus, res, h);
  glob_eval_as_double_leaf = 0;
  add_free(dd, res);
  res = tmp;
  return res;
}

static add_ptr compute_H(DdManager *dd, add_ptr h, add_ptr UV, bdd_ptr *F_i, BddVarSet_ptr next_cube, int nexts_num, 
                         int *support_presents_cube_incr, nusmv_double beta_coal, add_ptr *fresh_vars)
{
  return compute_H_i(dd, h, UV, F_i, next_cube, nexts_num, support_presents_cube_incr, beta_coal, fresh_vars);
}

static add_ptr add_max_min(DdManager *dd, bdd_ptr x_cube, add_ptr F, operation which)
{
  add_ptr res;
  bdd_ptr x_cube_support, support[2];

  if (which != MAXIMUM && which != MINIMUM) {
    fprintf(nusmv_stderr, "%s:%d (add_max_min) Operation not permitted\n", __FILE__, __LINE__);
    nusmv_exit(1);
  }

  bdd_classifysupport(dd, F, x_cube, &x_cube_support, &support[0], &support[1]);
  bdd_free(dd, support[1]);
  bdd_free(dd, support[0]);
  res = add_max_min_recur(dd, x_cube_support, F, which);
  bdd_free(dd, x_cube_support);
  return res;
}

static nusmv_double compute_Delta_i(DdManager *dd, add_ptr V_i, add_ptr U_i, bdd_ptr I, BddVarSet_ptr state_vars_cube, bdd_ptr b_cube) 
{ 
  add_ptr tmp, tmp2; 
  nusmv_double Delta_i; 
  nusmv_ptrint Delta_i_int;
  bdd_ptr x_and_b = bdd_dup(b_cube);
 
  glob_eval_as_double_leaf = 1;
  tmp = add_apply(dd, node_minus, V_i, U_i);
  if (opt_verbose_level_gt(options, 1)) {
    printf("V_i - U_i is:\n");
    cuddPdoubles(dd, tmp);
  }
  glob_eval_as_double_leaf = 0;
 
  bdd_and_accumulate(dd, &x_and_b, (bdd_ptr)state_vars_cube);
  tmp2 = add_max_min_conditioned(dd, x_and_b, I, tmp, MAXIMUM);
  bdd_free(dd, x_and_b);
  add_free(dd, tmp); 
 
  nusmv_assert(add_isleaf(tmp2));
  Delta_i_int = MY_NODE_TO_INT(add_get_leaf(dd, tmp2)); 
  CPY_DOUBLE_INT(Delta_i, Delta_i_int);
  add_free(dd, tmp2); 
 
  return Delta_i; 
} 

static add_ptr compute_Delta(DdManager *dd, add_ptr V, add_ptr U, bdd_ptr I, BddVarSet_ptr state_vars_cube, bdd_ptr b_cube) 
{ 
  add_ptr tmp, Delta; 
  bdd_ptr x_and_b = bdd_dup(b_cube);
 
  glob_eval_as_double_leaf = 1;
  tmp = add_apply(dd, node_minus, V, U);
  if (opt_verbose_level_gt(options, 1)) {
    printf("V - U is:\n");
    cuddPdoubles(dd, tmp);
  }
  glob_eval_as_double_leaf = 0;
 
  bdd_and_accumulate(dd, &x_and_b, (bdd_ptr)state_vars_cube);
  Delta = add_max_min_conditioned(dd, x_and_b, I, tmp, MAXIMUM);
  bdd_free(dd, x_and_b);
  add_free(dd, tmp); 
 
  return Delta; 
} 

static add_ptr add_max_min_recur(DdManager *dd, bdd_ptr x_cube, add_ptr F, operation which)
{ /* precondition: x_cube is exactly the support of F (and which in {MAXIMUM, MINIMUM}) */
  add_ptr zero, If, Then, Else, ThenRecur, ElseRecur, x_cube_minus_1, res;
  bdd_ptr one;
  int x_first, *supporti;

  one = bdd_one(dd);
  if (x_cube == one || add_isleaf(F)) {
    bdd_free(dd, one);
    add_ref(F);
    return F;
  }

  x_first = bdd_index(dd, x_cube);

  zero = add_zero(dd);

  /* Then = F(<0, x_{-1}>, y) */
  Then = add_compose(dd, F, zero, x_first);
  add_free(dd, zero);

  /* Recursion: */
  /* ThenRecur = add_max_recur(dd, x_{-1}, Then) */
  x_cube_minus_1 = bdd_compose(dd, x_cube, one, x_first);
  ThenRecur = add_max_min_recur(dd, x_cube_minus_1, Then, which);
  add_free(dd, Then);

  /* Else = F(<1, x_{-1}>, y) */
  Else = add_compose(dd, F, one, x_first);
  bdd_free(dd, one);

  /* ElseRecur = add_max_recur(dd, x_{-1}, Else) */
  ElseRecur = add_max_min_recur(dd, x_cube_minus_1, Else, which);
  add_free(dd, Else);
  bdd_free(dd, x_cube_minus_1);

  /* If = ADD_GT(ThenRecur, ElseRecur) */
  glob_eval_as_double_leaf = 1;
  If = add_apply(dd, node_gt, ThenRecur, ElseRecur);
  glob_eval_as_double_leaf = 0;

  /* return ADD_ITE(If, ThenRecur, ElseRecur) */
  if (which == MAXIMUM)
    res = add_ifthenelse(dd, If, ThenRecur, ElseRecur);
  else
    res = add_ifthenelse(dd, If, ElseRecur, ThenRecur);
  add_free(dd, If);
  add_free(dd, ThenRecur);
  add_free(dd, ElseRecur);

  return res;
}

static add_ptr add_max_min_conditioned(DdManager *dd, bdd_ptr x_cube, bdd_ptr C, add_ptr F, operation which)
{
  if (which != MAXIMUM && which != MINIMUM) {
    fprintf(nusmv_stderr, "%s:%d (add_max_min_conditioned) Operation not permitted\n", __FILE__, __LINE__);
    nusmv_exit(1);
  }

  return add_max_min_conditioned_recur_noenhanced(dd, x_cube, C, F, which);
}

static add_ptr add_max_min_conditioned_recur_noenhanced(DdManager *dd, bdd_ptr x_cube, bdd_ptr C, add_ptr F, operation which)
{ /* pre: which in {MAXIMUM, MINIMUM} */
  add_ptr zero, Gamma, Theta[2], F_0, F_1, Delta, ThenRecur, ElseRecur, res, add_Exists_C_0, add_Exists_C_1, add_DeltaTilda, add_infty;
  bdd_ptr zero_bdd, one, C_0, C_1, Exists_C_0, Exists_C_1, x_cube_minus_1, DeltaTilda, support[3];
  int i, size, x_first, *supporti;
  nusmv_double tmp_d = (which == MINIMUM? PLUS_INFTY : MINUS_INFTY);
  nusmv_ptrint tmp_d_i;

  one = bdd_one(dd);
  if (x_cube == one) {
    bdd_free(dd, one);
    add_ref(F);
    return F;
  }
  zero = add_zero(dd);
  zero_bdd = bdd_zero(dd);

  x_first = bdd_index(dd, x_cube);

  /* C_0 = C(<0, x_{-1}>) */
  C_0 = bdd_compose(dd, C, zero_bdd, x_first);
  x_cube_minus_1 = bdd_compose(dd, x_cube, one, x_first);
  /* Exists_C_0 = \exists x_{-1} C(<0, x_{-1}>) */
  bdd_classifysupport(dd, C_0, x_cube_minus_1, &support[0], &support[1], &support[2]);
  bdd_free(dd, support[0]);
  bdd_free(dd, support[1]);
  if (x_cube_minus_1 != support[2])/* or support[0] != bdd_zero... */
    Exists_C_0 = bdd_forsome(dd, C_0, x_cube_minus_1);
  else
    Exists_C_0 = bdd_dup(C_0);
  bdd_free(dd, support[2]);

  /* C_1 = C(<1, x_{-1}>) */
  C_1 = bdd_compose(dd, C, one, x_first);
  /* Exists_C_1 = \exists x_{-1} C(<1, x_{-1}>) */
  bdd_classifysupport(dd, C_1, x_cube_minus_1, &support[0], &support[1], &support[2]);
  bdd_free(dd, support[0]);
  bdd_free(dd, support[1]);
  if (x_cube_minus_1 != support[2])
    Exists_C_1 = bdd_forsome(dd, C_1, x_cube_minus_1);
  else
    Exists_C_1 = bdd_dup(C_1);
  bdd_free(dd, support[2]);

  CPY_DOUBLE_INT(tmp_d_i, tmp_d);
  glob_eval_as_double_leaf = 1;
  add_infty = add_leaf(dd, NODE_FROM_INT(tmp_d_i));
  glob_eval_as_double_leaf = 0;

  if (Exists_C_0 == zero_bdd && Exists_C_1 == zero_bdd) {
    bdd_free(dd, Exists_C_1);
    bdd_free(dd, Exists_C_0);
    bdd_free(dd, zero_bdd);
    bdd_free(dd, one);
    add_free(dd, zero);
    bdd_free(dd, C_0);
    bdd_free(dd, x_cube_minus_1);
    bdd_free(dd, C_1);
    return add_infty;
  }

  if (add_isleaf(F)) {
    bdd_free(dd, Exists_C_1);
    bdd_free(dd, Exists_C_0);
    bdd_free(dd, zero_bdd);
    bdd_free(dd, one);
    add_free(dd, zero);
    bdd_free(dd, C_0);
    bdd_free(dd, x_cube_minus_1);
    bdd_free(dd, C_1);
    add_free(dd, add_infty);
    add_ref(F);
    return F;
  }
  bdd_free(dd, zero_bdd);

  /* F_0 = F(<0, x_{-1}>, y) */
  F_0 = add_compose(dd, F, zero, x_first);
  add_free(dd, zero);
  /* ThenRecur = add_max_min_conditioned_recur_noenhanced(dd, x_{-1}, C_0, F_0, which); */
  ThenRecur = add_max_min_conditioned_recur_noenhanced(dd, x_cube_minus_1, C_0, F_0, which);
  add_free(dd, F_0);
  bdd_free(dd, C_0);
  add_Exists_C_0 = bdd_to_add(dd, Exists_C_0);
  bdd_free(dd, Exists_C_0);
  Theta[0] = add_ifthenelse(dd, add_Exists_C_0, ThenRecur, add_infty);
  add_free(dd, add_Exists_C_0);

  /* F_1 = F(<1, x_{-1}>, y) */
  F_1 = add_compose(dd, F, one, x_first);
  bdd_free(dd, one);
  /* ElseRecur = add_max_min_conditioned_recur_noenhanced(dd, x_{-1}, C_1, F_1, which); */
  ElseRecur = add_max_min_conditioned_recur_noenhanced(dd, x_cube_minus_1, C_1, F_1, which);
  add_free(dd, F_1);
  bdd_free(dd, C_1);
  bdd_free(dd, x_cube_minus_1);
  add_Exists_C_1 = bdd_to_add(dd, Exists_C_1);
  bdd_free(dd, Exists_C_1);
  Theta[1] = add_ifthenelse(dd, add_Exists_C_1, ElseRecur, add_infty);
  add_free(dd, add_infty);
  add_free(dd, add_Exists_C_1);

  /* Delta = ADD_GT(ThenRecur, ElseRecur) */
  glob_eval_as_double_leaf = 1;
  Delta = add_apply(dd, node_gt, Theta[0], Theta[1]);
  glob_eval_as_double_leaf = 0;

  /* Gamma = ADD_ITE(Delta, ThenRecur, ElseRecur) */
  if (which == MAXIMUM)
    Gamma = add_ifthenelse(dd, Delta, Theta[0], Theta[1]);
  else
    Gamma = add_ifthenelse(dd, Delta, Theta[1], Theta[0]);
  add_free(dd, Delta);

  add_free(dd, ThenRecur);
  add_free(dd, ElseRecur);
  add_free(dd, Theta[0]);
  add_free(dd, Theta[1]);

  return Gamma;
}

static nusmv_double compute_M_i(int i, Mechanism_ptr mech, BddVarSet_ptr state_vars_cube, BddVarSet_ptr input_vars_cube) 
{ 
  add_ptr zero, res, minus_h_i, abs_h_i, h_i_gt_0; 
  bdd_ptr support[3];
  nusmv_double M_i; 
  nusmv_ptrint M_i_int;
  BddVarSet_ptr input_state_vars_cube;

  zero = add_zero_double(mech->dd);
  glob_eval_as_double_leaf = 1;
  /*minus_h_i = add_apply(mech->dd, node_unary_minus, mech->h[i], (add_ptr)NULL); *//* seems cannot be done like this... */
  minus_h_i = add_apply(mech->dd, node_minus, zero, mech->h[i]);
  h_i_gt_0 = add_apply(mech->dd, node_gt, mech->h[i], zero); 
  glob_eval_as_double_leaf = 0;
  add_free(mech->dd, zero);
 
  abs_h_i = add_ifthenelse(mech->dd, h_i_gt_0, mech->h[i], minus_h_i); 
  add_free(mech->dd, h_i_gt_0); 
  add_free(mech->dd, minus_h_i); 
  
  input_state_vars_cube = bdd_and(mech->dd, input_vars_cube, state_vars_cube);
  bdd_classifysupport(mech->dd, abs_h_i, input_state_vars_cube, &support[0], &support[1], &support[2]);
  bdd_free(mech->dd, support[1]);
  bdd_free(mech->dd, support[2]);
  bdd_free(mech->dd, input_state_vars_cube);
  res = add_max_min(mech->dd, support[0], abs_h_i, MAXIMUM); 
  add_free(mech->dd, abs_h_i); 
  bdd_free(mech->dd, support[0]);
 
  nusmv_assert(add_isleaf(res));
  M_i_int = MY_NODE_TO_INT(add_get_leaf(mech->dd, res)); 
  add_free(mech->dd, res); 
  CPY_DOUBLE_INT(M_i, M_i_int); 
 
  return M_i; 
} 

static add_ptr compute_M_coal(Mechanism_ptr mech, BddVarSet_ptr state_vars_cube, BddVarSet_ptr input_vars_cube) 
{ 
  add_ptr zero, res, minus_h, abs_h, h_gt_0; 
  bdd_ptr support[3];
  nusmv_double M; 
  nusmv_ptrint M_int;
  BddVarSet_ptr input_state_vars_cube;

  zero = add_zero_double(mech->dd);
  glob_eval_as_double_leaf = 1;
  minus_h = add_apply(mech->dd, node_minus, zero, mech->h_coal);
  h_gt_0 = add_apply(mech->dd, node_gt, mech->h_coal, zero); 
  glob_eval_as_double_leaf = 0;
  add_free(mech->dd, zero);
 
  abs_h = add_ifthenelse(mech->dd, h_gt_0, mech->h_coal, minus_h); 
  add_free(mech->dd, h_gt_0); 
  add_free(mech->dd, minus_h); 
  
  input_state_vars_cube = bdd_and(mech->dd, input_vars_cube, state_vars_cube);
  bdd_classifysupport(mech->dd, abs_h, input_state_vars_cube, &support[0], &support[1], &support[2]);
  bdd_free(mech->dd, support[1]);
  bdd_free(mech->dd, support[2]);
  bdd_free(mech->dd, input_state_vars_cube);
  res = add_max_min(mech->dd, support[0], abs_h, MAXIMUM); 
  add_free(mech->dd, abs_h); 
  bdd_free(mech->dd, support[0]);
 
  return res; 
} 

static add_ptr add_max_min_together_conditioned(DdManager *dd, bdd_ptr x_M, bdd_ptr x_m, bdd_ptr C, add_ptr F)
{
  /* place (optional) optimization here */
  return add_max_min_together_conditioned_recur(dd, x_M, x_m, C, F);
}

static add_ptr add_max_min_together_conditioned_recur(DdManager *dd, bdd_ptr x_M, bdd_ptr x_m, bdd_ptr C, add_ptr F)
{
  add_ptr zero, Gamma, Theta[2], F_0, F_1, Delta, ThenRecur, ElseRecur, res, add_Exists_C_0, add_Exists_C_1, add_DeltaTilda, add_infty;
  bdd_ptr zero_bdd, one, C_0, C_1, Exists_C_0, Exists_C_1, x_m_minus_1, DeltaTilda, support[3], x_m_minus_1_plus_x_M;
  int i, size, x_first, *supporti;
  nusmv_double tmp_d = PLUS_INFTY;
  nusmv_ptrint tmp_d_i;

  one = bdd_one(dd);
  if (x_m == one) {
    bdd_free(dd, one);
    return add_max_min_conditioned(dd, x_M, C, F, MAXIMUM);
  }
  zero = add_zero(dd);
  zero_bdd = bdd_zero(dd);

  x_first = bdd_index(dd, x_m);

  /* C_0 = C(<0, x_{-1}>) */
  C_0 = bdd_compose(dd, C, zero_bdd, x_first);
  x_m_minus_1 = bdd_compose(dd, x_m, one, x_first);
  x_m_minus_1_plus_x_M = bdd_and(dd, x_m_minus_1, x_M);
  /* Exists_C_0 = \exists x_{-1} C(<0, x_{-1}>) */
  bdd_classifysupport(dd, C_0, x_m_minus_1_plus_x_M, &support[0], &support[1], &support[2]);
  bdd_free(dd, support[0]);
  bdd_free(dd, support[1]);
  if (x_m_minus_1_plus_x_M != support[2])/* or support[0] != bdd_zero... */
    Exists_C_0 = bdd_forsome(dd, C_0, x_m_minus_1_plus_x_M);
  else
    Exists_C_0 = bdd_dup(C_0);
  bdd_free(dd, support[2]);

  /* C_1 = C(<1, x_{-1}>) */
  C_1 = bdd_compose(dd, C, one, x_first);
  /* Exists_C_1 = \exists x_{-1} C(<1, x_{-1}>) */
  bdd_classifysupport(dd, C_1, x_m_minus_1_plus_x_M, &support[0], &support[1], &support[2]);
  bdd_free(dd, support[0]);
  bdd_free(dd, support[1]);
  if (x_m_minus_1_plus_x_M != support[2])
    Exists_C_1 = bdd_forsome(dd, C_1, x_m_minus_1_plus_x_M);
  else
    Exists_C_1 = bdd_dup(C_1);
  bdd_free(dd, support[2]);
  bdd_free(dd, x_m_minus_1_plus_x_M);

  CPY_DOUBLE_INT(tmp_d_i, tmp_d);
  glob_eval_as_double_leaf = 1;
  add_infty = add_leaf(dd, NODE_FROM_INT(tmp_d_i));
  glob_eval_as_double_leaf = 0;

  if (Exists_C_0 == zero_bdd && Exists_C_1 == zero_bdd) {
    bdd_free(dd, Exists_C_1);
    bdd_free(dd, Exists_C_0);
    bdd_free(dd, zero_bdd);
    bdd_free(dd, one);
    add_free(dd, zero);
    bdd_free(dd, C_0);
    bdd_free(dd, x_m_minus_1);
    bdd_free(dd, C_1);
    return add_infty;
  }

  if (add_isleaf(F)) {
    bdd_free(dd, Exists_C_1);
    bdd_free(dd, Exists_C_0);
    bdd_free(dd, zero_bdd);
    bdd_free(dd, one);
    add_free(dd, zero);
    bdd_free(dd, C_0);
    bdd_free(dd, x_m_minus_1);
    bdd_free(dd, C_1);
    add_free(dd, add_infty);
    add_ref(F);
    return F;
  }
  bdd_free(dd, zero_bdd);

  /* F_0 = F(<0, x_{-1}>, y) */
  F_0 = add_compose(dd, F, zero, x_first);
  add_free(dd, zero);
  /* ThenRecur = add_max_min_together_conditioned_recur(dd, x_{-1}, C_0, F_0, which); */
  ThenRecur = add_max_min_together_conditioned_recur(dd, x_M, x_m_minus_1, C_0, F_0);
  add_free(dd, F_0);
  bdd_free(dd, C_0);
  add_Exists_C_0 = bdd_to_add(dd, Exists_C_0);
  bdd_free(dd, Exists_C_0);
  Theta[0] = add_ifthenelse(dd, add_Exists_C_0, ThenRecur, add_infty);
  add_free(dd, add_Exists_C_0);

  /* F_1 = F(<1, x_{-1}>, y) */
  F_1 = add_compose(dd, F, one, x_first);
  bdd_free(dd, one);
  /* ElseRecur = add_max_min_together_conditioned_recur(dd, x_{-1}, C_1, F_1, which); */
  ElseRecur = add_max_min_together_conditioned_recur(dd, x_M, x_m_minus_1, C_1, F_1);
  add_free(dd, F_1);
  bdd_free(dd, C_1);
  bdd_free(dd, x_m_minus_1);
  add_Exists_C_1 = bdd_to_add(dd, Exists_C_1);
  bdd_free(dd, Exists_C_1);
  Theta[1] = add_ifthenelse(dd, add_Exists_C_1, ElseRecur, add_infty);
  add_free(dd, add_infty);
  add_free(dd, add_Exists_C_1);

  /* Delta = ADD_GT(ThenRecur, ElseRecur) */
  glob_eval_as_double_leaf = 1;
  Delta = add_apply(dd, node_gt, Theta[0], Theta[1]);
  glob_eval_as_double_leaf = 0;

  /* Gamma = ADD_ITE(Delta, ThenRecur, ElseRecur) */
  Gamma = add_ifthenelse(dd, Delta, Theta[1], Theta[0]);

  add_free(dd, Delta);
  add_free(dd, ThenRecur);
  add_free(dd, ElseRecur);
  add_free(dd, Theta[0]);
  add_free(dd, Theta[1]);

  return Gamma;
}

void PrintTop(const char *string)
{
  char to_be_exec[1024]; 
  sprintf(to_be_exec, "cat /proc/%d/stat | awk '{printf(\"%%lf seconds, %%s bytes of memory usage and \", $14/100, $23)}'; cat /proc/%d/status | grep VmPeak | awk '{printf(\"%%s kB of memory peak\\n\", $2)}'", getpid(), getpid());
  fprintf(nusmv_stdout, "Execution time and memory used by %s %s: ", PACKAGE_STRING, string);
  fflush(nusmv_stdout);
  system(to_be_exec);
  fflush(nusmv_stdout);
  fprintf(nusmv_stdout, "BDD nodes allocated: %d\n", get_dd_nodes_allocated(Prop_master_get_mechanism()->dd));
}

static void needed_bdds_adds_coal(DdManager *dd, BddVarSet_ptr *presents_cube, BddVarSet_ptr *inputs_cube, BddVarSet_ptr *nexts_cube, 
				  int *nexts_num, int *inputs_num, int *max_nexts_inputs_num, int **support_presents_cube_incr, 
				  int **support_nexts_cube_incr, int agents, bdd_ptr **word_plus_bit, int *index_w_from, 
				  int *index_w_to, int *b_local_index, int num_bits_sum, bdd_ptr *b_cube, bdd_ptr *q_cube, 
				  bdd_ptr *b, bdd_ptr *q, add_ptr **fresh_vars, bdd_ptr **action_fresh_vars, all_agents_info *agents_info)
{
  int next_first, i, *support_all_nexts_cube, *support_all_presents_cube;
  BddVarSet_ptr all_presents_cube, all_nexts_cube;
  unsigned count;
  indexes_list L;

  *b_cube = reuse_vars_and_return_cube_b(dd, b, agents, agents_info->num_glob_agents? 0 : -1, agents_info);
  *q_cube = reuse_vars_and_return_cube_q(dd, q, agents, agents_info);
  *inputs_cube = BddEnc_get_input_vars_cube(Enc_get_bdd_encoding());
  all_presents_cube = BddEnc_get_state_vars_cube(Enc_get_bdd_encoding());
  all_nexts_cube = BddEnc_get_next_state_vars_cube(Enc_get_bdd_encoding());
  support_all_nexts_cube = Cudd_SupportIndex(dd, all_nexts_cube); 
  support_all_presents_cube = Cudd_SupportIndex(dd, all_presents_cube); 
  /* exclude the variables for h, byz and q */
  for (L = agents_info->h_coal_indexes, count = 0; L; count++, L = L->next) {
    if (count%2)
      support_all_nexts_cube[L->index] = 2;
    else
      support_all_presents_cube[L->index] = 2;
  }
  for (i = 0; i < agents; i++) {
    for (count = 0; count < 2*agents; count++) {
      if (count%2) {
	support_all_nexts_cube[agents_info->byz_indexes[count]] = 2;
	support_all_nexts_cube[agents_info->coal_indexes[count]] = 2;
      }
      else {
	support_all_presents_cube[agents_info->byz_indexes[count]] = 2;
	support_all_presents_cube[agents_info->coal_indexes[count]] = 2;
      }
    }
  }
  *nexts_num = dd_get_size(dd);	/* of course, it ideally coincides with present_num... */
  for (i = 0; i < (*nexts_num); i++) {
    if (support_all_nexts_cube[i] == 0) support_all_nexts_cube[i] = 2;
    if (support_all_presents_cube[i] == 0) support_all_presents_cube[i] = 2;
  }
  *presents_cube = bdd_array2bdd(dd, support_all_presents_cube);
  *nexts_cube = bdd_array2bdd(dd, support_all_nexts_cube);
  *nexts_num = Cudd_SupportSize(dd, *nexts_cube);
  *support_nexts_cube_incr = ALLOC(int, *nexts_num);
  *support_presents_cube_incr = ALLOC(int, *nexts_num);
  for (i = 0, next_first = 0; i < (*nexts_num); i++) {
    for (; support_all_nexts_cube[next_first] != 1; next_first++);
    (*support_nexts_cube_incr)[i] = next_first++;
  }
  for (i = 0, next_first = 0; i < (*nexts_num); i++) {
    for (; support_all_presents_cube[next_first] != 1; next_first++);
    (*support_presents_cube_incr)[i] = next_first++;
  }
  FREE(support_all_presents_cube);
  FREE(support_all_nexts_cube);
  *word_plus_bit = bdd_word_plus_bit(dd, num_bits_sum, index_w_from, index_w_to, b_local_index);
  *inputs_num = 0;
  for (i = 0; i < agents; i++)
    for (L = agents_info->action_indexes[i]; L; L = L->next)
      (*inputs_num)++;
  *max_nexts_inputs_num = (*inputs_num) > (*nexts_num)? (*inputs_num) : (*nexts_num);
  *fresh_vars = ALLOC(add_ptr, *max_nexts_inputs_num);
  add_create_vars(dd, *fresh_vars, *max_nexts_inputs_num);
  *action_fresh_vars = ALLOC(bdd_ptr, *inputs_num);
  create_vars(dd, *action_fresh_vars, *inputs_num, -1);
}

void compute_G(DdManager *dd, bdd_ptr *G, add_ptr *q, bdd_ptr inputs_cube, bdd_ptr *inputs_tilde_cube, 
               bdd_ptr *action_fresh_vars, all_agents_info *agents_info, int agents, int **support_inputs_cube_incr)
{
  int i, count = 0, next_first;
  int *vars_array, *support_all_inputs_cube;
  indexes_list L;

  vars_array = ALLOC(int, dd_get_size(dd));
  memset(vars_array, 2, sizeof(int)*dd_get_size(dd));      
  for (i = 0; i < agents; i++) {
    for (L = agents_info->action_indexes[i]; L; L = L->next) {
      /*G[count] = bdd_ite(dd, q[i], bdd_new_var_with_index(dd, L->index), action_fresh_vars[count]);*/
      G[count] = bdd_ite(dd, q[i], action_fresh_vars[count], bdd_new_var_with_index(dd, L->index));
      if (opt_verbose_level_gt(options, 3)) {
	printf("G[%d] is:\n", count);
	cuddP(dd, G[count]);
      }
      vars_array[bdd_index(dd, action_fresh_vars[count])] = 1;
      count++;
    }
  }
  *inputs_tilde_cube = bdd_array2bdd(dd, vars_array);
  FREE(vars_array);
  *support_inputs_cube_incr = ALLOC(int, count);
  support_all_inputs_cube = Cudd_SupportIndex(dd, inputs_cube); 
  for (i = 0; i < count; i++) {
    if (support_all_inputs_cube[i] == 0) 
      support_all_inputs_cube[i] = 2;
  }
  for (i = 0, next_first = 0; i < count; i++) {
    for (; support_all_inputs_cube[next_first] != 1; next_first++);
    (*support_inputs_cube_incr)[i] = next_first++;
  }
  FREE(support_all_inputs_cube);
}

bdd_ptr apply_G(DdManager *dd, bdd_ptr *G, bdd_ptr A, int inputs_num, int *support_inputs_cube_incr)
{
  bdd_ptr res, tmp;
  int i;

  res = bdd_dup(A);
  for (i = 0; i < inputs_num; i++) {
    tmp = bdd_compose(dd, res, G[i], support_inputs_cube_incr[i]);
    bdd_free(dd, res);
    res = tmp;
  }
  return res;
}

#if 1
add_ptr apply_G_add(DdManager *dd, bdd_ptr *G, add_ptr A, int inputs_num, int *support_inputs_cube_incr)
{
  add_ptr res, tmp, tmp2;
  int i;

  res = add_dup(A);
  for (i = 0; i < inputs_num; i++) {
#if 0
    tmp = add_compose(dd, res, add_one(dd), 19);
    /*tmp2 = add_compose(dd, tmp, add_zero(dd), 21);
    add_free(dd, tmp);
    tmp = add_compose(dd, tmp2, add_zero(dd), 11);
    add_free(dd, tmp2);
    tmp2 = add_compose(dd, tmp, add_zero(dd), 13);
    add_free(dd, tmp);
    tmp = add_compose(dd, tmp2, add_zero(dd), 7);
    add_free(dd, tmp2);
    tmp2 = add_compose(dd, tmp, add_zero(dd), 9);
    add_free(dd, tmp);
    res = tmp2;*/
    res = tmp;
#endif
#if 1
    tmp2 = bdd_to_add(dd, G[i]);
    bdd_free(dd, G[i]);
    tmp = add_compose(dd, res, tmp2, support_inputs_cube_incr[i]);
    add_free(dd, res);
    if (opt_verbose_level_gt(options, 3)) {
      printf("tmp2 is:\n");
      cuddPdoubles(dd, tmp2);
      printf("tmp is:\n");
      cuddPdoubles(dd, tmp);
    }
    add_free(dd, tmp2);
#else /* segfaults */
    tmp = add_compose(dd, res, G[i], support_inputs_cube_incr[i]);
    if (opt_verbose_level_gt(options, 3)) {
      printf("tmp is:\n");
      cuddPdoubles(dd, tmp);
    }
    bdd_free(dd, G[i]);
    add_free(dd, res);
#endif
    res = tmp;
  }
  return res;
}
#else
add_ptr apply_G_add(DdManager *dd, bdd_ptr *G, add_ptr A, bdd_ptr *q, all_agents_info *agents_info, int agents, 
                    int *support_inputs_cube_incr)
{
  add_ptr res, tmp, tmp2, tmp3, tmp4, tmp_q, zero_add, one_add;
  int i, count;
  indexes_list L;

  res = bdd_dup(A);
  zero_add = add_zero(dd);
  one_add = add_one(dd);
  if (!add_isleaf(A)) {
    for (i = 0, count = 0; i < agents; i++) {
      tmp_q = bdd_to_add(dd, q[i]);
      for (L = agents_info->action_indexes[i]; L; L = L->next) {
        tmp = add_compose(dd, res, one_add, add_index(dd, q[i]));
	tmp3 = bdd_to_add(dd, G[count++]);
	tmp2 = add_compose(dd, tmp, tmp3, support_inputs_cube_incr[i]);
        add_free(dd, tmp);
        tmp = add_compose(dd, res, zero_add, add_index(dd, q[i]));
	tmp4 = add_compose(dd, tmp, tmp3, support_inputs_cube_incr[i]);
        add_free(dd, tmp);
        add_free(dd, tmp3);
        add_free(dd, res);
        res = add_ifthenelse(dd, tmp_q, tmp2, tmp4);
        add_free(dd, tmp2);
        add_free(dd, tmp4);
      }
      add_free(dd, tmp_q);
    }
  }
  add_free(dd, zero_add);
  add_free(dd, one_add);
  return res;
}
#endif

