/**CHeaderFile*****************************************************************

  FileName    [agentsReadComments.h]

  PackageName [agents]

  Synopsis    [The header file to read comments on the NuSMV.bar input file.]

  Description [The header file to read comments on the NuSMV.bar input file.]

  Author      [Igor Melatti, Federico Mari]

  Copyright   []

******************************************************************************/

#ifndef _AGENTS_READ_COMMENTS_H
#define _AGENTS_READ_COMMENTS_H
#include "util.h" /* for EXTERN and ARGS */
#include "opt/opt.h"
#include "node/node.h"

/*---------------------------------------------------------------------------*/
/* Constant declarations                                                     */
/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/* Type declarations                                                         */
/*---------------------------------------------------------------------------*/

/**Struct**********************************************************************

  Synopsis    [The data structure for agent index - variable name binding.]

  Description [The data structure for agent index - variable name binding is a 
  list of pairs (var name, agent id), thus it contains the following fields:
  <ul>
  <li><b>var_name</b></li>
  <li><b>agent</b></li>
  <li><b>next</b></li>
  </ul>]

  SeeAlso     []

******************************************************************************/
typedef struct agent_var_list_node {
  char *var_name;
  unsigned agent;
  struct agent_var_list_node *next;
} agent_var_list_node;

/**Type***********************************************************************

  Synopsis    [Definition of the public accessor for data structure for 
  agent index - variable name binding.]

  Description []

******************************************************************************/
typedef agent_var_list_node *agent_var_list;

/**Struct**********************************************************************

  Synopsis    [List of indexes.]

  Description []

  SeeAlso     []

******************************************************************************/
typedef struct indexes_list_node {
  int index;
  struct indexes_list_node *next;
} indexes_list_node;

/**Type***********************************************************************

  Synopsis    [Definition of the public accessor for the list of indexes.]

  Description []

******************************************************************************/
typedef indexes_list_node *indexes_list;

/**Struct**********************************************************************

  Synopsis    [The data structure containing all the needed informations about
  vars, var indexes and their relationships with agents.]

  Description [The agents data structure contains the following fields:</br>
  <dl>
  <dt><code>byz_names</code>
    <dd>byz_names[<em>i</em>] contains the name of the (boolean) variable 
    denoting if agent <em>i</em> is byzantine or not.</dd></dt>
  <dt><code>byz_indexes</code>
    <dd>byz_indexes[<em>2*i</em>] contains the index of the (boolean) 
    current state variable denoting if agent <em>i</em> is byzantine or not.</dd></dt>
    <dd>byz_indexes[<em>2*i + 1</em>] contains the index of the (boolean) 
    next state variable denoting if agent <em>i</em> is byzantine or not.</dd></dt>
  <dt><code>coal_names</code>
    <dd>coal_names[<em>i</em>] contains the name of the (boolean) variable 
    denoting if agent <em>i</em> is part of a coalition or not. Only used in coalition
    mode.</dd></dt>
  <dt><code>coal_indexes</code>
    <dd>coal_indexes[<em>2*i</em>] contains the index of the (boolean) 
    current state variable denoting if agent <em>i</em> is part of a coalition or not.</dd></dt>
    <dd>coal_indexes[<em>2*i + 1</em>] contains the index of the (boolean) 
    next state variable denoting if agent <em>i</em> is part of a coalition or not.
    Only used in coalition mode.</dd></dt>
  <dt><code>action_names</code>
    <dd>action_names[<em>i</em>] contains the name of the IVAR variable for 
    agent <em>i</em>.</dd></dt>
  <dt><code>action_indexes</code>
    <dd>action_indexes[<em>i</em>] contains the indexes of the (boolean flattened) 
    IVAR variables for agent <em>i</em> is byzantine or not.</dd></dt>
  <dt><code>h_names</code>
    <dd>h_names[<em>i</em>] contains the name of the variable modeling 
    agent <em>i</em> payoff.</dd></dt>
  <dt><code>h_indexes</code>
    <dd>h_indexes[<em>i</em>] contains the indexes of the (boolean flattened) 
    variables modeling agent <em>i</em> payoff (only for efficiency).</dd></dt>
  <dt><code>h_coal_name</code>
    <dd>h_coal_name contains the name of the variable modeling agents payoff, 
    also taking into account coalitions. Only used in coalition
    mode.</dd></dt></dd></dt>
  <dt><code>h_coal_indexes</code>
    <dd>h_coal_indexes contains the indexes of the (boolean flattened) 
    variables modeling agents payoff, also taking into account coalitions 
    (only for efficiency). Only used in coalition
    mode. If h_coal_name and h_coal_indexes are used, then 
    h_names and h_indexes are not used.</dd></dt>
  <dt><code>num_agents</code></dt>
    <dd>Number of agents (including global agent if present).</dd></dt>
  <dt><code>num_glob_agents</code>
    <dd>Will be 1 if the an additional agent for global variables is used,
    0 otherwise.</dd></dt>
  <dt><code>L</code>
    <dd>List for agent index - variable name binding.</dd></dt>
  </dl>]

  SeeAlso     []

******************************************************************************/
typedef struct all_agents_info {
  char **byz_names;
  int *byz_indexes;
  char **coal_names;
  int *coal_indexes;
  char **action_names;
  indexes_list *action_indexes;
  char **h_names;
  indexes_list *h_indexes;
  char *h_coal_name;
  indexes_list h_coal_indexes;
  unsigned num_agents;
  unsigned num_glob_agents;
  agent_var_list L;
} all_agents_info;

/**Type***********************************************************************

  Synopsis    [Used by Agents_insert_variable_index_from_node to distinguish 
  between two modes of execution.]

  Description []

******************************************************************************/
typedef enum {handle_a, handle_qbh} a_qbh;

/*---------------------------------------------------------------------------*/
/* Structure declarations                                                    */
/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/* Variable declarations                                                     */
/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/* Macro declarations                                                        */
/*---------------------------------------------------------------------------*/

/**AutomaticStart*************************************************************/

/*---------------------------------------------------------------------------*/
/* Function prototypes                                                       */
/*---------------------------------------------------------------------------*/

EXTERN void Agents_set_null_agents_info ARGS(());
EXTERN all_agents_info *Agents_get_agents_info ARGS((options_ptr));
EXTERN all_agents_info *Agents_only_get_agents_info ARGS((options_ptr));
EXTERN void Agents_agents_info_destroy ARGS(());
EXTERN int Agents_insert_variable_index_from_node ARGS((node_ptr, a_qbh, options_ptr, int, int));
EXTERN int Agents_get_agent_from_variable_node_h ARGS((node_ptr, options_ptr));
EXTERN char Agents_get_agent_from_variable_node ARGS((node_ptr expr, options_ptr options, int *));
EXTERN void PutTheRealValue ARGS((node_ptr *n));
EXTERN nusmv_double GetTheRealValue ARGS((node_ptr n));

/**AutomaticEnd***************************************************************/

#endif /* _AGENTS_READ_COMMENTS_H */
