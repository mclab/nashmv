/**CHeaderFile*****************************************************************

  FileName    [agentsUtils.h]

  PackageName [agents]

  Synopsis    [The public interface to the bar agents utilities]

  Description []

  SeeAlso     []

  Author      [Igor Melatti, Federico Mari]

  Copyright   []

  Revision    []

******************************************************************************/

#ifndef _AGENTS_UTILS__H
#define _AGENTS_UTILS__H

#include "utils/utils.h"
#include "utils/list.h"
#include "utils/ucmd.h"

/*---------------------------------------------------------------------------*/
/* Constant declarations                                                     */
/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/* Type declarations                                                         */
/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/* Structure declarations                                                    */
/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/* Variable declarations                                                     */
/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/* Macro declarations                                                        */
/*---------------------------------------------------------------------------*/

/**AutomaticStart*************************************************************/

/*---------------------------------------------------------------------------*/
/* Function prototypes                                                       */
/*---------------------------------------------------------------------------*/
EXTERN int CheckRationals ARGS((int, nusmv_double *, nusmv_double, nusmv_double *, nusmv_double *));
EXTERN int CheckRationalsCoal ARGS((int, int, nusmv_double, nusmv_double, nusmv_double, nusmv_double *));
EXTERN int CheckRationalsCoalExpl ARGS((int, int, nusmv_double, nusmv_double, nusmv_double, nusmv_double *, nusmv_double *));
EXTERN int CheckRationals_try_to_simplify ARGS((int, nusmv_double *, nusmv_double, nusmv_double *, nusmv_double *));
EXTERN int CheckRationalsCoal_try_to_simplify ARGS((int, int, nusmv_double, nusmv_double, nusmv_double, nusmv_double *));
EXTERN int CheckRationalsCoalExpl_try_to_simplify ARGS((int, int, nusmv_double, nusmv_double, nusmv_double, nusmv_double *));
EXTERN int CommandBuildModelBarAgents ARGS((int argc, char **argv));
EXTERN int CommandVerifyAgents ARGS((int argc, char **argv));
EXTERN void Agents_Init ARGS((void));
EXTERN void Agents_End ARGS((void));
EXTERN void PrintTop ARGS((const char *string));
EXTERN char ** GetVariableNames ARGS((DdManager *dd));
EXTERN int DefinitelyDump ARGS((DdManager *dd, int (*dump_fn)(DdManager *, int, DdNode **, char **, char **, FILE *), DdNode **node, char *node_name, char *filename, int));
EXTERN int q_not_valid ARGS((unsigned long q, int max_coal, int num_agents));
EXTERN char *print_q ARGS((unsigned long q, int num_agents));

/**AutomaticEnd***************************************************************/

#endif /* _AGENTS_UTILS__H */
