/**CFile***********************************************************************

  FileName    [agentCmd.c]

  PackageName [agent]

  Synopsis    [Agent commands]

  Description []

  Author      [Federico Mari, Igor Melatti]

  Copyright   []

******************************************************************************/

#if HAVE_CONFIG_H
# include "config.h"
#endif

#include "parser/symbols.h" 
#include "utils/error.h"
#include "cmd/cmd.h"
#include "utils/utils_io.h"
#include "compile/compile.h"
#include "agents/agentsMechanism.h"
#include "opt/opt.h"
#include "agents/agentsReadComments.h"
#include "agents/agentsUtils.h"

#ifndef BUFFER_LENGTH 
#define BUFFER_LENGTH 1024
#endif

/* prototypes of the command functions */
int CommandBuildModelBarAgents ARGS((int argc, char **argv));
int CommandVerifyAgents ARGS((int argc, char **argv));

/*---------------------------------------------------------------------------*/
/* Constant declarations                                                     */
/*---------------------------------------------------------------------------*/


/*---------------------------------------------------------------------------*/
/* Type declarations                                                         */
/*---------------------------------------------------------------------------*/


/*---------------------------------------------------------------------------*/
/* Structure declarations                                                    */
/*---------------------------------------------------------------------------*/


/*---------------------------------------------------------------------------*/
/* Variable declarations                                                     */
/*---------------------------------------------------------------------------*/
EXTERN cmp_struct_ptr cmps;
EXTERN options_ptr options;
EXTERN FsmBuilder_ptr global_fsm_builder;


/*---------------------------------------------------------------------------*/
/* Macro declarations                                                        */
/*---------------------------------------------------------------------------*/


/**AutomaticStart*************************************************************/

/*---------------------------------------------------------------------------*/
/* Static function prototypes                                                */
/*---------------------------------------------------------------------------*/
static void compile_create_flat_model_agents_version();
static int UsageBuildModelBarAgents ARGS((void));
static int UsageVerifyAgents ARGS((void));
static int CheckOptsGiven ARGS((int *, int *, nusmv_double *, nusmv_double *, nusmv_double *, nusmv_double *));
static void read_beta_values_from_file ARGS((Mechanism_ptr, nusmv_double *, char *));

/**AutomaticEnd***************************************************************/


/*---------------------------------------------------------------------------*/
/* Definition of exported functions                                          */
/*---------------------------------------------------------------------------*/

/**Function********************************************************************

  Synopsis           []

  Description        []

  SideEffects        []

******************************************************************************/
void Agents_Init(void){
  Cmd_CommandAdd("build_model_bar_agents", CommandBuildModelBarAgents, 0, false);
  Cmd_CommandAdd("verify_agents", CommandVerifyAgents, 0, false);
  Agents_set_null_agents_info();
}

/**Function********************************************************************

  Synopsis           []

  Description        []

  SideEffects        []

  SeeAlso            []

******************************************************************************/
void Agents_End(void)
{ 
  /*Mechanism_destroy(Prop_master_get_mechanism());*/ /* done in prop_deinit */
  Agents_agents_info_destroy(Agents_only_get_agents_info(options));
}

/**Function********************************************************************

  Synopsis           []

  CommandName        []         

  CommandSynopsis    []  

  CommandArguments   []

  CommandDescription []

  SideEffects        []

******************************************************************************/
int CommandBuildModelBarAgents(int argc, char **argv)
{
  int c;
  boolean force_build = false;

  if (!(opt_bar_agents_ICBFT(options) || opt_bar_agents_coal_ICBFT(options))) {
    fprintf(nusmv_stderr, 
      "Command \"build_model_bar_agents\" can only be used if bar_agents_ICBFT or bar_agents_coal_ICBFT options have been set\n");
    return 1;
  }

  if (opt_bar_agents_ICBFT(options) && opt_bar_agents_coal_ICBFT(options)) {
    fprintf(nusmv_stderr, 
      "Options bar_agents_ICBFT and bar_agents_coal_ICBFT cannot be set together\n");
    return 1;
  }

  util_getopt_reset();
  while((c = util_getopt(argc,argv,"fh")) != EOF){
    switch(c){
    case 'f': {
      force_build = true;
      break;
    }
    case 'h': return(UsageBuildModelBarAgents());
    default:  return(UsageBuildModelBarAgents());
    }
  }
  if (argc != util_optind) return(UsageBuildModelBarAgents());

  /* pre-conditions: */
  if (Compile_check_if_encoding_was_built(nusmv_stderr)) return 1;

  if (!force_build && cmp_struct_get_build_model_bar_agents(cmps)) {
    fprintf(nusmv_stderr, "A model appears to be already built from file: %s.\n",
            get_input_file(options));
    return 1;
  }

  {
    SexpFsm_ptr sexp_fsm;

    /* creates the model only if required (i.e. build_flat_model not called) */
    compile_create_flat_model_agents_version();

    if (opt_verbose_level_gt(options, 1)) {
      fprintf(nusmv_stderr, "\nCreating the BDD FSM...\n");
    }

    sexp_fsm = Prop_master_get_scalar_sexp_fsm();
    
    /* also stores the generated mechanism in Prop_master */
    (void)FsmBuilder_create_bdd_fsm(global_fsm_builder, 
                                    Enc_get_bdd_encoding(), 
                                    sexp_fsm,
                                    get_partition_method(options));

    if (opt_verbose_level_gt(options, 1)) {
      fprintf(nusmv_stderr, "Successfully created the BDD FSM\n");
    }
  }

  if (opt_verbose_level_gt(options, 0)) {
    fprintf(nusmv_stderr,
            "\nThe model has been built from file %s.\n", get_input_file(options));
  }
  /* We keep track that the master mechanism has been built. */
  cmp_struct_set_build_model_bar_agents(cmps);
  return 0;
}

/**Function********************************************************************

  Synopsis           []

  CommandName        []         

  CommandSynopsis    []  

  CommandArguments   []

  CommandDescription []

  SideEffects        []

******************************************************************************/
int CommandVerifyAgents(int argc, char **argv)
{
  int c;
  int i = 0;
  int res, agents;
  int fail, pass;
  all_agents_info *agents_info;
  int max_byz = -1, alpha = -1, max_coal = -1;
  nusmv_double *beta = (nusmv_double *)NULL, epsilon = (nusmv_double)-1, delta = (nusmv_double)-1, beta_coal = (nusmv_double)-1;
  Mechanism_ptr mech = Prop_master_get_mechanism();

  if (!(opt_bar_agents_ICBFT(options) || opt_bar_agents_coal_ICBFT(options))) {
    fprintf(nusmv_stderr, 
      "Command \"verify_agents\" can only be used if bar_agents_ICBFT or bar_agents_coal_ICBFT options have been set\n");
    return 1;
  }

  if (opt_bar_agents_ICBFT(options) && opt_bar_agents_coal_ICBFT(options)) {
    fprintf(nusmv_stderr, 
      "Options bar_agents_ICBFT and bar_agents_coal_ICBFT cannot be set together\n");
    return 1;
  }

  if (opt_bar_agents_ICBFT(options) && mech != NULL) {
    beta = ALLOC(nusmv_double, mech->agents);
    if (opt_bar_agents_ICBFT_beta_file(options) != (char*)NULL)
      read_beta_values_from_file(mech, beta, opt_bar_agents_ICBFT_beta_file(options));
    else {
      for (c = 0; c < mech->agents; c++)
        beta[c] = (nusmv_double)-1;
    }
  }
  util_getopt_reset();
  while((c = util_getopt(argc,argv,"e:d:b:B:a:m:c:h")) != EOF){
    switch(c){
    case 'e': 
      {
        char *err_occ[1];
	char* str_epsilon; 

	str_epsilon = util_strsav(util_optarg);

        epsilon = strtod(str_epsilon,err_occ);
        if (strcmp(err_occ[0], "") != 0) {
          fprintf(nusmv_stderr, "Error: \"%s\" is not a valid value for the \"-e\" option of command verify_agents.\n", err_occ[0]);
          nusmv_exit(1);
        }
	
	FREE(str_epsilon);	
	break;
      }
    case 'd': 
      {
        char *err_occ[1];
	char* str_delta; 

	str_delta = util_strsav(util_optarg);

        delta = strtod(str_delta,err_occ);
        if (strcmp(err_occ[0], "") != 0) {
          fprintf(nusmv_stderr, "Error: \"%s\" is not a valid value for the \"-e\" option of command verify_agents.\n", err_occ[0]);
          nusmv_exit(1);
        }
	
	FREE(str_delta);	
	break;
      }
    case 'b': 
      {
        char *err_occ[1];
	char* str_beta; 

	if (++i > mech->agents) {
          fprintf(nusmv_stderr, "Error: \"-b\" option of command verify_agents given %d times, while there are %d agents.\n", 
	          i, mech->agents);
          nusmv_exit(1);
        }
	str_beta = util_strsav(util_optarg);

        beta[i - 1] = strtod(str_beta,err_occ);
        if (strcmp(err_occ[0], "") != 0) {
          fprintf(nusmv_stderr, "Error: \"%s\" is not a valid value for the \"-b\" option of command verify_agents.\n", err_occ[0]);
          nusmv_exit(1);
        }

	FREE(str_beta);
	break;
      }
    case 'B': 
      {
	char* str_file_beta; 

	str_file_beta = util_strsav(util_optarg);
	read_beta_values_from_file(mech, beta, str_file_beta);
	FREE(str_file_beta);
	break;
      }
    case 'm': 
      {
	char* str_max_byz; 

	str_max_byz = util_strsav(util_optarg);

	if (util_str2int(str_max_byz, &max_byz) != 0) {
 	  error_invalid_number(str_max_byz);
	  FREE(str_max_byz);	
	  return(UsageVerifyAgents());
	}
	
	if (max_byz < 0) {
 	  error_invalid_number(str_max_byz);
	  FREE(str_max_byz);	
	  return(UsageVerifyAgents());
	}
	
	FREE(str_max_byz);	
	break;
      }
    case 'c': 
      {
	char* str_max_coal; 

	str_max_coal = util_strsav(util_optarg);

	if (util_str2int(str_max_coal, &max_coal) != 0) {
 	  error_invalid_number(str_max_coal);
	  FREE(str_max_coal);	
	  return(UsageVerifyAgents());
	}
	
	if (max_coal < 0) {
 	  error_invalid_number(str_max_coal);
	  FREE(str_max_coal);	
	  return(UsageVerifyAgents());
	}
	
	FREE(str_max_coal);	
	break;
      }
    case 'h': return(UsageVerifyAgents());
    default:  return(UsageVerifyAgents());
    }
  }
  if (argc != util_optind) return(UsageVerifyAgents());
  
  /* pre-conditions: */
  if (!cmp_struct_get_build_model_bar_agents(cmps)) {
    fprintf(nusmv_stderr, "Please run \"build_model_bar_agents\" before \"verify_agents\".\n");
    return 0;
  }

  if (CheckOptsGiven(&max_byz, &max_coal, beta, &beta_coal, &epsilon, &delta)) return(UsageVerifyAgents());

  if (opt_bar_agents_ICBFT(options)) {
    nusmv_double *epsilon_1, *epsilon_2;

    agents = Prop_master_get_mechanism()->agents;
    epsilon_1 = ALLOC(nusmv_double, agents);
    epsilon_2 = ALLOC(nusmv_double, agents);
    PrintTop("before CheckRationals");
    if (!opt_bar_agents_ICBFT_try_to_simplify(options))
      res = CheckRationals(max_byz, beta, delta, epsilon_1, epsilon_2);
    else
      res = CheckRationals_try_to_simplify(max_byz, beta, delta, epsilon_1, epsilon_2);
    PrintTop("after CheckRationals");

    fprintf(nusmv_stdout, "======================  Nash Results  =====================\n");
    fprintf(nusmv_stdout, "The protocol is not epsilon-Nash with epsilon less than:\n");
    agents_info = Agents_get_agents_info(options);
    for (fail = 0, i = 0; i < agents; i++) {
      fprintf(nusmv_stdout, "Agent %d: %lg\n", i + 1 - agents_info->num_glob_agents, 
              sizeof(nusmv_double) == sizeof(double)? epsilon_1[i] : (double)epsilon_1[i]);
      if (epsilon_1[i] > epsilon)
	fail = 1;
      if (opt_bar_agents_ICBFT_one_agent(options))
	break;
    }
    fprintf(nusmv_stdout, "-----------------------------------------------------------\n");
    fprintf(nusmv_stdout, "The protocol is epsilon-Nash with epsilon greater than:\n");
    for (pass = 1, i = 0; i < agents; i++) {
      fprintf(nusmv_stdout, "Agent %d: %lg\n", i + 1 - agents_info->num_glob_agents, 
              sizeof(nusmv_double) == sizeof(double)? epsilon_2[i] : (double)epsilon_2[i]);
      if (epsilon_2[i] >= epsilon)
	pass = 0;
      if (opt_bar_agents_ICBFT_one_agent(options))
	break;
    }
    fprintf(nusmv_stdout, "FINAL RESULT with epsilon=%le: %s", 
      sizeof(nusmv_double) == sizeof(double)? epsilon : (double)epsilon, fail? "FAIL\n" : "PASS with ");
    if (!fail)
      fprintf(nusmv_stdout, "%le\n", pass? (sizeof(nusmv_double) == sizeof(double)? epsilon : (double)epsilon) : 
        (sizeof(nusmv_double) == sizeof(double)? epsilon + delta : (double)(epsilon + delta)));
    fprintf(nusmv_stdout, "==========================  End  ==========================\n");

    FREE(epsilon_1);
    FREE(epsilon_2);
    FREE(beta);
  }
  else if (!opt_bar_agents_ICBFT_coal_expl(options)) {
    nusmv_double res_double;
    PrintTop("before CheckRationalsCoal");
    if (!opt_bar_agents_ICBFT_try_to_simplify(options))
      res = CheckRationalsCoal(max_byz, max_coal, beta_coal, delta, epsilon, &res_double);
    else
      res = CheckRationalsCoal_try_to_simplify(max_byz, max_coal, beta_coal, delta, epsilon, &res_double);
    PrintTop("after CheckRationalsCoal");

    fprintf(nusmv_stdout, "======================  Nash Results  =====================\n");
    fprintf(nusmv_stdout, "FINAL RESULT with epsilon=%le: %s", 
      sizeof(nusmv_double) == sizeof(double)? epsilon : (double)epsilon, res == 0? "FAIL\n" : "PASS with ");
    if (res)
      fprintf(nusmv_stdout, "%le\n", res == 1? (sizeof(nusmv_double) == sizeof(double)? epsilon : (double)epsilon) : 
        (sizeof(nusmv_double) == sizeof(double)? epsilon + delta : (double)(epsilon + delta)));
    fprintf(nusmv_stdout, "==========================  End  ==========================\n");
    res = 0;
  }
  else {
    nusmv_double *epsilon_1, *epsilon_2;
    unsigned long q;

    agents = Prop_master_get_mechanism()->agents;
    epsilon_1 = ALLOC(nusmv_double, 1 << agents);
    epsilon_2 = ALLOC(nusmv_double, 1 << agents);
    PrintTop("before CheckRationalsCoalExpl");
    /*if (!opt_bar_agents_ICBFT_try_to_simplify(options))*/
      res = CheckRationalsCoalExpl(max_byz, max_coal, beta_coal, delta, epsilon, epsilon_1, epsilon_2);
    /*else
      res = CheckRationalsCoalExpl_try_to_simplify(max_byz, max_coal, beta_coal, delta, epsilon, &res_double);*/
    PrintTop("after CheckRationalsCoalExpl");

    fprintf(nusmv_stdout, "======================  Nash Results  =====================\n");
    fprintf(nusmv_stdout, "The protocol is not epsilon-Nash with epsilon less than:\n");
    agents_info = Agents_get_agents_info(options);
    for (fail = 0, q = 1; q < (1 << agents); q++) {
      if (q_not_valid(q, max_coal, agents))
	continue;
      fprintf(nusmv_stdout, "Coalition %s: %lg\n", print_q(q, agents), 
              sizeof(nusmv_double) == sizeof(double)? epsilon_1[q] : (double)epsilon_1[q]);
      if (epsilon_1[q] > epsilon)
	fail = 1;
    }
    fprintf(nusmv_stdout, "-----------------------------------------------------------\n");
    fprintf(nusmv_stdout, "The protocol is epsilon-Nash with epsilon greater than:\n");
    for (pass = 1, q = 1; q < (1 << agents); q++) {
      if (q_not_valid(q, max_coal, agents))
	continue;
      fprintf(nusmv_stdout, "Coalition %s: %lg\n", print_q(q, agents), 
              sizeof(nusmv_double) == sizeof(double)? epsilon_2[q] : (double)epsilon_2[q]);
      if (epsilon_2[q] >= epsilon)
	pass = 0;
    }
    fprintf(nusmv_stdout, "FINAL RESULT with epsilon=%le: %s", 
      sizeof(nusmv_double) == sizeof(double)? epsilon : (double)epsilon, fail? "FAIL\n" : "PASS with ");
    if (!fail)
      fprintf(nusmv_stdout, "%le\n", pass? (sizeof(nusmv_double) == sizeof(double)? epsilon : (double)epsilon) : 
        (sizeof(nusmv_double) == sizeof(double)? epsilon + delta : (double)(epsilon + delta)));
    fprintf(nusmv_stdout, "==========================  End  ==========================\n");

    FREE(epsilon_1);
    FREE(epsilon_2);
  }

  return res;
}

/*---------------------------------------------------------------------------*/
/* Definition of static functions                                            */
/*---------------------------------------------------------------------------*/

/**Function********************************************************************

  Synopsis           []

  Description        []

  SideEffects        []

  SeeAlso            []

******************************************************************************/
static int UsageBuildModelBarAgents()
{
  fprintf(nusmv_stderr, "usage: build_model_bar_agents [-h] [-f]\n");
  fprintf(nusmv_stderr, "   -h \t\tPrints the command usage\n");
  fprintf(nusmv_stderr, "   -f \t\tForces the model re-construction, even if a model has already been built\n");
  return 1;
}

/**Function********************************************************************

  Synopsis           []

  Description        []

  SideEffects        []

  SeeAlso            []

******************************************************************************/
static int UsageVerifyAgents()
{
  char betas[BUFFER_LENGTH] = "";
  int i;
  all_agents_info *agents_info = Agents_get_agents_info(options);
  
  if (agents_info != NULL) {
    for (i = 0; i < agents_info->num_agents; i++)
      sprintf(betas, "%s[-b beta_%d] ", betas, i + 1 - agents_info->num_glob_agents);
  }
  else
    sprintf(betas, "[-b beta_i] ");
  fprintf(nusmv_stderr, "usage: verify_agents [-h] %s[-B file_beta] [-e epsilon] [-d delta] [-m max_byz]\n", betas);
  fprintf(nusmv_stderr, "   -h \t\t\tPrints the command usage\n");
  fprintf(nusmv_stderr, "   -b beta_i \t\tSets the i-th beta (discount) parameter\n");
  fprintf(nusmv_stderr, "   -B file \t\tFile name for beta (discount) parameters\n");
  fprintf(nusmv_stderr, "   -e epsilon \t\tSets the epsilon (tolerance) parameter\n");
  fprintf(nusmv_stderr, "   -d delta \t\tSets the epsilon (accuracy) parameter\n");
  fprintf(nusmv_stderr, "   -m max_byz \t\tSets the max_byz (maximum number of byzantine agents) parameter\n");
  return 0;
}

/**Function********************************************************************

  Synopsis           [Check if all the needed options have been given in some way.]

  Description        []

  SideEffects        []

  SeeAlso            []

******************************************************************************/
static int CheckOptsGiven(int *max_byz, int *max_coal, nusmv_double *beta, nusmv_double *beta_coal, 
                          nusmv_double *epsilon, nusmv_double *delta)
{
  int ret = 0, i, agents = Prop_master_get_mechanism()->agents;

  if (*max_byz == -1 && opt_bar_agents_ICBFT_maxbyz(options) == -1) {
    fprintf(nusmv_stderr, "Maximum number of byzantine agents not provided\n");
    ret = 1;
  }
  else if (*max_byz == -1)
    *max_byz = opt_bar_agents_ICBFT_maxbyz(options);

  if (*epsilon == (nusmv_double)-1 && opt_bar_agents_ICBFT_epsilon(options) == (nusmv_double)-1) {
    fprintf(nusmv_stderr, "Parameter epsilon (tolerance) not provided\n");
    ret = 1;
  }
  else if (*epsilon == (nusmv_double)-1)
    *epsilon = opt_bar_agents_ICBFT_epsilon(options);

  if (*delta == (nusmv_double)-1 && opt_bar_agents_ICBFT_delta(options) == (nusmv_double)-1) {
    fprintf(nusmv_stderr, "Parameter delta (tolerance) not provided\n");
    ret = 1;
  }
  else if (*delta == (nusmv_double)-1)
    *delta = opt_bar_agents_ICBFT_delta(options);

  if (opt_bar_agents_ICBFT(options)) {
    for (i = 0; i < agents && beta[i] != (nusmv_double)-1; i++);
    if (i < agents && opt_bar_agents_ICBFT_beta(options) == (nusmv_double)-1) {
      fprintf(nusmv_stderr, "Parameter beta (discount) not provided\n");
      ret = 1;
    }
    else if (opt_bar_agents_ICBFT_beta(options) != (nusmv_double)-1) {
      int j;
      for (j = i; j < agents; j++)
	beta[j] = opt_bar_agents_ICBFT_beta(options);
    }
  }
  else {
    if (opt_bar_agents_ICBFT_beta(options) == (nusmv_double)-1) {
      fprintf(nusmv_stderr, "Parameter beta (discount) not provided\n");
      ret = 1;
    }
    else
      *beta_coal = opt_bar_agents_ICBFT_beta(options);

    if (*max_coal == -1 && opt_bar_agents_ICBFT_maxcoal(options) == -1) {
      fprintf(nusmv_stderr, "Maximum size of coalitions not provided\n");
      ret = 1;
    }
    else if (*max_coal == -1)
      *max_coal = opt_bar_agents_ICBFT_maxcoal(options);
  }

  return ret;
}

/**Function********************************************************************

  Synopsis           [Creates the  master scalar fsm if needed. It only differs
  from the non-agent version for the last call. Moreover, FsmBuilder_create_sexp_fsm
  will behave differently than in the non-agent version.]

  Description        []

  SideEffects        []

******************************************************************************/
static void compile_create_flat_model_agents_version()
{ 

  SymbTable_ptr st;
  NodeList_ptr vars;
  SexpFsm_ptr sexp_fsm;

  if (Prop_master_get_scalar_sexp_fsm() != SEXP_FSM(NULL)) return;

  if (opt_verbose_level_gt(options, 1)) {
    fprintf(nusmv_stderr, "\nCreating the scalar FSM...\n");
  }

  st = Compile_get_global_symb_table();

  vars = SymbLayer_get_all_vars(SymbTable_get_layer(st, MODEL_LAYER_NAME));

  sexp_fsm = FsmBuilder_create_sexp_fsm(global_fsm_builder,
					  Enc_get_bdd_encoding(), 
					  SYMB_LAYER(NULL), /* scalar fsm */
					  SYMB_LAYER(NULL), /* no inlining */
					  NodeList_to_node_ptr(vars));
  
  /* Builds the sexp FSM of the whole read model */
  Prop_master_set_scalar_sexp_fsm(sexp_fsm);

  if (opt_verbose_level_gt(options, 1)) {
    fprintf(nusmv_stderr, "Successfully created the scalar FSM\n");
  }

  /* We keep track that the master FSM has been built. */
  cmp_struct_set_build_flat_model_bar_agents(cmps);
}

static void read_beta_values_from_file(Mechanism_ptr mech, nusmv_double *beta, char *namefile) 
{
  if (mech != (Mechanism_ptr)NULL) {
    FILE *stream = fopen(namefile, "r");
    int i;
  
    if (stream == (FILE*)NULL) {
      fprintf(nusmv_stderr, "Unable to open file for beta values %s\n", namefile);
      nusmv_exit(1);
    }
    for (i = 0; i < mech->agents; i++)
      fscanf(stream, "%le", &beta[i]);
    fclose(stream);
  }
  else
    fprintf(nusmv_stderr, "Mechanism uninitialized, unable to read beta values from file\n");
}
