static bdd_ptr compute_C_expl(DdManager *dd, bdd_ptr *b, unsigned long q, int max_byz, int max_coal, int agents, int num_bits_sum,
			      bdd_ptr *word_plus_bit, int index_w_from, int index_w_to, int b_local_index)
{
  bdd_ptr tmp, tmp2, tmp3, *sum, *sum_next;
  
  sum = ALLOC(bdd_ptr, num_bits_sum);
  sum_next = ALLOC(bdd_ptr, num_bits_sum);
  for (i = 0; i < agents; i++) {
    for (l = 0; l < num_bits_sum; l++) {
      /* the following part computes the l-th bit of sum + not(q[i])*b_i... */
      tmp = bdd_compose(dd, word_plus_bit[l], b[i], b_local_index);
      for (k = index_w_from; k <= index_w_to; k++) {
	tmp2 = bdd_compose(dd, tmp, sum[k - index_w_from], k);
	bdd_free(dd, tmp);
	tmp = tmp2;
      }
      /* ...and puts it in sum_next[l]... */
      sum_next[l] = tmp;
    }
  }
  tmp = bdd_word_int_leq(dd, sum, max_byz, num_bits_sum);

  for (i = 0; i < agents; i++) {
    for (l = 0; l < num_bits_sum; l++) {
      /* the following part computes the l-th bit of sum + q_i*b_i... */
      tmp = bdd_compose(dd, word_plus_bit[l], b[i], b_local_index);
      for (k = index_w_from; k <= index_w_to; k++) {
	tmp2 = bdd_compose(dd, tmp, sum[k - index_w_from], k);
	bdd_free(dd, tmp);
	tmp = tmp2;
      }
      /* ...and puts it in sum_next[l]... */
      sum_next[l] = tmp;
    }
  }
  for (i = 0; i < num_bits_sum; i++) {
    bdd_and_accumulate(dd, &tmp, );
  }
    
  FREE(sum);
  FREE(sum_next);
  return tmp;
}

