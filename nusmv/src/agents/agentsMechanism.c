#include "agentsMechanism.h"
#include "util.h"	/* for ALLOC and FREE */
#include "enc/enc.h"
#include "node/node.h"
#include "utils/ustring.h"
#include "parser/symbols.h"
#include <string.h>

/*Mechanism_ptr global_M = NULL;*/

Mechanism_ptr Mechanism_create(DdManager *manager, int num_agents, boolean without_coal)
{
  Mechanism_ptr mech;
  int i;

  mech = ALLOC(Mechanism, 1);
  mech->agents = num_agents;
  mech->dd = manager;
  
  mech->T = ALLOC(bdd_ptr, num_agents); /* T_0, T_1, ..., T_n */
  mech->B = ALLOC(bdd_ptr, num_agents); /* B_0, B_1, ..., B_n */
  if (without_coal)
    mech->h = ALLOC(add_ptr, num_agents); /* h_0, h_1, ..., h_n */
  else
    mech->h = (add_ptr *)NULL;
  mech->h_coal = (add_ptr)NULL;
  for (i = 0; i < num_agents; i++) {
    mech->B[i] = bdd_one(manager);
    mech->T[i] = (bdd_ptr)NULL;	/* IM: directly set */
    if (without_coal)
      mech->h[i] = (bdd_ptr)NULL;	/* IM: directly set */
  }

  return mech;
}

void Mechanism_destroy(Mechanism_ptr mech)
{
  int i;

  bdd_free(mech->dd, mech->I);
  for (i = 0; i < mech->agents; i++) {
    bdd_free(mech->dd, mech->B[i]);
    if (mech->T[i])
      bdd_free(mech->dd, mech->T[i]);
    if (mech->h && mech->h[i])
      add_free(mech->dd, mech->h[i]);
  }
  if (mech->h_coal)
    add_free(mech->dd, mech->h_coal);
  FREE(mech->h);
  FREE(mech->T);
  FREE(mech->B);
  FREE(mech);

  return;
}

void Mechanism_reset(Mechanism_ptr mech)
{
  int i;

  if (mech->I)
    bdd_free(mech->dd, mech->I);
  for (i = 0; i < mech->agents; i++) {
    if (mech->B[i]) /* should be useless */
      bdd_free(mech->dd, mech->B[i]);
    mech->B[i] = bdd_one(mech->dd);
    if (mech->T[i]) {
      bdd_free(mech->dd, mech->T[i]);
      mech->T[i] = (bdd_ptr)NULL;	/* IM: directly set */
    }
    if (mech->h && mech->h[i]) {
      add_free(mech->dd, mech->h[i]);
      mech->h[i] = (bdd_ptr)NULL;	/* IM: directly set */
    }
  }
  if (mech->h_coal)
    add_free(mech->dd, mech->h_coal);
}

void Mechanism_set_T_and_B(Mechanism_ptr mech, all_agents_info *agents_info) 
{
  int i, j;
  bdd_ptr zero = bdd_zero(mech->dd), one = bdd_one(mech->dd), tmp;
  
  for (i = 0; i < mech->agents; i++) {
    mech->T[i] = bdd_dup(mech->B[i]);
    for (j = 0; j < 2*mech->agents; j++) {
      tmp = bdd_compose(mech->dd, mech->T[i], zero, agents_info->byz_indexes[j]);
      bdd_free(mech->dd, mech->T[i]);
      mech->T[i] = tmp;
    }
    for (j = 0; j < 2*mech->agents; j++) {
      tmp = bdd_compose(mech->dd, mech->B[i], one, agents_info->byz_indexes[j]);
      bdd_free(mech->dd, mech->B[i]);
      mech->B[i] = tmp;
    }
  }
  bdd_free(mech->dd, zero);
  bdd_free(mech->dd, one);
}

