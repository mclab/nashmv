/**CHeaderFile*****************************************************************

  FileName    [agentsMechanism.h]

  PackageName [agents]

  Synopsis    [The header file defining the mechanism structure.]

  Description [The header file defining the mechanism structure.]

  Author      [Igor Melatti, Federico Mari]

  Copyright   []

******************************************************************************/

#ifndef _AGENTS_MECHANISM_H
#define _AGENTS_MECHANISM_H

#include "dd/dd.h"
#include "agents/agentsReadComments.h"

/*---------------------------------------------------------------------------*/
/* Constant declarations                                                     */
/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/* Type declarations                                                         */
/*---------------------------------------------------------------------------*/

/**Struct**********************************************************************

  Synopsis    [The data structure for the mechanism.]

  Description [The data structure for the mechanism contains the following fields:
  <dl>
  <dt><code>dd</code>
    <dd>Manager for BDD/ADD</dd></dt>
  <dt><code>agents</code>
    <dd>Total number of agents</dd></dt>
  <dt><code>I</code>
    <dd>BDD for (global) initial states</dd></dt>
  <dt><code>T</code>
    <dd>T[<em>i</em>] is the BDD containing the altruistic transition function 
    of agent <em>i</em></dd></dt>
  <dt><code>B</code>
    <dd>B[<em>i</em>] is the BDD containing the byzantine transition function 
    (underlying behavior) of agent <em>i</em></dd></dt>
  <dt><code>h</code>
    <dd>h[<em>i</em>] is the ADD containing the payoff function for agent 
    <em>i</em></dd></dt>
  </dl>]

  SeeAlso     []

******************************************************************************/
typedef struct mechanism_TAG {
  DdManager *dd;

  int agents;   /* number of agents */
  bdd_ptr I;	/* global init */
  bdd_ptr *T;   /* T_0, T_1, ..., T_{agents} */
  bdd_ptr *B;   /* B_0, B_1, ..., B_{agents} */
  add_ptr *h;   /* h_0, ..., h_{agents} */
  add_ptr h_coal;

} Mechanism;

/**Type***********************************************************************

  Synopsis    [Definition of the public accessor for the mechanism.]

  Description []

******************************************************************************/
typedef Mechanism * Mechanism_ptr;

/*---------------------------------------------------------------------------*/
/* Structure declarations                                                    */
/*---------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
/* Variable declarations                                                     */
/*---------------------------------------------------------------------------*/

EXTERN FILE* nusmv_stderr;
EXTERN FILE* nusmv_stdout;

/*---------------------------------------------------------------------------*/
/* Macro declarations                                                        */
/*---------------------------------------------------------------------------*/

/**AutomaticStart*************************************************************/

/*---------------------------------------------------------------------------*/
/* Function prototypes                                                       */
/*---------------------------------------------------------------------------*/

EXTERN Mechanism_ptr Mechanism_create ARGS((DdManager *, int, boolean));
EXTERN void Mechanism_destroy ARGS((Mechanism_ptr));
EXTERN void Mechanism_set_T_and_B ARGS((Mechanism_ptr, all_agents_info *));
EXTERN void Mechanism_reset ARGS((Mechanism_ptr mech));

/**AutomaticEnd***************************************************************/

#endif /* _AGENTS_MECHANISM_H */
