/* A Bison parser, made by GNU Bison 1.875.  */

/* Skeleton parser for Yacc-like parsing with Bison,
   Copyright (C) 1984, 1989, 1990, 2000, 2001, 2002 Free Software Foundation, Inc.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330,
   Boston, MA 02111-1307, USA.  */

/* As a special exception, when this file is copied by Bison into a
   Bison output file, you may use that output file without restriction.
   This special exception was added by the Free Software Foundation
   in version 1.24 of Bison.  */

/* Tokens.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
   /* Put the tokens into the symbol table, so that GDB and other debuggers
      know about them.  */
   enum yytokentype {
     TOK_CONSTRAINT = 258,
     TOK_GOTO = 259,
     TOK_MAXU = 260,
     TOK_MINU = 261,
     TOK_ABU = 262,
     TOK_EBU = 263,
     TOK_AU = 264,
     TOK_EU = 265,
     TOK_CONTEXT = 266,
     TOK_PROCESS = 267,
     TOK_MODULE = 268,
     TOK_COMPUTE = 269,
     TOK_LTLSPEC = 270,
     TOK_CTLSPEC = 271,
     TOK_SPEC = 272,
     TOK_INVAR = 273,
     TOK_AGENTS_TRANS = 274,
     TOK_TRANS = 275,
     TOK_INIT = 276,
     TOK_DEFINE = 277,
     TOK_IVAR = 278,
     TOK_VAR = 279,
     TOK_PSLSPEC = 280,
     TOK_CONSTANTS = 281,
     TOK_JUSTICE = 282,
     TOK_COMPASSION = 283,
     TOK_FAIRNESS = 284,
     TOK_INVARSPEC = 285,
     TOK_ASSIGN = 286,
     TOK_ISA = 287,
     TOK_SEMI = 288,
     TOK_CONS = 289,
     TOK_OF = 290,
     TOK_RCB = 291,
     TOK_LCB = 292,
     TOK_RB = 293,
     TOK_RP = 294,
     TOK_LP = 295,
     TOK_TWODOTS = 296,
     TOK_EQDEF = 297,
     TOK_SELF = 298,
     TOK_COLON = 299,
     TOK_ESAC = 300,
     TOK_CASE = 301,
     TOK_COMPWFF = 302,
     TOK_CTLWFF = 303,
     TOK_LTLPSL = 304,
     TOK_LTLWFF = 305,
     TOK_SIMPWFF = 306,
     TOK_INCONTEXT = 307,
     TOK_WORD = 308,
     TOK_REAL = 309,
     TOK_INTEGER = 310,
     TOK_BOOLEAN = 311,
     TOK_ARRAY = 312,
     TOK_WORD1 = 313,
     TOK_BOOL = 314,
     TOK_WAWRITE = 315,
     TOK_WAREAD = 316,
     TOK_TRUEEXP = 317,
     TOK_FALSEEXP = 318,
     TOK_ATOM = 319,
     TOK_NUMBER_WORD = 320,
     TOK_NUMBER_EXP = 321,
     TOK_NUMBER_REAL = 322,
     TOK_NUMBER_FRAC = 323,
     TOK_NUMBER = 324,
     TOK_NOT = 325,
     TOK_AND = 326,
     TOK_XNOR = 327,
     TOK_XOR = 328,
     TOK_OR = 329,
     TOK_IFF = 330,
     TOK_IMPLIES = 331,
     TOK_COMMA = 332,
     TOK_AA = 333,
     TOK_EE = 334,
     TOK_AG = 335,
     TOK_EG = 336,
     TOK_AF = 337,
     TOK_EF = 338,
     TOK_AX = 339,
     TOK_EX = 340,
     TOK_RELEASES = 341,
     TOK_TRIGGERED = 342,
     TOK_UNTIL = 343,
     TOK_SINCE = 344,
     TOK_MMAX = 345,
     TOK_MMIN = 346,
     TOK_BUNTIL = 347,
     TOK_ABG = 348,
     TOK_ABF = 349,
     TOK_EBG = 350,
     TOK_EBF = 351,
     TOK_OP_FUTURE = 352,
     TOK_OP_GLOBAL = 353,
     TOK_OP_NEXT = 354,
     TOK_OP_ONCE = 355,
     TOK_OP_HISTORICAL = 356,
     TOK_OP_NOTPRECNOT = 357,
     TOK_OP_PREC = 358,
     TOK_GE = 359,
     TOK_LE = 360,
     TOK_GT = 361,
     TOK_LT = 362,
     TOK_NOTEQUAL = 363,
     TOK_EQUAL = 364,
     TOK_RROTATE = 365,
     TOK_LROTATE = 366,
     TOK_RSHIFT = 367,
     TOK_LSHIFT = 368,
     TOK_SETIN = 369,
     TOK_UNION = 370,
     TOK_DIVIDE = 371,
     TOK_TIMES = 372,
     TOK_MINUS = 373,
     TOK_PLUS = 374,
     TOK_MOD = 375,
     TOK_CONCATENATION = 376,
     TOK_SMALLINIT = 377,
     TOK_NEXT = 378,
     TOK_BIT = 379,
     TOK_DOT = 380,
     TOK_LB = 381,
     TOK_PLAYER_2 = 382,
     TOK_PLAYER_1 = 383,
     TOK_GAME = 384,
     TOK_AVOIDTARGET = 385,
     TOK_REACHTARGET = 386,
     TOK_AVOIDDEADLOCK = 387,
     TOK_REACHDEADLOCK = 388,
     TOK_GENREACTIVITY = 389,
     TOK_BUCHIGAME = 390,
     TOK_MIRROR = 391,
     TOK_PREDSLIST = 392,
     TOK_PRED = 393
   };
#endif
#define TOK_CONSTRAINT 258
#define TOK_GOTO 259
#define TOK_MAXU 260
#define TOK_MINU 261
#define TOK_ABU 262
#define TOK_EBU 263
#define TOK_AU 264
#define TOK_EU 265
#define TOK_CONTEXT 266
#define TOK_PROCESS 267
#define TOK_MODULE 268
#define TOK_COMPUTE 269
#define TOK_LTLSPEC 270
#define TOK_CTLSPEC 271
#define TOK_SPEC 272
#define TOK_INVAR 273
#define TOK_AGENTS_TRANS 274
#define TOK_TRANS 275
#define TOK_INIT 276
#define TOK_DEFINE 277
#define TOK_IVAR 278
#define TOK_VAR 279
#define TOK_PSLSPEC 280
#define TOK_CONSTANTS 281
#define TOK_JUSTICE 282
#define TOK_COMPASSION 283
#define TOK_FAIRNESS 284
#define TOK_INVARSPEC 285
#define TOK_ASSIGN 286
#define TOK_ISA 287
#define TOK_SEMI 288
#define TOK_CONS 289
#define TOK_OF 290
#define TOK_RCB 291
#define TOK_LCB 292
#define TOK_RB 293
#define TOK_RP 294
#define TOK_LP 295
#define TOK_TWODOTS 296
#define TOK_EQDEF 297
#define TOK_SELF 298
#define TOK_COLON 299
#define TOK_ESAC 300
#define TOK_CASE 301
#define TOK_COMPWFF 302
#define TOK_CTLWFF 303
#define TOK_LTLPSL 304
#define TOK_LTLWFF 305
#define TOK_SIMPWFF 306
#define TOK_INCONTEXT 307
#define TOK_WORD 308
#define TOK_REAL 309
#define TOK_INTEGER 310
#define TOK_BOOLEAN 311
#define TOK_ARRAY 312
#define TOK_WORD1 313
#define TOK_BOOL 314
#define TOK_WAWRITE 315
#define TOK_WAREAD 316
#define TOK_TRUEEXP 317
#define TOK_FALSEEXP 318
#define TOK_ATOM 319
#define TOK_NUMBER_WORD 320
#define TOK_NUMBER_EXP 321
#define TOK_NUMBER_REAL 322
#define TOK_NUMBER_FRAC 323
#define TOK_NUMBER 324
#define TOK_NOT 325
#define TOK_AND 326
#define TOK_XNOR 327
#define TOK_XOR 328
#define TOK_OR 329
#define TOK_IFF 330
#define TOK_IMPLIES 331
#define TOK_COMMA 332
#define TOK_AA 333
#define TOK_EE 334
#define TOK_AG 335
#define TOK_EG 336
#define TOK_AF 337
#define TOK_EF 338
#define TOK_AX 339
#define TOK_EX 340
#define TOK_RELEASES 341
#define TOK_TRIGGERED 342
#define TOK_UNTIL 343
#define TOK_SINCE 344
#define TOK_MMAX 345
#define TOK_MMIN 346
#define TOK_BUNTIL 347
#define TOK_ABG 348
#define TOK_ABF 349
#define TOK_EBG 350
#define TOK_EBF 351
#define TOK_OP_FUTURE 352
#define TOK_OP_GLOBAL 353
#define TOK_OP_NEXT 354
#define TOK_OP_ONCE 355
#define TOK_OP_HISTORICAL 356
#define TOK_OP_NOTPRECNOT 357
#define TOK_OP_PREC 358
#define TOK_GE 359
#define TOK_LE 360
#define TOK_GT 361
#define TOK_LT 362
#define TOK_NOTEQUAL 363
#define TOK_EQUAL 364
#define TOK_RROTATE 365
#define TOK_LROTATE 366
#define TOK_RSHIFT 367
#define TOK_LSHIFT 368
#define TOK_SETIN 369
#define TOK_UNION 370
#define TOK_DIVIDE 371
#define TOK_TIMES 372
#define TOK_MINUS 373
#define TOK_PLUS 374
#define TOK_MOD 375
#define TOK_CONCATENATION 376
#define TOK_SMALLINIT 377
#define TOK_NEXT 378
#define TOK_BIT 379
#define TOK_DOT 380
#define TOK_LB 381
#define TOK_PLAYER_2 382
#define TOK_PLAYER_1 383
#define TOK_GAME 384
#define TOK_AVOIDTARGET 385
#define TOK_REACHTARGET 386
#define TOK_AVOIDDEADLOCK 387
#define TOK_REACHDEADLOCK 388
#define TOK_GENREACTIVITY 389
#define TOK_BUCHIGAME 390
#define TOK_MIRROR 391
#define TOK_PREDSLIST 392
#define TOK_PRED 393




#if ! defined (YYSTYPE) && ! defined (YYSTYPE_IS_DECLARED)
#line 113 "grammar.y"
typedef union YYSTYPE {
  node_ptr node;
  int lineno;
} YYSTYPE;
/* Line 1249 of yacc.c.  */
#line 317 "grammar.h"
# define yystype YYSTYPE /* obsolescent; will be withdrawn */
# define YYSTYPE_IS_DECLARED 1
# define YYSTYPE_IS_TRIVIAL 1
#endif

extern YYSTYPE yylval;



