/* A Bison parser, made by GNU Bison 1.875.  */

/* Skeleton parser for Yacc-like parsing with Bison,
   Copyright (C) 1984, 1989, 1990, 2000, 2001, 2002 Free Software Foundation, Inc.

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2, or (at your option)
   any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place - Suite 330,
   Boston, MA 02111-1307, USA.  */

/* As a special exception, when this file is copied by Bison into a
   Bison output file, you may use that output file without restriction.
   This special exception was added by the Free Software Foundation
   in version 1.24 of Bison.  */

/* Written by Richard Stallman by simplifying the original so called
   ``semantic'' parser.  */

/* All symbols defined below should begin with yy or YY, to avoid
   infringing on user name space.  This should be done even for local
   variables, as they might otherwise be expanded by user macros.
   There are some unavoidable exceptions within include files to
   define necessary library symbols; they are noted "INFRINGES ON
   USER NAME SPACE" below.  */

/* Identify Bison output.  */
#define YYBISON 1

/* Skeleton name.  */
#define YYSKELETON_NAME "yacc.c"

/* Pure parsers.  */
#define YYPURE 0

/* Using locations.  */
#define YYLSP_NEEDED 0



/* Tokens.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
   /* Put the tokens into the symbol table, so that GDB and other debuggers
      know about them.  */
   enum yytokentype {
     TOK_CONSTRAINT = 258,
     TOK_GOTO = 259,
     TOK_MAXU = 260,
     TOK_MINU = 261,
     TOK_ABU = 262,
     TOK_EBU = 263,
     TOK_AU = 264,
     TOK_EU = 265,
     TOK_CONTEXT = 266,
     TOK_PROCESS = 267,
     TOK_MODULE = 268,
     TOK_COMPUTE = 269,
     TOK_LTLSPEC = 270,
     TOK_CTLSPEC = 271,
     TOK_SPEC = 272,
     TOK_INVAR = 273,
     TOK_AGENTS_TRANS = 274,
     TOK_TRANS = 275,
     TOK_INIT = 276,
     TOK_DEFINE = 277,
     TOK_IVAR = 278,
     TOK_VAR = 279,
     TOK_PSLSPEC = 280,
     TOK_CONSTANTS = 281,
     TOK_JUSTICE = 282,
     TOK_COMPASSION = 283,
     TOK_FAIRNESS = 284,
     TOK_INVARSPEC = 285,
     TOK_ASSIGN = 286,
     TOK_ISA = 287,
     TOK_SEMI = 288,
     TOK_CONS = 289,
     TOK_OF = 290,
     TOK_RCB = 291,
     TOK_LCB = 292,
     TOK_RB = 293,
     TOK_RP = 294,
     TOK_LP = 295,
     TOK_TWODOTS = 296,
     TOK_EQDEF = 297,
     TOK_SELF = 298,
     TOK_COLON = 299,
     TOK_ESAC = 300,
     TOK_CASE = 301,
     TOK_COMPWFF = 302,
     TOK_CTLWFF = 303,
     TOK_LTLPSL = 304,
     TOK_LTLWFF = 305,
     TOK_SIMPWFF = 306,
     TOK_INCONTEXT = 307,
     TOK_WORD = 308,
     TOK_REAL = 309,
     TOK_INTEGER = 310,
     TOK_BOOLEAN = 311,
     TOK_ARRAY = 312,
     TOK_WORD1 = 313,
     TOK_BOOL = 314,
     TOK_WAWRITE = 315,
     TOK_WAREAD = 316,
     TOK_TRUEEXP = 317,
     TOK_FALSEEXP = 318,
     TOK_ATOM = 319,
     TOK_NUMBER_WORD = 320,
     TOK_NUMBER_EXP = 321,
     TOK_NUMBER_REAL = 322,
     TOK_NUMBER_FRAC = 323,
     TOK_NUMBER = 324,
     TOK_NOT = 325,
     TOK_AND = 326,
     TOK_XNOR = 327,
     TOK_XOR = 328,
     TOK_OR = 329,
     TOK_IFF = 330,
     TOK_IMPLIES = 331,
     TOK_COMMA = 332,
     TOK_AA = 333,
     TOK_EE = 334,
     TOK_AG = 335,
     TOK_EG = 336,
     TOK_AF = 337,
     TOK_EF = 338,
     TOK_AX = 339,
     TOK_EX = 340,
     TOK_RELEASES = 341,
     TOK_TRIGGERED = 342,
     TOK_UNTIL = 343,
     TOK_SINCE = 344,
     TOK_MMAX = 345,
     TOK_MMIN = 346,
     TOK_BUNTIL = 347,
     TOK_ABG = 348,
     TOK_ABF = 349,
     TOK_EBG = 350,
     TOK_EBF = 351,
     TOK_OP_FUTURE = 352,
     TOK_OP_GLOBAL = 353,
     TOK_OP_NEXT = 354,
     TOK_OP_ONCE = 355,
     TOK_OP_HISTORICAL = 356,
     TOK_OP_NOTPRECNOT = 357,
     TOK_OP_PREC = 358,
     TOK_GE = 359,
     TOK_LE = 360,
     TOK_GT = 361,
     TOK_LT = 362,
     TOK_NOTEQUAL = 363,
     TOK_EQUAL = 364,
     TOK_RROTATE = 365,
     TOK_LROTATE = 366,
     TOK_RSHIFT = 367,
     TOK_LSHIFT = 368,
     TOK_SETIN = 369,
     TOK_UNION = 370,
     TOK_DIVIDE = 371,
     TOK_TIMES = 372,
     TOK_MINUS = 373,
     TOK_PLUS = 374,
     TOK_MOD = 375,
     TOK_CONCATENATION = 376,
     TOK_SMALLINIT = 377,
     TOK_NEXT = 378,
     TOK_BIT = 379,
     TOK_DOT = 380,
     TOK_LB = 381,
     TOK_PLAYER_2 = 382,
     TOK_PLAYER_1 = 383,
     TOK_GAME = 384,
     TOK_AVOIDTARGET = 385,
     TOK_REACHTARGET = 386,
     TOK_AVOIDDEADLOCK = 387,
     TOK_REACHDEADLOCK = 388,
     TOK_GENREACTIVITY = 389,
     TOK_BUCHIGAME = 390,
     TOK_MIRROR = 391,
     TOK_PREDSLIST = 392,
     TOK_PRED = 393
   };
#endif
#define TOK_CONSTRAINT 258
#define TOK_GOTO 259
#define TOK_MAXU 260
#define TOK_MINU 261
#define TOK_ABU 262
#define TOK_EBU 263
#define TOK_AU 264
#define TOK_EU 265
#define TOK_CONTEXT 266
#define TOK_PROCESS 267
#define TOK_MODULE 268
#define TOK_COMPUTE 269
#define TOK_LTLSPEC 270
#define TOK_CTLSPEC 271
#define TOK_SPEC 272
#define TOK_INVAR 273
#define TOK_AGENTS_TRANS 274
#define TOK_TRANS 275
#define TOK_INIT 276
#define TOK_DEFINE 277
#define TOK_IVAR 278
#define TOK_VAR 279
#define TOK_PSLSPEC 280
#define TOK_CONSTANTS 281
#define TOK_JUSTICE 282
#define TOK_COMPASSION 283
#define TOK_FAIRNESS 284
#define TOK_INVARSPEC 285
#define TOK_ASSIGN 286
#define TOK_ISA 287
#define TOK_SEMI 288
#define TOK_CONS 289
#define TOK_OF 290
#define TOK_RCB 291
#define TOK_LCB 292
#define TOK_RB 293
#define TOK_RP 294
#define TOK_LP 295
#define TOK_TWODOTS 296
#define TOK_EQDEF 297
#define TOK_SELF 298
#define TOK_COLON 299
#define TOK_ESAC 300
#define TOK_CASE 301
#define TOK_COMPWFF 302
#define TOK_CTLWFF 303
#define TOK_LTLPSL 304
#define TOK_LTLWFF 305
#define TOK_SIMPWFF 306
#define TOK_INCONTEXT 307
#define TOK_WORD 308
#define TOK_REAL 309
#define TOK_INTEGER 310
#define TOK_BOOLEAN 311
#define TOK_ARRAY 312
#define TOK_WORD1 313
#define TOK_BOOL 314
#define TOK_WAWRITE 315
#define TOK_WAREAD 316
#define TOK_TRUEEXP 317
#define TOK_FALSEEXP 318
#define TOK_ATOM 319
#define TOK_NUMBER_WORD 320
#define TOK_NUMBER_EXP 321
#define TOK_NUMBER_REAL 322
#define TOK_NUMBER_FRAC 323
#define TOK_NUMBER 324
#define TOK_NOT 325
#define TOK_AND 326
#define TOK_XNOR 327
#define TOK_XOR 328
#define TOK_OR 329
#define TOK_IFF 330
#define TOK_IMPLIES 331
#define TOK_COMMA 332
#define TOK_AA 333
#define TOK_EE 334
#define TOK_AG 335
#define TOK_EG 336
#define TOK_AF 337
#define TOK_EF 338
#define TOK_AX 339
#define TOK_EX 340
#define TOK_RELEASES 341
#define TOK_TRIGGERED 342
#define TOK_UNTIL 343
#define TOK_SINCE 344
#define TOK_MMAX 345
#define TOK_MMIN 346
#define TOK_BUNTIL 347
#define TOK_ABG 348
#define TOK_ABF 349
#define TOK_EBG 350
#define TOK_EBF 351
#define TOK_OP_FUTURE 352
#define TOK_OP_GLOBAL 353
#define TOK_OP_NEXT 354
#define TOK_OP_ONCE 355
#define TOK_OP_HISTORICAL 356
#define TOK_OP_NOTPRECNOT 357
#define TOK_OP_PREC 358
#define TOK_GE 359
#define TOK_LE 360
#define TOK_GT 361
#define TOK_LT 362
#define TOK_NOTEQUAL 363
#define TOK_EQUAL 364
#define TOK_RROTATE 365
#define TOK_LROTATE 366
#define TOK_RSHIFT 367
#define TOK_LSHIFT 368
#define TOK_SETIN 369
#define TOK_UNION 370
#define TOK_DIVIDE 371
#define TOK_TIMES 372
#define TOK_MINUS 373
#define TOK_PLUS 374
#define TOK_MOD 375
#define TOK_CONCATENATION 376
#define TOK_SMALLINIT 377
#define TOK_NEXT 378
#define TOK_BIT 379
#define TOK_DOT 380
#define TOK_LB 381
#define TOK_PLAYER_2 382
#define TOK_PLAYER_1 383
#define TOK_GAME 384
#define TOK_AVOIDTARGET 385
#define TOK_REACHTARGET 386
#define TOK_AVOIDDEADLOCK 387
#define TOK_REACHDEADLOCK 388
#define TOK_GENREACTIVITY 389
#define TOK_BUCHIGAME 390
#define TOK_MIRROR 391
#define TOK_PREDSLIST 392
#define TOK_PRED 393




/* Copy the first part of user declarations.  */
#line 1 "grammar.y"

/**CFile***********************************************************************

  FileName    [grammar.y]

  PackageName [parser]

  Synopsis    [Grammar (for Yacc and Bison) of NuSMV input language]

  SeeAlso     [input.l]

  Author      [Andrei Tchaltsev, Marco Roveri]

  Copyright   [
  This file is part of the ``parser'' package of NuSMV version 2. 
  Copyright (C) 1998-2005 by CMU and ITC-irst. 

  NuSMV version 2 is free software; you can redistribute it and/or 
  modify it under the terms of the GNU Lesser General Public 
  License as published by the Free Software Foundation; either 
  version 2 of the License, or (at your option) any later version.

  NuSMV version 2 is distributed in the hope that it will be useful, 
  but WITHOUT ANY WARRANTY; without even the implied warranty of 
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU 
  Lesser General Public License for more details.

  You should have received a copy of the GNU Lesser General Public 
  License along with this library; if not, write to the Free Software 
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307  USA.

  For more information on NuSMV see <http://nusmv.irst.itc.it>
  or email to <nusmv-users@irst.itc.it>.
  Please report bugs to <nusmv-users@irst.itc.it>.

  To contact the NuSMV development board, email to <nusmv@irst.itc.it>. ]

******************************************************************************/

#if HAVE_CONFIG_H
# include "config.h"
#endif

#include <setjmp.h>

#if HAVE_MALLOC_H
# if HAVE_SYS_TYPES_H
#  include <sys/types.h>
# endif  
# include <malloc.h>
#elif HAVE_SYS_MALLOC_H
# if HAVE_SYS_TYPES_H
#  include <sys/types.h>
# endif  
# include <sys/malloc.h>
#elif HAVE_STDLIB_H
# include <stdlib.h>
#endif

#include <limits.h>

#include "parserInt.h"
#include "utils/utils.h"
#include "utils/ustring.h"
#include "node/node.h"
#include "opt/opt.h"
#include "utils/error.h"

#include "symbols.h"
#include "mbp_symbols.h"

static char rcsid[] UTIL_UNUSED = "$Id: grammar.y,v 1.19.4.10.4.46.4.17 2007/05/17 09:23:22 nusmv Exp $";

#define YYMAXDEPTH INT_MAX

node_ptr parsed_tree; /* the returned value of parsing */

enum PARSE_MODE parse_mode_flag; /* the flag what should be parsed */

extern FILE * nusmv_stderr;
    
void yyerror ARGS((char *s));
void yyerror_lined ARGS((const char *s, int line));
static node_ptr context2maincontext ARGS((node_ptr context));


/* this enum is used to distinguish 
   different kinds of expressions: SIMPLE, NEXT, CTL and LTL.
   Since syntactically only one global kind of expressions exists,
   we have to invoke a special function which checks that an expression
   complies to the additional syntactic constrains.
   So, if an ltl-expression is expected then occurrences of NEXT or EBF
   operators will cause the termination of parsing.

   NB: An alternative to invoking a checking function would be to write quite
   intricate syntactic rules to distinguish all the cases.

   NB: This checking function could also be a part of the type checker,
   but it is more straightforward to write a separate function.
*/
  enum EXP_KIND {EXP_SIMPLE, EXP_NEXT, EXP_LTL, EXP_CTL}; 

  static boolean isCorrectExp ARGS((node_ptr exp, enum EXP_KIND expectedKind));

  static int nusmv_parse_psl ARGS((void));

  /* below vars are used if input file contains game definition */
  static node_ptr mbp_parser_spec_list = Nil;
  static node_ptr mbp_parser_player_1 = Nil;
  static node_ptr mbp_parser_player_2 = Nil;


/* Enabling traces.  */
#ifndef YYDEBUG
# define YYDEBUG 0
#endif

/* Enabling verbose error messages.  */
#ifdef YYERROR_VERBOSE
# undef YYERROR_VERBOSE
# define YYERROR_VERBOSE 1
#else
# define YYERROR_VERBOSE 0
#endif

#if ! defined (YYSTYPE) && ! defined (YYSTYPE_IS_DECLARED)
#line 113 "grammar.y"
typedef union YYSTYPE {
  node_ptr node;
  int lineno;
} YYSTYPE;
/* Line 191 of yacc.c.  */
#line 468 "grammar.c"
# define yystype YYSTYPE /* obsolescent; will be withdrawn */
# define YYSTYPE_IS_DECLARED 1
# define YYSTYPE_IS_TRIVIAL 1
#endif



/* Copy the second part of user declarations.  */


/* Line 214 of yacc.c.  */
#line 480 "grammar.c"

#if ! defined (yyoverflow) || YYERROR_VERBOSE

/* The parser invokes alloca or malloc; define the necessary symbols.  */

# if YYSTACK_USE_ALLOCA
#  define YYSTACK_ALLOC alloca
# else
#  ifndef YYSTACK_USE_ALLOCA
#   if defined (alloca) || defined (_ALLOCA_H)
#    define YYSTACK_ALLOC alloca
#   else
#    ifdef __GNUC__
#     define YYSTACK_ALLOC __builtin_alloca
#    endif
#   endif
#  endif
# endif

# ifdef YYSTACK_ALLOC
   /* Pacify GCC's `empty if-body' warning. */
#  define YYSTACK_FREE(Ptr) do { /* empty */; } while (0)
# else
#  if defined (__STDC__) || defined (__cplusplus)
#   include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#   define YYSIZE_T size_t
#  endif
#  define YYSTACK_ALLOC malloc
#  define YYSTACK_FREE free
# endif
#endif /* ! defined (yyoverflow) || YYERROR_VERBOSE */


#if (! defined (yyoverflow) \
     && (! defined (__cplusplus) \
	 || (YYSTYPE_IS_TRIVIAL)))

/* A type that is properly aligned for any stack member.  */
union yyalloc
{
  short yyss;
  YYSTYPE yyvs;
  };

/* The size of the maximum gap between one aligned stack and the next.  */
# define YYSTACK_GAP_MAXIMUM (sizeof (union yyalloc) - 1)

/* The size of an array large to enough to hold all stacks, each with
   N elements.  */
# define YYSTACK_BYTES(N) \
     ((N) * (sizeof (short) + sizeof (YYSTYPE))				\
      + YYSTACK_GAP_MAXIMUM)

/* Copy COUNT objects from FROM to TO.  The source and destination do
   not overlap.  */
# ifndef YYCOPY
#  if 1 < __GNUC__
#   define YYCOPY(To, From, Count) \
      __builtin_memcpy (To, From, (Count) * sizeof (*(From)))
#  else
#   define YYCOPY(To, From, Count)		\
      do					\
	{					\
	  register YYSIZE_T yyi;		\
	  for (yyi = 0; yyi < (Count); yyi++)	\
	    (To)[yyi] = (From)[yyi];		\
	}					\
      while (0)
#  endif
# endif

/* Relocate STACK from its old location to the new one.  The
   local variables YYSIZE and YYSTACKSIZE give the old and new number of
   elements in the stack, and YYPTR gives the new location of the
   stack.  Advance YYPTR to a properly aligned location for the next
   stack.  */
# define YYSTACK_RELOCATE(Stack)					\
    do									\
      {									\
	YYSIZE_T yynewbytes;						\
	YYCOPY (&yyptr->Stack, Stack, yysize);				\
	Stack = &yyptr->Stack;						\
	yynewbytes = yystacksize * sizeof (*Stack) + YYSTACK_GAP_MAXIMUM; \
	yyptr += yynewbytes / sizeof (*yyptr);				\
      }									\
    while (0)

#endif

#if defined (__STDC__) || defined (__cplusplus)
   typedef signed char yysigned_char;
#else
   typedef short yysigned_char;
#endif

/* YYFINAL -- State number of the termination state. */
#define YYFINAL  5
/* YYLAST -- Last index in YYTABLE.  */
#define YYLAST   1015

/* YYNTOKENS -- Number of terminals. */
#define YYNTOKENS  139
/* YYNNTS -- Number of nonterminals. */
#define YYNNTS  108
/* YYNRULES -- Number of rules. */
#define YYNRULES  292
/* YYNRULES -- Number of states. */
#define YYNSTATES  581

/* YYTRANSLATE(YYLEX) -- Bison symbol number corresponding to YYLEX.  */
#define YYUNDEFTOK  2
#define YYMAXUTOK   393

#define YYTRANSLATE(YYX) 						\
  ((unsigned int) (YYX) <= YYMAXUTOK ? yytranslate[YYX] : YYUNDEFTOK)

/* YYTRANSLATE[YYLEX] -- Bison symbol number corresponding to YYLEX.  */
static const unsigned char yytranslate[] =
{
       0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     2,     3,     4,
       5,     6,     7,     8,     9,    10,    11,    12,    13,    14,
      15,    16,    17,    18,    19,    20,    21,    22,    23,    24,
      25,    26,    27,    28,    29,    30,    31,    32,    33,    34,
      35,    36,    37,    38,    39,    40,    41,    42,    43,    44,
      45,    46,    47,    48,    49,    50,    51,    52,    53,    54,
      55,    56,    57,    58,    59,    60,    61,    62,    63,    64,
      65,    66,    67,    68,    69,    70,    71,    72,    73,    74,
      75,    76,    77,    78,    79,    80,    81,    82,    83,    84,
      85,    86,    87,    88,    89,    90,    91,    92,    93,    94,
      95,    96,    97,    98,    99,   100,   101,   102,   103,   104,
     105,   106,   107,   108,   109,   110,   111,   112,   113,   114,
     115,   116,   117,   118,   119,   120,   121,   122,   123,   124,
     125,   126,   127,   128,   129,   130,   131,   132,   133,   134,
     135,   136,   137,   138
};

#if YYDEBUG
/* YYPRHS[YYN] -- Index of the first RHS symbol of rule number YYN in
   YYRHS.  */
static const unsigned short yyprhs[] =
{
       0,     0,     3,     4,     7,     8,    11,    12,    15,    17,
      20,    22,    25,    28,    30,    32,    34,    36,    40,    42,
      44,    46,    48,    50,    52,    54,    56,    59,    61,    63,
      67,    71,    76,    83,    87,    90,    95,   100,   105,   109,
     116,   125,   127,   130,   135,   137,   141,   143,   147,   151,
     153,   157,   161,   163,   167,   169,   173,   177,   179,   181,
     185,   187,   191,   193,   197,   199,   203,   205,   209,   213,
     217,   221,   225,   229,   231,   233,   236,   239,   242,   245,
     248,   251,   258,   265,   273,   281,   285,   289,   293,   297,
     300,   302,   306,   308,   312,   316,   320,   322,   326,   328,
     332,   334,   336,   338,   341,   344,   347,   350,   353,   356,
     359,   362,   364,   368,   372,   376,   380,   382,   386,   388,
     392,   396,   400,   402,   406,   408,   412,   414,   416,   418,
     420,   422,   424,   426,   428,   433,   435,   439,   450,   455,
     457,   459,   462,   464,   468,   470,   472,   474,   476,   478,
     482,   484,   488,   493,   498,   500,   504,   506,   508,   511,
     514,   518,   520,   524,   529,   531,   535,   536,   539,   541,
     543,   545,   547,   549,   551,   553,   555,   557,   559,   561,
     563,   565,   567,   569,   571,   573,   575,   577,   580,   581,
     584,   587,   590,   592,   594,   596,   598,   600,   602,   603,
     606,   608,   610,   612,   614,   616,   618,   620,   623,   625,
     628,   633,   635,   638,   640,   643,   648,   651,   652,   658,
     661,   662,   665,   670,   678,   686,   690,   694,   698,   703,
     707,   711,   719,   723,   727,   731,   735,   737,   741,   742,
     744,   748,   750,   753,   757,   764,   768,   770,   772,   777,
     782,   786,   790,   797,   808,   812,   819,   826,   829,   830,
     832,   834,   838,   842,   847,   849,   851,   855,   859,   864,
     866,   869,   871,   875,   879,   883,   887,   891,   895,   901,
     905,   911,   915,   921,   925,   931,   934,   936,   940,   945,
     947,   951,   952
};

/* YYRHS -- A `-1'-separated list of the rules' RHS. */
static const short yyrhs[] =
{
     140,     0,    -1,    -1,   141,   191,    -1,    -1,   142,   241,
      -1,    -1,   143,   183,    -1,    69,    -1,   119,    69,    -1,
      69,    -1,   119,    69,    -1,   118,    69,    -1,    65,    -1,
      68,    -1,    67,    -1,    66,    -1,   145,    41,   145,    -1,
      63,    -1,    62,    -1,   144,    -1,   146,    -1,   147,    -1,
     149,    -1,   148,    -1,   151,    -1,   118,   152,    -1,    64,
      -1,    43,    -1,   152,   125,    64,    -1,   152,   125,    69,
      -1,   152,   126,   180,    38,    -1,   152,   126,   180,    44,
     180,    38,    -1,    40,   179,    39,    -1,    70,   152,    -1,
      59,    40,   179,    39,    -1,    58,    40,   179,    39,    -1,
     123,    40,   179,    39,    -1,    46,   153,    45,    -1,    61,
      40,   180,    77,   180,    39,    -1,    60,    40,   180,    77,
     180,    77,   180,    39,    -1,   154,    -1,   154,   153,    -1,
     179,    44,   179,    33,    -1,   152,    -1,   155,   121,   152,
      -1,   155,    -1,   156,   117,   155,    -1,   156,   116,   155,
      -1,   156,    -1,   157,   119,   156,    -1,   157,   118,   156,
      -1,   157,    -1,   158,   120,   157,    -1,   158,    -1,   159,
     113,   158,    -1,   159,   112,   158,    -1,   159,    -1,   150,
      -1,    37,   161,    36,    -1,   179,    -1,   161,    77,   179,
      -1,   160,    -1,   162,   115,   160,    -1,   162,    -1,   163,
     114,   162,    -1,   163,    -1,   164,   109,   163,    -1,   164,
     108,   163,    -1,   164,   107,   163,    -1,   164,   106,   163,
      -1,   164,   105,   163,    -1,   164,   104,   163,    -1,   164,
      -1,   166,    -1,    85,   165,    -1,    84,   165,    -1,    83,
     165,    -1,    82,   165,    -1,    81,   165,    -1,    80,   165,
      -1,    78,   126,   171,    88,   171,    38,    -1,    79,   126,
     171,    88,   171,    38,    -1,    78,   126,   171,    92,   150,
     171,    38,    -1,    79,   126,   171,    92,   150,   171,    38,
      -1,    96,   150,   165,    -1,    94,   150,   165,    -1,    95,
     150,   165,    -1,    93,   150,   165,    -1,    70,   166,    -1,
     165,    -1,   167,    71,   165,    -1,   167,    -1,   168,    74,
     167,    -1,   168,    73,   167,    -1,   168,    72,   167,    -1,
     168,    -1,   169,    75,   168,    -1,   169,    -1,   169,    76,
     170,    -1,   170,    -1,   165,    -1,   173,    -1,    99,   172,
      -1,   103,   172,    -1,   102,   172,    -1,    98,   172,    -1,
     101,   172,    -1,    97,   172,    -1,   100,   172,    -1,    70,
     173,    -1,   172,    -1,   174,    88,   172,    -1,   174,    89,
     172,    -1,   174,    86,   172,    -1,   174,    87,   172,    -1,
     174,    -1,   175,    71,   174,    -1,   175,    -1,   176,    74,
     175,    -1,   176,    73,   175,    -1,   176,    72,   175,    -1,
     176,    -1,   177,    75,   176,    -1,   177,    -1,   177,    76,
     178,    -1,   178,    -1,   179,    -1,   179,    -1,   179,    -1,
     179,    -1,    56,    -1,    55,    -1,    54,    -1,    53,   126,
     180,    38,    -1,   150,    -1,    37,   186,    36,    -1,    57,
      53,   126,   180,    38,    35,    53,   126,   180,    38,    -1,
      57,   150,    35,   184,    -1,   184,    -1,   189,    -1,    12,
     189,    -1,   187,    -1,   186,    77,   187,    -1,   188,    -1,
     145,    -1,    63,    -1,    62,    -1,    64,    -1,   188,   125,
      64,    -1,    64,    -1,    64,    40,    39,    -1,    64,    40,
     190,    39,    -1,    57,   150,    35,   189,    -1,   180,    -1,
     190,    77,   180,    -1,   192,    -1,   197,    -1,   191,   192,
      -1,   191,   197,    -1,    13,   193,   195,    -1,    64,    -1,
      64,    40,    39,    -1,    64,    40,   194,    39,    -1,    64,
      -1,   194,    77,    64,    -1,    -1,   195,   196,    -1,   237,
      -1,   202,    -1,   205,    -1,   210,    -1,   213,    -1,   214,
      -1,   215,    -1,   208,    -1,   216,    -1,   217,    -1,   218,
      -1,   219,    -1,   220,    -1,   221,    -1,   222,    -1,   235,
      -1,   223,    -1,   226,    -1,   227,    -1,   129,   198,    -1,
      -1,   199,   198,    -1,   128,   200,    -1,   127,   200,    -1,
     229,    -1,   230,    -1,   231,    -1,   232,    -1,   233,    -1,
     234,    -1,    -1,   200,   201,    -1,   202,    -1,   210,    -1,
     213,    -1,   214,    -1,   215,    -1,   208,    -1,    24,    -1,
      24,   203,    -1,   204,    -1,   203,   204,    -1,   239,    44,
     185,    33,    -1,    23,    -1,    23,   206,    -1,   207,    -1,
     206,   207,    -1,   239,    44,   184,    33,    -1,    22,   209,
      -1,    -1,   209,   239,    42,   180,    33,    -1,    31,   211,
      -1,    -1,   211,   212,    -1,   240,    42,   180,    33,    -1,
     122,    40,   240,    39,    42,   180,    33,    -1,   123,    40,
     240,    39,    42,   181,    33,    -1,    21,   180,   238,    -1,
      18,   180,   238,    -1,    20,   181,   238,    -1,    19,   144,
     181,   238,    -1,    29,   180,   238,    -1,    27,   180,   238,
      -1,    28,    40,   180,    77,   180,    39,   238,    -1,    30,
     180,   238,    -1,    17,   182,   238,    -1,    16,   182,   238,
      -1,    15,   183,   238,    -1,    25,    -1,    26,   224,    33,
      -1,    -1,   188,    -1,   224,    77,   188,    -1,   226,    -1,
     225,   226,    -1,   138,   180,   238,    -1,   138,   126,    69,
      38,   180,   238,    -1,   136,   239,   238,    -1,   128,    -1,
     127,    -1,   131,   228,   180,   238,    -1,   130,   228,   180,
     238,    -1,   133,   228,   238,    -1,   132,   228,   238,    -1,
     135,   228,    40,   190,    39,   238,    -1,   134,   228,    40,
     190,    39,    76,    40,   190,    39,   238,    -1,    14,   236,
     238,    -1,    91,   126,   182,    77,   182,    38,    -1,    90,
     126,   182,    77,   182,    38,    -1,    32,    64,    -1,    -1,
      33,    -1,    64,    -1,   239,   125,    64,    -1,   239,   125,
      69,    -1,   239,   126,   145,    38,    -1,    64,    -1,    43,
      -1,   240,   125,    64,    -1,   240,   125,    69,    -1,   240,
     126,   180,    38,    -1,   242,    -1,     1,    33,    -1,     1,
      -1,     4,   245,    33,    -1,    21,   180,    33,    -1,    29,
     180,    33,    -1,    20,   181,    33,    -1,     3,   180,    33,
      -1,    51,   180,    33,    -1,    51,   180,    52,   243,    33,
      -1,    48,   182,    33,    -1,    48,   182,    52,   243,    33,
      -1,    50,   183,    33,    -1,    50,   183,    52,   243,    33,
      -1,    47,   236,    33,    -1,    47,   236,    52,   243,    33,
      -1,   137,   225,    -1,    64,    -1,   243,   125,    64,    -1,
     243,   126,   180,    38,    -1,    69,    -1,   244,   125,    69,
      -1,    -1,   246,   244,   125,    69,    -1
};

/* YYRLINE[YYN] -- source line where rule number YYN was defined.  */
static const unsigned short yyrline[] =
{
       0,   213,   213,   213,   243,   243,   250,   250,   265,   266,
     269,   270,   271,   275,   278,   280,   282,   285,   289,   290,
     291,   292,   293,   302,   311,   329,   330,   331,   332,   333,
     341,   349,   357,   362,   363,   364,   365,   366,   367,   369,
     372,   379,   381,   385,   390,   391,   395,   396,   397,   401,
     402,   403,   407,   408,   411,   412,   413,   419,   420,   421,
     424,   425,   429,   430,   433,   434,   438,   439,   440,   441,
     442,   443,   444,   447,   448,   452,   453,   454,   455,   456,
     457,   458,   460,   462,   464,   466,   467,   468,   469,   472,
     478,   479,   482,   483,   484,   485,   488,   489,   493,   494,
     497,   501,   502,   507,   508,   509,   510,   511,   512,   513,
     515,   520,   521,   523,   525,   532,   542,   543,   547,   548,
     549,   550,   554,   555,   559,   560,   563,   569,   572,   575,
     578,   588,   589,   597,   605,   607,   608,   610,   612,   617,
     618,   619,   623,   624,   627,   628,   629,   630,   633,   634,
     637,   638,   639,   641,   646,   647,   662,   663,   664,   665,
     668,   671,   672,   673,   676,   677,   682,   683,   685,   686,
     687,   688,   689,   690,   691,   692,   693,   694,   695,   696,
     697,   698,   699,   700,   701,   702,   703,   713,   738,   739,
     747,   763,   784,   785,   786,   787,   788,   789,   795,   796,
     800,   802,   803,   804,   805,   806,   818,   819,   821,   822,
     824,   826,   827,   829,   830,   832,   836,   838,   839,   843,
     845,   846,   848,   850,   855,   863,   865,   867,   869,   874,
     877,   880,   885,   888,   889,   892,   895,   904,   909,   910,
     911,   915,   916,   919,   930,   941,   952,   953,   955,   958,
     961,   964,   968,   975,   984,   987,   989,   994,   998,   998,
    1006,  1007,  1008,  1009,  1012,  1013,  1014,  1015,  1016,  1024,
    1025,  1026,  1029,  1031,  1033,  1035,  1037,  1039,  1043,  1047,
    1051,  1055,  1059,  1063,  1067,  1071,  1083,  1084,  1085,  1088,
    1089,  1092,  1092
};
#endif

#if YYDEBUG || YYERROR_VERBOSE
/* YYTNME[SYMBOL-NUM] -- String name of the symbol SYMBOL-NUM.
   First, the terminals, then, starting at YYNTOKENS, nonterminals. */
static const char *const yytname[] =
{
  "$end", "error", "$undefined", "TOK_CONSTRAINT", "TOK_GOTO", "TOK_MAXU", 
  "TOK_MINU", "TOK_ABU", "TOK_EBU", "TOK_AU", "TOK_EU", "TOK_CONTEXT", 
  "TOK_PROCESS", "TOK_MODULE", "TOK_COMPUTE", "TOK_LTLSPEC", 
  "TOK_CTLSPEC", "TOK_SPEC", "TOK_INVAR", "TOK_AGENTS_TRANS", "TOK_TRANS", 
  "TOK_INIT", "TOK_DEFINE", "TOK_IVAR", "TOK_VAR", "TOK_PSLSPEC", 
  "TOK_CONSTANTS", "TOK_JUSTICE", "TOK_COMPASSION", "TOK_FAIRNESS", 
  "TOK_INVARSPEC", "TOK_ASSIGN", "TOK_ISA", "TOK_SEMI", "TOK_CONS", 
  "TOK_OF", "TOK_RCB", "TOK_LCB", "TOK_RB", "TOK_RP", "TOK_LP", 
  "TOK_TWODOTS", "TOK_EQDEF", "TOK_SELF", "TOK_COLON", "TOK_ESAC", 
  "TOK_CASE", "TOK_COMPWFF", "TOK_CTLWFF", "TOK_LTLPSL", "TOK_LTLWFF", 
  "TOK_SIMPWFF", "TOK_INCONTEXT", "TOK_WORD", "TOK_REAL", "TOK_INTEGER", 
  "TOK_BOOLEAN", "TOK_ARRAY", "TOK_WORD1", "TOK_BOOL", "TOK_WAWRITE", 
  "TOK_WAREAD", "TOK_TRUEEXP", "TOK_FALSEEXP", "TOK_ATOM", 
  "TOK_NUMBER_WORD", "TOK_NUMBER_EXP", "TOK_NUMBER_REAL", 
  "TOK_NUMBER_FRAC", "TOK_NUMBER", "TOK_NOT", "TOK_AND", "TOK_XNOR", 
  "TOK_XOR", "TOK_OR", "TOK_IFF", "TOK_IMPLIES", "TOK_COMMA", "TOK_AA", 
  "TOK_EE", "TOK_AG", "TOK_EG", "TOK_AF", "TOK_EF", "TOK_AX", "TOK_EX", 
  "TOK_RELEASES", "TOK_TRIGGERED", "TOK_UNTIL", "TOK_SINCE", "TOK_MMAX", 
  "TOK_MMIN", "TOK_BUNTIL", "TOK_ABG", "TOK_ABF", "TOK_EBG", "TOK_EBF", 
  "TOK_OP_FUTURE", "TOK_OP_GLOBAL", "TOK_OP_NEXT", "TOK_OP_ONCE", 
  "TOK_OP_HISTORICAL", "TOK_OP_NOTPRECNOT", "TOK_OP_PREC", "TOK_GE", 
  "TOK_LE", "TOK_GT", "TOK_LT", "TOK_NOTEQUAL", "TOK_EQUAL", 
  "TOK_RROTATE", "TOK_LROTATE", "TOK_RSHIFT", "TOK_LSHIFT", "TOK_SETIN", 
  "TOK_UNION", "TOK_DIVIDE", "TOK_TIMES", "TOK_MINUS", "TOK_PLUS", 
  "TOK_MOD", "TOK_CONCATENATION", "TOK_SMALLINIT", "TOK_NEXT", "TOK_BIT", 
  "TOK_DOT", "TOK_LB", "TOK_PLAYER_2", "TOK_PLAYER_1", "TOK_GAME", 
  "TOK_AVOIDTARGET", "TOK_REACHTARGET", "TOK_AVOIDDEADLOCK", 
  "TOK_REACHDEADLOCK", "TOK_GENREACTIVITY", "TOK_BUCHIGAME", "TOK_MIRROR", 
  "TOK_PREDSLIST", "TOK_PRED", "$accept", "begin", "@1", "@2", "@3", 
  "number", "integer", "number_word", "number_frac", "number_real", 
  "number_exp", "subrange", "constant", "primary_expr", 
  "case_element_list_expr", "case_element_expr", "concatination_expr", 
  "multiplicative_expr", "additive_expr", "remainder_expr", "shift_expr", 
  "set_expr", "set_list_expr", "union_expr", "in_expr", "relational_expr", 
  "ctl_expr", "pure_ctl_expr", "ctl_and_expr", "ctl_or_expr", 
  "ctl_iff_expr", "ctl_implies_expr", "ctl_basic_expr", "ltl_unary_expr", 
  "pure_ltl_unary_expr", "ltl_binary_expr", "and_expr", "or_expr", 
  "iff_expr", "implies_expr", "basic_expr", "simple_expression", 
  "next_expression", "ctl_expression", "ltl_expression", "itype", "type", 
  "type_value_list", "type_value", "complex_atom", "module_type", 
  "simple_list_expression", "module_list", "module", "module_sign", 
  "atom_list", "declarations", "declaration", "game_definition", 
  "game_body", "game_body_element", "player_body", "player_body_element", 
  "var", "var_decl_list", "var_decl", "input_var", "ivar_decl_list", 
  "ivar_decl", "define", "define_list", "assign", "assign_list", 
  "one_assign", "init", "invar", "trans", "fairness", "justice", 
  "compassion", "invarspec", "ctlspec", "ltlspec", "pslspec", "constants", 
  "constants_expression", "predicate_list", "predicate", "mirror", 
  "player_num", "reachtarget", "avoidtarget", "reachdeadlock", 
  "avoiddeadlock", "buchigame", "genreactivity", "compute", 
  "compute_expression", "isa", "optsemi", "decl_var_id", "var_id", 
  "command", "command_case", "context", "trace", "state", "@4", 0
};
#endif

# ifdef YYPRINT
/* YYTOKNUM[YYLEX-NUM] -- Internal token number corresponding to
   token YYLEX-NUM.  */
static const unsigned short yytoknum[] =
{
       0,   256,   257,   258,   259,   260,   261,   262,   263,   264,
     265,   266,   267,   268,   269,   270,   271,   272,   273,   274,
     275,   276,   277,   278,   279,   280,   281,   282,   283,   284,
     285,   286,   287,   288,   289,   290,   291,   292,   293,   294,
     295,   296,   297,   298,   299,   300,   301,   302,   303,   304,
     305,   306,   307,   308,   309,   310,   311,   312,   313,   314,
     315,   316,   317,   318,   319,   320,   321,   322,   323,   324,
     325,   326,   327,   328,   329,   330,   331,   332,   333,   334,
     335,   336,   337,   338,   339,   340,   341,   342,   343,   344,
     345,   346,   347,   348,   349,   350,   351,   352,   353,   354,
     355,   356,   357,   358,   359,   360,   361,   362,   363,   364,
     365,   366,   367,   368,   369,   370,   371,   372,   373,   374,
     375,   376,   377,   378,   379,   380,   381,   382,   383,   384,
     385,   386,   387,   388,   389,   390,   391,   392,   393
};
# endif

/* YYR1[YYN] -- Symbol number of symbol that rule YYN derives.  */
static const unsigned char yyr1[] =
{
       0,   139,   141,   140,   142,   140,   143,   140,   144,   144,
     145,   145,   145,   146,   147,   148,   149,   150,   151,   151,
     151,   151,   151,   151,   151,   152,   152,   152,   152,   152,
     152,   152,   152,   152,   152,   152,   152,   152,   152,   152,
     152,   153,   153,   154,   155,   155,   156,   156,   156,   157,
     157,   157,   158,   158,   159,   159,   159,   160,   160,   160,
     161,   161,   162,   162,   163,   163,   164,   164,   164,   164,
     164,   164,   164,   165,   165,   166,   166,   166,   166,   166,
     166,   166,   166,   166,   166,   166,   166,   166,   166,   166,
     167,   167,   168,   168,   168,   168,   169,   169,   170,   170,
     171,   172,   172,   173,   173,   173,   173,   173,   173,   173,
     173,   174,   174,   174,   174,   174,   175,   175,   176,   176,
     176,   176,   177,   177,   178,   178,   179,   180,   181,   182,
     183,   184,   184,   184,   184,   184,   184,   184,   184,   185,
     185,   185,   186,   186,   187,   187,   187,   187,   188,   188,
     189,   189,   189,   189,   190,   190,   191,   191,   191,   191,
     192,   193,   193,   193,   194,   194,   195,   195,   196,   196,
     196,   196,   196,   196,   196,   196,   196,   196,   196,   196,
     196,   196,   196,   196,   196,   196,   196,   197,   198,   198,
     199,   199,   199,   199,   199,   199,   199,   199,   200,   200,
     201,   201,   201,   201,   201,   201,   202,   202,   203,   203,
     204,   205,   205,   206,   206,   207,   208,   209,   209,   210,
     211,   211,   212,   212,   212,   213,   214,   215,   215,   216,
     217,   218,   219,   220,   220,   221,   222,   223,   224,   224,
     224,   225,   225,   226,   226,   227,   228,   228,   229,   230,
     231,   232,   233,   234,   235,   236,   236,   237,   238,   238,
     239,   239,   239,   239,   240,   240,   240,   240,   240,   241,
     241,   241,   242,   242,   242,   242,   242,   242,   242,   242,
     242,   242,   242,   242,   242,   242,   243,   243,   243,   244,
     244,   246,   245
};

/* YYR2[YYN] -- Number of symbols composing right hand side of rule YYN.  */
static const unsigned char yyr2[] =
{
       0,     2,     0,     2,     0,     2,     0,     2,     1,     2,
       1,     2,     2,     1,     1,     1,     1,     3,     1,     1,
       1,     1,     1,     1,     1,     1,     2,     1,     1,     3,
       3,     4,     6,     3,     2,     4,     4,     4,     3,     6,
       8,     1,     2,     4,     1,     3,     1,     3,     3,     1,
       3,     3,     1,     3,     1,     3,     3,     1,     1,     3,
       1,     3,     1,     3,     1,     3,     1,     3,     3,     3,
       3,     3,     3,     1,     1,     2,     2,     2,     2,     2,
       2,     6,     6,     7,     7,     3,     3,     3,     3,     2,
       1,     3,     1,     3,     3,     3,     1,     3,     1,     3,
       1,     1,     1,     2,     2,     2,     2,     2,     2,     2,
       2,     1,     3,     3,     3,     3,     1,     3,     1,     3,
       3,     3,     1,     3,     1,     3,     1,     1,     1,     1,
       1,     1,     1,     1,     4,     1,     3,    10,     4,     1,
       1,     2,     1,     3,     1,     1,     1,     1,     1,     3,
       1,     3,     4,     4,     1,     3,     1,     1,     2,     2,
       3,     1,     3,     4,     1,     3,     0,     2,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     1,     1,     1,
       1,     1,     1,     1,     1,     1,     1,     2,     0,     2,
       2,     2,     1,     1,     1,     1,     1,     1,     0,     2,
       1,     1,     1,     1,     1,     1,     1,     2,     1,     2,
       4,     1,     2,     1,     2,     4,     2,     0,     5,     2,
       0,     2,     4,     7,     7,     3,     3,     3,     4,     3,
       3,     7,     3,     3,     3,     3,     1,     3,     0,     1,
       3,     1,     2,     3,     6,     3,     1,     1,     4,     4,
       3,     3,     6,    10,     3,     6,     6,     2,     0,     1,
       1,     3,     3,     4,     1,     1,     3,     3,     4,     1,
       2,     1,     3,     3,     3,     3,     3,     3,     5,     3,
       5,     3,     5,     3,     5,     2,     1,     3,     4,     1,
       3,     0,     4
};

/* YYDEFACT[STATE-NAME] -- Default rule to reduce with in state
   STATE-NUM when YYTABLE doesn't specify something else to do.  Zero
   means the default is an error.  */
static const unsigned short yydefact[] =
{
       6,     0,     0,     0,     0,     1,     0,   188,     3,   156,
     157,   271,     0,   291,     0,     0,     0,     0,     0,     0,
       0,     0,     5,   269,     0,     0,    28,     0,     0,     0,
       0,     0,    19,    18,    27,    13,    16,    15,    14,     8,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,    20,     0,    21,    22,    24,    23,    58,
      25,    44,    46,    49,    52,    54,    57,    62,    64,    66,
      73,   101,    74,   111,   102,   116,   118,   122,   124,   126,
     130,     7,   161,   166,   198,   198,     0,     0,     0,     0,
       0,     0,   187,   188,   192,   193,   194,   195,   196,   197,
     158,   159,   270,   127,     0,     0,     0,   128,     0,     0,
       0,     0,     0,     0,   129,     0,     0,     0,     0,   285,
     241,     0,    60,     0,     0,    41,     0,     0,     0,     0,
       0,     8,     0,     0,    34,    89,   110,     0,     0,     0,
      80,    79,    78,    77,    76,    75,    10,     0,     0,     0,
       0,     0,     0,   108,   106,   103,   109,   107,   105,   104,
       8,     0,    26,     9,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,   160,   191,   190,   247,   246,
       0,     0,   258,   258,     0,     0,   189,   276,   272,   289,
       0,   275,   273,   274,     0,     0,   283,     0,   279,     0,
     281,     0,   277,     0,     0,   258,   242,    59,     0,    33,
      38,    42,     0,     0,     0,     0,     0,     9,    90,    92,
      96,    98,   100,     0,     0,    12,    11,    88,    86,    87,
      85,     0,    17,    29,    30,     0,    45,    48,    47,    51,
      50,    53,    56,    55,    63,    65,    72,    71,    70,    69,
      68,    67,   114,   115,   112,   113,   117,   121,   120,   119,
     123,   125,   162,   164,     0,     0,     0,     0,     0,     0,
       0,     0,     0,   217,   211,   206,   236,   238,     0,     0,
       0,     0,   220,     0,     0,   167,   169,   170,   175,   171,
     172,   173,   174,   176,   177,   178,   179,   180,   181,   182,
     184,   185,   186,   183,   168,   199,   200,   205,   201,   202,
     203,   204,   258,   258,   259,   251,   250,     0,     0,     0,
       0,     0,   286,     0,     0,     0,     0,     0,   243,    61,
       0,    36,    35,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,    37,    31,     0,   163,     0,
     258,   258,   258,   258,   258,     0,   258,   258,   216,   260,
     212,   213,     0,   207,   208,     0,   148,   239,     0,   258,
       0,   258,   258,   219,   257,   258,   249,   248,   154,     0,
       0,   290,     0,     0,   284,     0,     0,   280,   282,   278,
       0,    43,     0,     0,    91,    95,    94,    93,    97,    99,
       0,     0,     0,     0,     0,   165,   254,   235,   234,   233,
     226,   258,   227,   225,     0,   214,     0,     0,     0,   209,
       0,     0,   237,     0,   230,     0,   229,   232,   265,   264,
       0,     0,   221,     0,   245,     0,     0,   258,     0,     0,
     287,     0,   258,     0,    39,    81,     0,    82,     0,    32,
     228,     0,     0,     0,   133,   132,   131,     0,   135,     0,
     261,   262,     0,     0,     0,   150,   139,     0,   140,   149,
     240,     0,     0,     0,     0,     0,     0,     0,   155,   252,
     256,   255,   288,   244,     0,    83,    84,     0,   147,   146,
     145,     0,   142,   144,     0,     0,     0,   215,   263,     0,
     141,     0,     0,   210,     0,     0,     0,     0,   266,   267,
       0,     0,    40,   218,   136,     0,     0,     0,     0,     0,
       0,   151,     0,   258,     0,     0,   222,   268,     0,   143,
     134,     0,   138,     0,   153,   152,   231,     0,     0,   258,
       0,     0,     0,   253,     0,   223,   224,     0,     0,     0,
     137
};

/* YYDEFGOTO[NTERM-NUM]. */
static const short yydefgoto[] =
{
      -1,     1,     2,     3,     4,    63,    64,    65,    66,    67,
      68,    69,    70,    71,   134,   135,    72,    73,    74,    75,
      76,    77,   131,    78,    79,    80,    81,    82,   249,   250,
     251,   252,   253,    83,    84,    85,    86,    87,    88,    89,
     113,   408,   118,   125,    91,   562,   497,   521,   522,   523,
     564,   409,     8,     9,    93,   294,   205,   315,    10,   102,
     103,   206,   335,   336,   393,   394,   317,   390,   391,   337,
     388,   338,   403,   462,   339,   340,   341,   323,   324,   325,
     326,   327,   328,   329,   330,   398,   129,   130,   332,   210,
     104,   105,   106,   107,   108,   109,   333,   123,   334,   345,
     392,   463,    22,    23,   353,   220,   115,   116
};

/* YYPACT[STATE-NUM] -- Index in YYTABLE of the portion describing
   STATE-NUM.  */
#define YYPACT_NINF -430
static const short yypact[] =
{
      64,    66,    -7,    53,   652,  -430,    44,   264,    -7,  -430,
    -430,   151,   652,  -430,   652,   652,   652,   198,   652,   652,
     652,     3,  -430,  -430,   652,   652,  -430,   652,   138,   141,
     164,   206,  -430,  -430,  -430,  -430,  -430,  -430,  -430,   197,
     765,   122,   124,   719,   719,   719,   719,   719,   719,   -28,
     -28,   -28,   -28,   652,   652,   652,   652,   652,   652,   652,
     857,   204,   231,  -430,   235,  -430,  -430,  -430,  -430,  -430,
    -430,   -30,   174,    94,   186,   160,   218,  -430,   182,   196,
     320,  -430,  -430,  -430,  -430,   205,   257,   145,   250,  -430,
    -430,  -430,   296,  -430,  -430,  -430,   222,   222,   222,   222,
     222,   222,  -430,   264,  -430,  -430,  -430,  -430,  -430,  -430,
    -430,  -430,  -430,  -430,   301,   321,   293,  -430,   331,   342,
     344,   254,   286,   140,  -430,   143,   156,   173,   516,     3,
    -430,    36,  -430,   350,   365,   652,   369,   652,   652,   652,
     652,  -430,   892,   345,   -30,  -430,  -430,   719,   719,   811,
    -430,  -430,  -430,  -430,  -430,  -430,  -430,   346,   347,   719,
     719,   719,   719,  -430,  -430,  -430,  -430,  -430,  -430,  -430,
     378,   892,   -30,   379,   652,   -28,   105,   652,   892,   892,
     892,   892,   892,   892,   892,   892,   413,   413,   413,   413,
     413,   413,   413,   413,   652,   652,   652,   652,   652,   652,
     652,   652,   652,   652,    30,   238,   213,   213,  -430,  -430,
     652,   652,   389,   389,   390,   391,  -430,  -430,  -430,  -430,
     307,  -430,  -430,  -430,   652,   652,  -430,   370,  -430,   370,
    -430,   370,  -430,   370,   364,   389,  -430,  -430,   652,  -430,
    -430,  -430,   652,   396,   397,   360,   361,  -430,  -430,   368,
     241,   294,  -430,   -29,   148,  -430,  -430,  -430,  -430,  -430,
    -430,   401,  -430,  -430,  -430,    48,   -30,   174,   174,    94,
      94,   186,   160,   160,  -430,   182,   196,   196,   196,   196,
     196,   196,  -430,  -430,  -430,  -430,   205,   257,   257,   257,
     145,  -430,  -430,  -430,    84,   198,   652,   652,   652,   652,
     -38,   652,   652,  -430,   377,   377,  -430,   380,   652,   402,
     652,   652,  -430,   381,   377,  -430,  -430,  -430,  -430,  -430,
    -430,  -430,  -430,  -430,  -430,  -430,  -430,  -430,  -430,  -430,
    -430,  -430,  -430,  -430,  -430,  -430,  -430,  -430,  -430,  -430,
    -430,  -430,   389,   389,  -430,  -430,  -430,   652,   652,   374,
     372,   375,  -430,    -8,    -6,     7,    10,   408,  -430,  -430,
     414,  -430,  -430,   652,   652,   719,   719,   719,   719,   719,
     719,   719,   -28,   719,   -28,  -430,  -430,   652,  -430,   387,
     389,   389,   389,   389,   389,   652,   389,   389,   377,  -430,
     377,  -430,   -16,   377,  -430,    32,  -430,   332,    25,   389,
     652,   389,   389,   -25,  -430,    19,  -430,  -430,  -430,    90,
      91,   425,   652,   652,  -430,   398,   652,  -430,  -430,  -430,
     652,  -430,   383,   424,  -430,   368,   368,   368,   241,  -430,
     426,   719,   427,   719,   428,  -430,  -430,  -430,  -430,  -430,
    -430,   389,  -430,  -430,    29,  -430,   159,   108,   -28,  -430,
     263,   403,  -430,   380,  -430,   393,  -430,  -430,  -430,  -430,
     444,   445,  -430,    38,  -430,   392,   652,   389,   448,   449,
    -430,   450,   389,   652,  -430,  -430,   453,  -430,   455,  -430,
    -430,   652,   260,   371,  -430,  -430,  -430,    52,  -430,   462,
    -430,  -430,   458,    -2,    52,   459,  -430,   465,  -430,  -430,
     332,   652,    35,    35,   652,   111,   652,   460,  -430,  -430,
    -430,  -430,  -430,  -430,   463,  -430,  -430,   468,  -430,  -430,
    -430,    89,  -430,   332,   652,   382,   469,  -430,  -430,   -28,
    -430,   470,   585,  -430,   464,   -19,    22,   473,  -430,  -430,
     472,   652,  -430,  -430,  -430,   260,   475,   652,   159,   479,
     284,  -430,   117,   389,   474,   476,  -430,  -430,   120,  -430,
    -430,   477,  -430,    -2,  -430,  -430,  -430,   652,   652,   389,
     482,   486,   487,  -430,   454,  -430,  -430,   395,   652,   484,
    -430
};

/* YYPGOTO[NTERM-NUM].  */
static const short yypgoto[] =
{
    -430,  -430,  -430,  -430,  -430,   223,  -174,  -430,  -430,  -430,
    -430,   -39,  -430,   -18,   394,  -430,   193,   219,   341,   202,
    -430,   339,  -430,   340,   154,  -430,   -10,   -23,     0,   157,
    -430,   158,  -122,    -9,   490,   335,   184,   336,  -430,   334,
       5,   -12,  -296,  -210,   -17,  -367,  -430,  -430,   -11,  -291,
    -429,  -341,  -430,   531,  -430,  -430,  -430,  -430,   532,   438,
    -430,   447,  -430,   338,  -430,   152,  -430,  -430,   161,   343,
    -430,   349,  -430,  -430,   352,   353,   355,  -430,  -430,  -430,
    -430,  -430,  -430,  -430,  -430,  -430,  -430,   -59,  -430,   185,
    -430,  -430,  -430,  -430,  -430,  -430,  -430,   249,  -430,  -160,
    -254,   -97,  -430,  -430,   -24,  -430,  -430,  -430
};

/* YYTABLE[YYPACT[STATE-NUM]].  What to do in state STATE-NUM.  If
   positive, shift that token.  If negative, reduce the rule which
   number is the opposite.  If zero, do what YYDEFACT says.
   If YYTABLE_NINF, syntax error.  */
#define YYTABLE_NINF -293
static const short yytable[] =
{
     114,   262,   126,   119,   120,   386,     6,   410,   127,    90,
     159,   160,   161,   162,   350,   351,   397,   145,   458,   117,
     554,   498,   144,   124,    90,   414,   254,   417,   446,   132,
     133,   141,   136,   150,   151,   152,   153,   154,   155,   459,
     418,   156,   172,   419,   163,   164,   165,   166,   167,   168,
     169,   395,   344,   346,    11,   529,    12,    13,   452,   371,
     405,   555,   495,   372,   530,    -4,     5,    -4,    -4,   292,
     236,   481,   237,    14,    15,   358,   450,    -2,   458,   489,
     504,   143,    16,   496,    -4,    -4,   376,   382,   383,   441,
     157,   158,   377,    -4,   293,   176,   177,   460,   461,   459,
      17,    18,   453,    19,    20,   525,   505,   506,    92,   447,
     448,    -4,    -4,   238,    -4,    -4,   235,   415,   416,   415,
     416,   156,     7,   378,   172,   544,   145,   245,   246,   465,
     467,   144,   415,   416,   444,   415,   416,   248,   248,   395,
     136,   128,   243,   244,   447,   448,   331,   505,   506,   257,
     258,   259,   260,   144,   447,   448,   565,   447,   448,   569,
     266,   379,   500,   505,   506,   265,   545,   466,   466,   263,
     157,   158,   490,   226,   264,   538,   228,   491,   137,   261,
     539,   138,   406,   407,   112,   282,   283,   284,   285,   230,
      21,   552,   227,    -2,   466,   229,   482,   466,   342,   343,
     558,    -4,   468,   469,   139,   354,   232,   355,   231,   356,
     179,   180,   483,   484,   485,   486,   487,   199,   200,   201,
     436,   437,   438,   439,   440,   233,   442,   443,   156,   124,
     124,   299,   300,   301,   302,   303,   373,   305,   -10,   454,
     374,   456,   457,   359,   312,   464,   140,   360,   147,   430,
     148,   432,   295,   296,   297,   298,   299,   300,   301,   302,
     303,   304,   305,   306,   307,   308,   309,   310,   311,   312,
     313,   174,   572,   173,   492,   493,   175,   157,   158,   381,
     183,   480,   211,   212,   213,   214,   215,   384,   121,   122,
     387,   194,   195,   196,   197,   178,   399,   186,   401,   402,
     482,    90,   124,   124,   181,   182,   117,   509,   520,   476,
     187,   478,   513,   366,   367,   368,   483,   484,   485,   486,
     494,   482,   518,   519,   396,   202,   203,   495,   198,   156,
     184,   185,   156,   431,   217,   433,   204,   483,   484,   485,
     486,   494,   276,   277,   278,   279,   280,   281,   495,   208,
     209,   422,   423,   156,   218,   424,   248,   248,   248,   248,
     248,   248,   219,   248,   221,   434,   425,   426,   427,   369,
     370,   520,   267,   268,   314,   222,   128,   223,   157,   158,
     224,   157,   158,   287,   288,   289,   272,   273,   455,   239,
     117,    94,    95,   566,    96,    97,    98,    99,   100,   101,
     269,   270,   157,   158,   471,   535,   536,   488,   472,   573,
     240,   488,   225,   242,   247,   255,   256,   124,   124,   -12,
     -11,   248,   344,   248,   188,   189,   190,   191,   192,   193,
     347,   348,   349,   357,   352,   361,   362,   363,   364,   365,
     375,   389,   400,   411,   396,   404,   420,   421,   526,   412,
      24,   435,   413,    25,   508,   531,    26,   451,  -292,    27,
     473,   514,   470,   474,   475,   477,   479,   499,   507,   517,
     501,    28,    29,    30,    31,    32,    33,    34,    35,    36,
      37,    38,    39,   171,   502,   503,   510,   511,   512,   534,
     549,   515,   537,   516,   540,   527,   528,   524,   533,   532,
     541,   543,   542,   553,   548,   550,   556,   577,   547,   488,
     557,   488,   546,   560,   563,   570,   567,   574,   568,   575,
     576,   578,   580,   385,   271,   274,   428,   275,   429,   241,
     146,    60,    61,   286,   559,   561,    62,   291,   290,   110,
     111,   216,   207,   316,   380,   449,     0,     0,   318,     0,
       0,   445,     0,    24,   319,   571,    25,   320,   321,    26,
     322,     0,    27,     0,     0,     0,   579,     0,     0,     0,
       0,     0,     0,   117,    28,    29,    30,    31,    32,    33,
      34,    35,    36,    37,    38,    39,    40,     0,     0,     0,
       0,     0,     0,     0,    41,    42,    43,    44,    45,    46,
      47,    48,     0,     0,     0,     0,     0,     0,     0,    49,
      50,    51,    52,    53,    54,    55,    56,    57,    58,    59,
       0,     0,    24,     0,   551,    25,     0,     0,    26,     0,
       0,    27,     0,     0,    60,    61,     0,     0,     0,    62,
       0,     0,   234,    28,    29,    30,    31,    32,    33,    34,
      35,    36,    37,    38,    39,    40,     0,     0,     0,     0,
       0,     0,     0,    41,    42,    43,    44,    45,    46,    47,
      48,     0,     0,     0,     0,     0,     0,     0,    49,    50,
      51,    52,    53,    54,    55,    56,    57,    58,    59,    24,
       0,     0,    25,     0,     0,    26,     0,     0,    27,     0,
       0,     0,     0,    60,    61,     0,     0,     0,    62,     0,
      28,    29,    30,    31,    32,    33,    34,    35,    36,    37,
      38,    39,    40,     0,     0,     0,     0,     0,     0,     0,
      41,    42,    43,    44,    45,    46,    47,    48,     0,     0,
       0,     0,     0,     0,     0,    49,    50,    51,    52,    53,
      54,    55,    56,    57,    58,    59,    24,     0,     0,    25,
       0,     0,    26,     0,     0,    27,     0,     0,     0,     0,
      60,    61,     0,     0,     0,    62,     0,    28,    29,    30,
      31,    32,    33,    34,    35,    36,    37,    38,    39,   149,
       0,     0,     0,     0,     0,     0,     0,    41,    42,    43,
      44,    45,    46,    47,    48,    25,     0,     0,    26,     0,
       0,    27,    49,    50,    51,    52,     0,     0,     0,     0,
       0,     0,     0,    28,    29,    30,    31,    32,    33,    34,
      35,    36,    37,    38,   141,    40,     0,    60,    61,     0,
       0,     0,    62,    41,    42,    43,    44,    45,    46,    47,
      48,    25,     0,     0,    26,     0,     0,    27,    49,    50,
      51,    52,    53,    54,    55,    56,    57,    58,    59,    28,
      29,    30,    31,    32,    33,    34,    35,    36,    37,    38,
     141,   149,     0,   142,   143,     0,     0,     0,    62,    41,
      42,    43,    44,    45,    46,    47,    48,    25,     0,     0,
      26,     0,     0,    27,    49,    50,    51,    52,     0,     0,
       0,     0,     0,     0,     0,    28,    29,    30,    31,    32,
      33,    34,    35,    36,    37,    38,   170,   171,     0,   142,
     143,     0,    25,     0,    62,    26,     0,     0,    27,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
      28,    29,    30,    31,    32,    33,    34,    35,    36,    37,
      38,   141,   171,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,   142,   143,     0,     0,     0,
      62,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
     142,   143,     0,     0,     0,    62
};

static const short yycheck[] =
{
      12,   175,    19,    15,    16,   301,    13,   348,    20,     4,
      49,    50,    51,    52,   224,   225,   307,    40,    43,    14,
      39,   450,    40,    18,    19,    33,   148,    33,    44,    24,
      25,    69,    27,    43,    44,    45,    46,    47,    48,    64,
      33,    69,    60,    33,    53,    54,    55,    56,    57,    58,
      59,   305,    33,   213,     1,    57,     3,     4,    33,    88,
     314,    39,    64,    92,   493,     1,     0,     3,     4,    39,
     129,    42,    36,    20,    21,   235,    44,    13,    43,   446,
      42,   119,    29,   450,    20,    21,    38,   297,   298,   385,
     118,   119,    44,    29,    64,   125,   126,   122,   123,    64,
      47,    48,    77,    50,    51,    53,   125,   126,    64,   125,
     126,    47,    48,    77,    50,    51,   128,   125,   126,   125,
     126,    69,   129,    39,   142,    36,   149,   139,   140,    39,
      39,   149,   125,   126,   388,   125,   126,   147,   148,   393,
     135,   138,   137,   138,   125,   126,   205,   125,   126,   159,
     160,   161,   162,   171,   125,   126,    39,   125,   126,    39,
     178,    77,   453,   125,   126,   177,    77,    77,    77,    64,
     118,   119,    64,    33,    69,    64,    33,    69,    40,   174,
      69,    40,   342,   343,    33,   194,   195,   196,   197,    33,
     137,   532,    52,   129,    77,    52,    37,    77,   210,   211,
     541,   137,   412,   413,    40,   229,    33,   231,    52,   233,
     116,   117,    53,    54,    55,    56,    57,    72,    73,    74,
     380,   381,   382,   383,   384,    52,   386,   387,    69,   224,
     225,    18,    19,    20,    21,    22,    88,    24,    41,   399,
      92,   401,   402,   238,    31,   405,    40,   242,   126,   371,
     126,   373,    14,    15,    16,    17,    18,    19,    20,    21,
      22,    23,    24,    25,    26,    27,    28,    29,    30,    31,
      32,    40,   568,    69,   448,    12,    41,   118,   119,   296,
     120,   441,    97,    98,    99,   100,   101,   299,    90,    91,
     302,    86,    87,    88,    89,   121,   308,   115,   310,   311,
      37,   296,   297,   298,   118,   119,   301,   467,   482,   431,
     114,   433,   472,    72,    73,    74,    53,    54,    55,    56,
      57,    37,    62,    63,    64,    75,    76,    64,    71,    69,
     112,   113,    69,   372,    33,   374,    40,    53,    54,    55,
      56,    57,   188,   189,   190,   191,   192,   193,    64,   127,
     128,   363,   364,    69,    33,   365,   366,   367,   368,   369,
     370,   371,    69,   373,    33,   377,   366,   367,   368,    75,
      76,   545,   179,   180,   136,    33,   138,    33,   118,   119,
     126,   118,   119,   199,   200,   201,   184,   185,   400,    39,
     385,   127,   128,   553,   130,   131,   132,   133,   134,   135,
     181,   182,   118,   119,   416,   502,   503,   446,   420,   569,
      45,   450,   126,    44,    69,    69,    69,   412,   413,    41,
      41,   431,    33,   433,   104,   105,   106,   107,   108,   109,
      40,    40,   125,    69,    64,    39,    39,    77,    77,    71,
      39,    64,    40,    69,    64,    64,    38,    33,   487,    77,
      37,    64,    77,    40,   466,   494,    43,   125,    33,    46,
      77,   473,    64,    39,    38,    38,    38,    64,    76,   481,
      77,    58,    59,    60,    61,    62,    63,    64,    65,    66,
      67,    68,    69,    70,    40,    40,    38,    38,    38,   501,
     529,    38,   504,    38,   506,    33,    38,   126,    33,    40,
      40,    33,    39,    39,    35,    35,    33,    53,   126,   548,
      38,   550,   524,    38,    35,    38,    42,    35,    42,    33,
      33,   126,    38,   300,   183,   186,   369,   187,   370,   135,
      40,   118,   119,   198,   545,   547,   123,   203,   202,     8,
       8,   103,    95,   205,   295,   393,    -1,    -1,   205,    -1,
      -1,   390,    -1,    37,   205,   567,    40,   205,   205,    43,
     205,    -1,    46,    -1,    -1,    -1,   578,    -1,    -1,    -1,
      -1,    -1,    -1,   568,    58,    59,    60,    61,    62,    63,
      64,    65,    66,    67,    68,    69,    70,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    78,    79,    80,    81,    82,    83,
      84,    85,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    93,
      94,    95,    96,    97,    98,    99,   100,   101,   102,   103,
      -1,    -1,    37,    -1,    39,    40,    -1,    -1,    43,    -1,
      -1,    46,    -1,    -1,   118,   119,    -1,    -1,    -1,   123,
      -1,    -1,   126,    58,    59,    60,    61,    62,    63,    64,
      65,    66,    67,    68,    69,    70,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    78,    79,    80,    81,    82,    83,    84,
      85,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    93,    94,
      95,    96,    97,    98,    99,   100,   101,   102,   103,    37,
      -1,    -1,    40,    -1,    -1,    43,    -1,    -1,    46,    -1,
      -1,    -1,    -1,   118,   119,    -1,    -1,    -1,   123,    -1,
      58,    59,    60,    61,    62,    63,    64,    65,    66,    67,
      68,    69,    70,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      78,    79,    80,    81,    82,    83,    84,    85,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    93,    94,    95,    96,    97,
      98,    99,   100,   101,   102,   103,    37,    -1,    -1,    40,
      -1,    -1,    43,    -1,    -1,    46,    -1,    -1,    -1,    -1,
     118,   119,    -1,    -1,    -1,   123,    -1,    58,    59,    60,
      61,    62,    63,    64,    65,    66,    67,    68,    69,    70,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    78,    79,    80,
      81,    82,    83,    84,    85,    40,    -1,    -1,    43,    -1,
      -1,    46,    93,    94,    95,    96,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    58,    59,    60,    61,    62,    63,    64,
      65,    66,    67,    68,    69,    70,    -1,   118,   119,    -1,
      -1,    -1,   123,    78,    79,    80,    81,    82,    83,    84,
      85,    40,    -1,    -1,    43,    -1,    -1,    46,    93,    94,
      95,    96,    97,    98,    99,   100,   101,   102,   103,    58,
      59,    60,    61,    62,    63,    64,    65,    66,    67,    68,
      69,    70,    -1,   118,   119,    -1,    -1,    -1,   123,    78,
      79,    80,    81,    82,    83,    84,    85,    40,    -1,    -1,
      43,    -1,    -1,    46,    93,    94,    95,    96,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    58,    59,    60,    61,    62,
      63,    64,    65,    66,    67,    68,    69,    70,    -1,   118,
     119,    -1,    40,    -1,   123,    43,    -1,    -1,    46,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      58,    59,    60,    61,    62,    63,    64,    65,    66,    67,
      68,    69,    70,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,   118,   119,    -1,    -1,    -1,
     123,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
      -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,    -1,
     118,   119,    -1,    -1,    -1,   123
};

/* YYSTOS[STATE-NUM] -- The (internal number of the) accessing
   symbol of state STATE-NUM.  */
static const unsigned char yystos[] =
{
       0,   140,   141,   142,   143,     0,    13,   129,   191,   192,
     197,     1,     3,     4,    20,    21,    29,    47,    48,    50,
      51,   137,   241,   242,    37,    40,    43,    46,    58,    59,
      60,    61,    62,    63,    64,    65,    66,    67,    68,    69,
      70,    78,    79,    80,    81,    82,    83,    84,    85,    93,
      94,    95,    96,    97,    98,    99,   100,   101,   102,   103,
     118,   119,   123,   144,   145,   146,   147,   148,   149,   150,
     151,   152,   155,   156,   157,   158,   159,   160,   162,   163,
     164,   165,   166,   172,   173,   174,   175,   176,   177,   178,
     179,   183,    64,   193,   127,   128,   130,   131,   132,   133,
     134,   135,   198,   199,   229,   230,   231,   232,   233,   234,
     192,   197,    33,   179,   180,   245,   246,   179,   181,   180,
     180,    90,    91,   236,   179,   182,   183,   180,   138,   225,
     226,   161,   179,   179,   153,   154,   179,    40,    40,    40,
      40,    69,   118,   119,   152,   166,   173,   126,   126,    70,
     165,   165,   165,   165,   165,   165,    69,   118,   119,   150,
     150,   150,   150,   172,   172,   172,   172,   172,   172,   172,
      69,    70,   152,    69,    40,    41,   125,   126,   121,   116,
     117,   118,   119,   120,   112,   113,   115,   114,   104,   105,
     106,   107,   108,   109,    86,    87,    88,    89,    71,    72,
      73,    74,    75,    76,    40,   195,   200,   200,   127,   128,
     228,   228,   228,   228,   228,   228,   198,    33,    33,    69,
     244,    33,    33,    33,   126,   126,    33,    52,    33,    52,
      33,    52,    33,    52,   126,   180,   226,    36,    77,    39,
      45,   153,    44,   179,   179,   180,   180,    69,   165,   167,
     168,   169,   170,   171,   171,    69,    69,   165,   165,   165,
     165,   179,   145,    64,    69,   180,   152,   155,   155,   156,
     156,   157,   158,   158,   160,   162,   163,   163,   163,   163,
     163,   163,   172,   172,   172,   172,   174,   175,   175,   175,
     176,   178,    39,    64,   194,    14,    15,    16,    17,    18,
      19,    20,    21,    22,    23,    24,    25,    26,    27,    28,
      29,    30,    31,    32,   136,   196,   202,   205,   208,   210,
     213,   214,   215,   216,   217,   218,   219,   220,   221,   222,
     223,   226,   227,   235,   237,   201,   202,   208,   210,   213,
     214,   215,   180,   180,    33,   238,   238,    40,    40,   125,
     182,   182,    64,   243,   243,   243,   243,    69,   238,   179,
     179,    39,    39,    77,    77,    71,    72,    73,    74,    75,
      76,    88,    92,    88,    92,    39,    38,    44,    39,    77,
     236,   183,   182,   182,   180,   144,   181,   180,   209,    64,
     206,   207,   239,   203,   204,   239,    64,   188,   224,   180,
      40,   180,   180,   211,    64,   239,   238,   238,   180,   190,
     190,    69,    77,    77,    33,   125,   126,    33,    33,    33,
      38,    33,   180,   180,   165,   167,   167,   167,   168,   170,
     171,   150,   171,   150,   180,    64,   238,   238,   238,   238,
     238,   181,   238,   238,   239,   207,    44,   125,   126,   204,
      44,   125,    33,    77,   238,   180,   238,   238,    43,    64,
     122,   123,   212,   240,   238,    39,    77,    39,   182,   182,
      64,   180,   180,    77,    39,    38,   171,    38,   171,    38,
     238,    42,    37,    53,    54,    55,    56,    57,   150,   184,
      64,    69,   145,    12,    57,    64,   184,   185,   189,    64,
     188,    77,    40,    40,    42,   125,   126,    76,   180,   238,
      38,    38,    38,   238,   180,    38,    38,   180,    62,    63,
     145,   186,   187,   188,   126,    53,   150,    33,    38,    57,
     189,   150,    40,    33,   180,   240,   240,   180,    64,    69,
     180,    40,    39,    33,    36,    77,   180,   126,    35,   150,
      35,    39,   190,    39,    39,    39,    33,    38,   190,   187,
      38,   180,   184,    35,   189,    39,   238,    42,    42,    39,
      38,   180,   181,   238,    35,    33,    33,    53,   126,   180,
      38
};

#if ! defined (YYSIZE_T) && defined (__SIZE_TYPE__)
# define YYSIZE_T __SIZE_TYPE__
#endif
#if ! defined (YYSIZE_T) && defined (size_t)
# define YYSIZE_T size_t
#endif
#if ! defined (YYSIZE_T)
# if defined (__STDC__) || defined (__cplusplus)
#  include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  define YYSIZE_T size_t
# endif
#endif
#if ! defined (YYSIZE_T)
# define YYSIZE_T unsigned int
#endif

#define yyerrok		(yyerrstatus = 0)
#define yyclearin	(yychar = YYEMPTY)
#define YYEMPTY		(-2)
#define YYEOF		0

#define YYACCEPT	goto yyacceptlab
#define YYABORT		goto yyabortlab
#define YYERROR		goto yyerrlab1

/* Like YYERROR except do call yyerror.  This remains here temporarily
   to ease the transition to the new meaning of YYERROR, for GCC.
   Once GCC version 2 has supplanted version 1, this can go.  */

#define YYFAIL		goto yyerrlab

#define YYRECOVERING()  (!!yyerrstatus)

#define YYBACKUP(Token, Value)					\
do								\
  if (yychar == YYEMPTY && yylen == 1)				\
    {								\
      yychar = (Token);						\
      yylval = (Value);						\
      yytoken = YYTRANSLATE (yychar);				\
      YYPOPSTACK;						\
      goto yybackup;						\
    }								\
  else								\
    { 								\
      yyerror ("syntax error: cannot back up");\
      YYERROR;							\
    }								\
while (0)

#define YYTERROR	1
#define YYERRCODE	256

/* YYLLOC_DEFAULT -- Compute the default location (before the actions
   are run).  */

#ifndef YYLLOC_DEFAULT
# define YYLLOC_DEFAULT(Current, Rhs, N)         \
  Current.first_line   = Rhs[1].first_line;      \
  Current.first_column = Rhs[1].first_column;    \
  Current.last_line    = Rhs[N].last_line;       \
  Current.last_column  = Rhs[N].last_column;
#endif

/* YYLEX -- calling `yylex' with the right arguments.  */

#ifdef YYLEX_PARAM
# define YYLEX yylex (YYLEX_PARAM)
#else
# define YYLEX yylex ()
#endif

/* Enable debugging if requested.  */
#if YYDEBUG

# ifndef YYFPRINTF
#  include <stdio.h> /* INFRINGES ON USER NAME SPACE */
#  define YYFPRINTF fprintf
# endif

# define YYDPRINTF(Args)			\
do {						\
  if (yydebug)					\
    YYFPRINTF Args;				\
} while (0)

# define YYDSYMPRINT(Args)			\
do {						\
  if (yydebug)					\
    yysymprint Args;				\
} while (0)

# define YYDSYMPRINTF(Title, Token, Value, Location)		\
do {								\
  if (yydebug)							\
    {								\
      YYFPRINTF (stderr, "%s ", Title);				\
      yysymprint (stderr, 					\
                  Token, Value);	\
      YYFPRINTF (stderr, "\n");					\
    }								\
} while (0)

/*------------------------------------------------------------------.
| yy_stack_print -- Print the state stack from its BOTTOM up to its |
| TOP (cinluded).                                                   |
`------------------------------------------------------------------*/

#if defined (__STDC__) || defined (__cplusplus)
static void
yy_stack_print (short *bottom, short *top)
#else
static void
yy_stack_print (bottom, top)
    short *bottom;
    short *top;
#endif
{
  YYFPRINTF (stderr, "Stack now");
  for (/* Nothing. */; bottom <= top; ++bottom)
    YYFPRINTF (stderr, " %d", *bottom);
  YYFPRINTF (stderr, "\n");
}

# define YY_STACK_PRINT(Bottom, Top)				\
do {								\
  if (yydebug)							\
    yy_stack_print ((Bottom), (Top));				\
} while (0)


/*------------------------------------------------.
| Report that the YYRULE is going to be reduced.  |
`------------------------------------------------*/

#if defined (__STDC__) || defined (__cplusplus)
static void
yy_reduce_print (int yyrule)
#else
static void
yy_reduce_print (yyrule)
    int yyrule;
#endif
{
  int yyi;
  unsigned int yylineno = yyrline[yyrule];
  YYFPRINTF (stderr, "Reducing stack by rule %d (line %u), ",
             yyrule - 1, yylineno);
  /* Print the symbols being reduced, and their result.  */
  for (yyi = yyprhs[yyrule]; 0 <= yyrhs[yyi]; yyi++)
    YYFPRINTF (stderr, "%s ", yytname [yyrhs[yyi]]);
  YYFPRINTF (stderr, "-> %s\n", yytname [yyr1[yyrule]]);
}

# define YY_REDUCE_PRINT(Rule)		\
do {					\
  if (yydebug)				\
    yy_reduce_print (Rule);		\
} while (0)

/* Nonzero means print parse trace.  It is left uninitialized so that
   multiple parsers can coexist.  */
int yydebug;
#else /* !YYDEBUG */
# define YYDPRINTF(Args)
# define YYDSYMPRINT(Args)
# define YYDSYMPRINTF(Title, Token, Value, Location)
# define YY_STACK_PRINT(Bottom, Top)
# define YY_REDUCE_PRINT(Rule)
#endif /* !YYDEBUG */


/* YYINITDEPTH -- initial size of the parser's stacks.  */
#ifndef	YYINITDEPTH
# define YYINITDEPTH 200
#endif

/* YYMAXDEPTH -- maximum size the stacks can grow to (effective only
   if the built-in stack extension method is used).

   Do not make this value too large; the results are undefined if
   SIZE_MAX < YYSTACK_BYTES (YYMAXDEPTH)
   evaluated with infinite-precision integer arithmetic.  */

#if YYMAXDEPTH == 0
# undef YYMAXDEPTH
#endif

#ifndef YYMAXDEPTH
# define YYMAXDEPTH 10000
#endif



#if YYERROR_VERBOSE

# ifndef yystrlen
#  if defined (__GLIBC__) && defined (_STRING_H)
#   define yystrlen strlen
#  else
/* Return the length of YYSTR.  */
static YYSIZE_T
#   if defined (__STDC__) || defined (__cplusplus)
yystrlen (const char *yystr)
#   else
yystrlen (yystr)
     const char *yystr;
#   endif
{
  register const char *yys = yystr;

  while (*yys++ != '\0')
    continue;

  return yys - yystr - 1;
}
#  endif
# endif

# ifndef yystpcpy
#  if defined (__GLIBC__) && defined (_STRING_H) && defined (_GNU_SOURCE)
#   define yystpcpy stpcpy
#  else
/* Copy YYSRC to YYDEST, returning the address of the terminating '\0' in
   YYDEST.  */
static char *
#   if defined (__STDC__) || defined (__cplusplus)
yystpcpy (char *yydest, const char *yysrc)
#   else
yystpcpy (yydest, yysrc)
     char *yydest;
     const char *yysrc;
#   endif
{
  register char *yyd = yydest;
  register const char *yys = yysrc;

  while ((*yyd++ = *yys++) != '\0')
    continue;

  return yyd - 1;
}
#  endif
# endif

#endif /* !YYERROR_VERBOSE */



#if YYDEBUG
/*--------------------------------.
| Print this symbol on YYOUTPUT.  |
`--------------------------------*/

#if defined (__STDC__) || defined (__cplusplus)
static void
yysymprint (FILE *yyoutput, int yytype, YYSTYPE *yyvaluep)
#else
static void
yysymprint (yyoutput, yytype, yyvaluep)
    FILE *yyoutput;
    int yytype;
    YYSTYPE *yyvaluep;
#endif
{
  /* Pacify ``unused variable'' warnings.  */
  (void) yyvaluep;

  if (yytype < YYNTOKENS)
    {
      YYFPRINTF (yyoutput, "token %s (", yytname[yytype]);
# ifdef YYPRINT
      YYPRINT (yyoutput, yytoknum[yytype], *yyvaluep);
# endif
    }
  else
    YYFPRINTF (yyoutput, "nterm %s (", yytname[yytype]);

  switch (yytype)
    {
      default:
        break;
    }
  YYFPRINTF (yyoutput, ")");
}

#endif /* ! YYDEBUG */
/*-----------------------------------------------.
| Release the memory associated to this symbol.  |
`-----------------------------------------------*/

#if defined (__STDC__) || defined (__cplusplus)
static void
yydestruct (int yytype, YYSTYPE *yyvaluep)
#else
static void
yydestruct (yytype, yyvaluep)
    int yytype;
    YYSTYPE *yyvaluep;
#endif
{
  /* Pacify ``unused variable'' warnings.  */
  (void) yyvaluep;

  switch (yytype)
    {

      default:
        break;
    }
}


/* Prevent warnings from -Wmissing-prototypes.  */

#ifdef YYPARSE_PARAM
# if defined (__STDC__) || defined (__cplusplus)
int yyparse (void *YYPARSE_PARAM);
# else
int yyparse ();
# endif
#else /* ! YYPARSE_PARAM */
#if defined (__STDC__) || defined (__cplusplus)
int yyparse (void);
#else
int yyparse ();
#endif
#endif /* ! YYPARSE_PARAM */



/* The lookahead symbol.  */
int yychar;

/* The semantic value of the lookahead symbol.  */
YYSTYPE yylval;

/* Number of syntax errors so far.  */
int yynerrs;



/*----------.
| yyparse.  |
`----------*/

#ifdef YYPARSE_PARAM
# if defined (__STDC__) || defined (__cplusplus)
int yyparse (void *YYPARSE_PARAM)
# else
int yyparse (YYPARSE_PARAM)
  void *YYPARSE_PARAM;
# endif
#else /* ! YYPARSE_PARAM */
#if defined (__STDC__) || defined (__cplusplus)
int
yyparse (void)
#else
int
yyparse ()

#endif
#endif
{
  
  register int yystate;
  register int yyn;
  int yyresult;
  /* Number of tokens to shift before error messages enabled.  */
  int yyerrstatus;
  /* Lookahead token as an internal (translated) token number.  */
  int yytoken = 0;

  /* Three stacks and their tools:
     `yyss': related to states,
     `yyvs': related to semantic values,
     `yyls': related to locations.

     Refer to the stacks thru separate pointers, to allow yyoverflow
     to reallocate them elsewhere.  */

  /* The state stack.  */
  short	yyssa[YYINITDEPTH];
  short *yyss = yyssa;
  register short *yyssp;

  /* The semantic value stack.  */
  YYSTYPE yyvsa[YYINITDEPTH];
  YYSTYPE *yyvs = yyvsa;
  register YYSTYPE *yyvsp;



#define YYPOPSTACK   (yyvsp--, yyssp--)

  YYSIZE_T yystacksize = YYINITDEPTH;

  /* The variables used to return semantic value and location from the
     action routines.  */
  YYSTYPE yyval;


  /* When reducing, the number of symbols on the RHS of the reduced
     rule.  */
  int yylen;

  YYDPRINTF ((stderr, "Starting parse\n"));

  yystate = 0;
  yyerrstatus = 0;
  yynerrs = 0;
  yychar = YYEMPTY;		/* Cause a token to be read.  */

  /* Initialize stack pointers.
     Waste one element of value and location stack
     so that they stay on the same level as the state stack.
     The wasted elements are never initialized.  */

  yyssp = yyss;
  yyvsp = yyvs;

  goto yysetstate;

/*------------------------------------------------------------.
| yynewstate -- Push a new state, which is found in yystate.  |
`------------------------------------------------------------*/
 yynewstate:
  /* In all cases, when you get here, the value and location stacks
     have just been pushed. so pushing a state here evens the stacks.
     */
  yyssp++;

 yysetstate:
  *yyssp = yystate;

  if (yyss + yystacksize - 1 <= yyssp)
    {
      /* Get the current used size of the three stacks, in elements.  */
      YYSIZE_T yysize = yyssp - yyss + 1;

#ifdef yyoverflow
      {
	/* Give user a chance to reallocate the stack. Use copies of
	   these so that the &'s don't force the real ones into
	   memory.  */
	YYSTYPE *yyvs1 = yyvs;
	short *yyss1 = yyss;


	/* Each stack pointer address is followed by the size of the
	   data in use in that stack, in bytes.  This used to be a
	   conditional around just the two extra args, but that might
	   be undefined if yyoverflow is a macro.  */
	yyoverflow ("parser stack overflow",
		    &yyss1, yysize * sizeof (*yyssp),
		    &yyvs1, yysize * sizeof (*yyvsp),

		    &yystacksize);

	yyss = yyss1;
	yyvs = yyvs1;
      }
#else /* no yyoverflow */
# ifndef YYSTACK_RELOCATE
      goto yyoverflowlab;
# else
      /* Extend the stack our own way.  */
      if (YYMAXDEPTH <= yystacksize)
	goto yyoverflowlab;
      yystacksize *= 2;
      if (YYMAXDEPTH < yystacksize)
	yystacksize = YYMAXDEPTH;

      {
	short *yyss1 = yyss;
	union yyalloc *yyptr =
	  (union yyalloc *) YYSTACK_ALLOC (YYSTACK_BYTES (yystacksize));
	if (! yyptr)
	  goto yyoverflowlab;
	YYSTACK_RELOCATE (yyss);
	YYSTACK_RELOCATE (yyvs);

#  undef YYSTACK_RELOCATE
	if (yyss1 != yyssa)
	  YYSTACK_FREE (yyss1);
      }
# endif
#endif /* no yyoverflow */

      yyssp = yyss + yysize - 1;
      yyvsp = yyvs + yysize - 1;


      YYDPRINTF ((stderr, "Stack size increased to %lu\n",
		  (unsigned long int) yystacksize));

      if (yyss + yystacksize - 1 <= yyssp)
	YYABORT;
    }

  YYDPRINTF ((stderr, "Entering state %d\n", yystate));

  goto yybackup;

/*-----------.
| yybackup.  |
`-----------*/
yybackup:

/* Do appropriate processing given the current state.  */
/* Read a lookahead token if we need one and don't already have one.  */
/* yyresume: */

  /* First try to decide what to do without reference to lookahead token.  */

  yyn = yypact[yystate];
  if (yyn == YYPACT_NINF)
    goto yydefault;

  /* Not known => get a lookahead token if don't already have one.  */

  /* YYCHAR is either YYEMPTY or YYEOF or a valid lookahead symbol.  */
  if (yychar == YYEMPTY)
    {
      YYDPRINTF ((stderr, "Reading a token: "));
      yychar = YYLEX;
    }

  if (yychar <= YYEOF)
    {
      yychar = yytoken = YYEOF;
      YYDPRINTF ((stderr, "Now at end of input.\n"));
    }
  else
    {
      yytoken = YYTRANSLATE (yychar);
      YYDSYMPRINTF ("Next token is", yytoken, &yylval, &yylloc);
    }

  /* If the proper action on seeing token YYTOKEN is to reduce or to
     detect an error, take that action.  */
  yyn += yytoken;
  if (yyn < 0 || YYLAST < yyn || yycheck[yyn] != yytoken)
    goto yydefault;
  yyn = yytable[yyn];
  if (yyn <= 0)
    {
      if (yyn == 0 || yyn == YYTABLE_NINF)
	goto yyerrlab;
      yyn = -yyn;
      goto yyreduce;
    }

  if (yyn == YYFINAL)
    YYACCEPT;

  /* Shift the lookahead token.  */
  YYDPRINTF ((stderr, "Shifting token %s, ", yytname[yytoken]));

  /* Discard the token being shifted unless it is eof.  */
  if (yychar != YYEOF)
    yychar = YYEMPTY;

  *++yyvsp = yylval;


  /* Count tokens shifted since error; after three, turn off error
     status.  */
  if (yyerrstatus)
    yyerrstatus--;

  yystate = yyn;
  goto yynewstate;


/*-----------------------------------------------------------.
| yydefault -- do the default action for the current state.  |
`-----------------------------------------------------------*/
yydefault:
  yyn = yydefact[yystate];
  if (yyn == 0)
    goto yyerrlab;
  goto yyreduce;


/*-----------------------------.
| yyreduce -- Do a reduction.  |
`-----------------------------*/
yyreduce:
  /* yyn is the number of a rule to reduce with.  */
  yylen = yyr2[yyn];

  /* If YYLEN is nonzero, implement the default value of the action:
     `$$ = $1'.

     Otherwise, the following line sets YYVAL to garbage.
     This behavior is undocumented and Bison
     users should not rely upon it.  Assigning to YYVAL
     unconditionally makes the parser a bit smaller, and it avoids a
     GCC warning that YYVAL may be used uninitialized.  */
  yyval = yyvsp[1-yylen];


  YY_REDUCE_PRINT (yyn);
  switch (yyn)
    {
        case 2:
#line 213 "grammar.y"
    { 
  if (PARSE_MODULES != parse_mode_flag) {
    yyerror("unexpected MODULE definition encountered during parsing");
    YYABORT;
  }
#if HAVE_MBP
  /* By default unset the flag that this is a game declaration */
  unset_mbp_game(options);
#endif
}
    break;

  case 3:
#line 224 "grammar.y"
    {
                  if (!opt_mbp_game(options)) {/*this is a usual SMV file*/
                    parsed_tree = yyvsp[0].node;
                  }
                  else {
                    /* this is a game (realizability problem) .
                       Return a Game with spec list on the left and a
                       module list on the right. This module list
                       contains two special modules (with names
                       PLAYER_1 and PLAYER_2) created from player
                       declarations.
                    */
                    parsed_tree = new_node(GAME,
                                           mbp_parser_spec_list,
                                           cons(mbp_parser_player_1,
                                                cons(mbp_parser_player_2, 
                                                     yyvsp[0].node)));
                  }
                }
    break;

  case 4:
#line 243 "grammar.y"
    {
                  if (PARSE_COMMAND != parse_mode_flag) {
		    yyerror("unexpected command encountered during parsing");
		    YYABORT;
		  }
                }
    break;

  case 5:
#line 249 "grammar.y"
    {parsed_tree = yyvsp[0].node;}
    break;

  case 6:
#line 250 "grammar.y"
    {
		  if (PARSE_LTL_EXPR != parse_mode_flag){
		    yyerror("unexpected expression encountered during parsing");
		    YYABORT;
		  }
                }
    break;

  case 7:
#line 256 "grammar.y"
    {parsed_tree = yyvsp[0].node;}
    break;

  case 9:
#line 266 "grammar.y"
    { yyval.node = yyvsp[0].node; }
    break;

  case 11:
#line 270 "grammar.y"
    { yyval.node = yyvsp[0].node; }
    break;

  case 12:
#line 272 "grammar.y"
    {node_int_setcar(yyvsp[0].node, -(node_get_int(yyvsp[0].node))); yyval.node = yyvsp[0].node;}
    break;

  case 17:
#line 286 "grammar.y"
    {yyval.node = new_lined_node(TWODOTS, yyvsp[-2].node, yyvsp[0].node, yyvsp[-1].lineno);}
    break;

  case 22:
#line 294 "grammar.y"
    {
#if HAVE_MATHSAT 
                 yyval.node = yyvsp[0].node;
#else
                 yyerror("fractional constants are not supported.");
                 YYABORT;
#endif
               }
    break;

  case 23:
#line 303 "grammar.y"
    {
#if HAVE_MATHSAT 
                 yyval.node = yyvsp[0].node;
#else
                 yyerror("exponential constants are not supported.");
                 YYABORT;
#endif
               }
    break;

  case 24:
#line 312 "grammar.y"
    {
#if HAVE_MATHSAT 
                 yyval.node = yyvsp[0].node;
#else
                 yyerror("real constants are not supported.");
                 YYABORT;
#endif
               }
    break;

  case 26:
#line 330 "grammar.y"
    { yyval.node = new_lined_node(UMINUS, yyvsp[0].node, Nil, yyvsp[-1].lineno); }
    break;

  case 28:
#line 332 "grammar.y"
    {yyval.node = new_node(SELF,Nil,Nil);}
    break;

  case 29:
#line 334 "grammar.y"
    {
		      if (ATOM != node_get_type(yyvsp[-2].node) && DOT != node_get_type(yyvsp[-2].node) && ARRAY != node_get_type(yyvsp[-2].node) && SELF != node_get_type(yyvsp[-2].node)) {
			yyerror_lined("incorrect DOT expression", yyvsp[-1].lineno);
		        YYABORT;
		      }
		      yyval.node = new_lined_node(DOT, yyvsp[-2].node, yyvsp[0].node, yyvsp[-1].lineno) ;
		    }
    break;

  case 30:
#line 342 "grammar.y"
    { 
		      if (ATOM != node_get_type(yyvsp[-2].node) && DOT != node_get_type(yyvsp[-2].node) && ARRAY != node_get_type(yyvsp[-2].node) && SELF != node_get_type(yyvsp[-2].node)) {
			yyerror_lined("incorrect DOT expression", yyvsp[-1].lineno);
			YYABORT;
		      }
		      yyval.node = new_lined_node(DOT, yyvsp[-2].node, yyvsp[0].node, 2) ;
		    }
    break;

  case 31:
#line 350 "grammar.y"
    { 
		       if (ATOM != node_get_type(yyvsp[-3].node) && DOT != node_get_type(yyvsp[-3].node) && ARRAY != node_get_type(yyvsp[-3].node) && SELF != node_get_type(yyvsp[-3].node)) {
			yyerror_lined("incorrect ARRAY expression", yyvsp[-2].lineno);
			YYABORT;
		       }
		       yyval.node = new_lined_node(ARRAY, yyvsp[-3].node, yyvsp[-1].node, yyvsp[-2].lineno);
		     }
    break;

  case 32:
#line 358 "grammar.y"
    { 
			yyval.node = new_lined_node(BIT_SELECTION, yyvsp[-5].node, 
			               new_lined_node(COLON, yyvsp[-3].node, yyvsp[-1].node, yyvsp[-2].lineno), yyvsp[-4].lineno);
		       }
    break;

  case 33:
#line 362 "grammar.y"
    { yyval.node = yyvsp[-1].node; }
    break;

  case 34:
#line 363 "grammar.y"
    { yyval.node = new_lined_node(NOT, yyvsp[0].node, Nil, yyvsp[-1].lineno); }
    break;

  case 35:
#line 364 "grammar.y"
    { yyval.node = new_lined_node(CAST_BOOL, yyvsp[-1].node, Nil, yyvsp[-3].lineno); }
    break;

  case 36:
#line 365 "grammar.y"
    { yyval.node = new_lined_node(CAST_WORD1, yyvsp[-1].node, Nil, yyvsp[-3].lineno); }
    break;

  case 37:
#line 366 "grammar.y"
    { yyval.node = new_lined_node(NEXT, yyvsp[-1].node, Nil, yyvsp[-3].lineno); }
    break;

  case 38:
#line 367 "grammar.y"
    { yyval.node = yyvsp[-1].node; }
    break;

  case 39:
#line 371 "grammar.y"
    { yyval.node = new_lined_node(WAREAD, yyvsp[-3].node, yyvsp[-1].node, yyvsp[-5].lineno); }
    break;

  case 40:
#line 374 "grammar.y"
    { yyval.node = new_lined_node(WAWRITE, yyvsp[-5].node, new_node(WAWRITE, yyvsp[-3].node, yyvsp[-1].node), yyvsp[-6].lineno); }
    break;

  case 41:
#line 380 "grammar.y"
    { yyval.node = new_node(CASE, yyvsp[0].node, failure_make("case conditions are not exhaustive", FAILURE_CASE_NOT_EXHAUSTIVE, yylineno));}
    break;

  case 42:
#line 381 "grammar.y"
    { yyval.node = new_node(CASE, yyvsp[-1].node, yyvsp[0].node); }
    break;

  case 43:
#line 386 "grammar.y"
    {yyval.node = new_lined_node(COLON, yyvsp[-3].node, yyvsp[-1].node, yyvsp[-2].lineno);}
    break;

  case 45:
#line 391 "grammar.y"
    { yyval.node = new_lined_node(CONCATENATION, yyvsp[-2].node, yyvsp[0].node, yyvsp[-1].lineno); }
    break;

  case 47:
#line 396 "grammar.y"
    { yyval.node = new_lined_node(TIMES, yyvsp[-2].node, yyvsp[0].node, yyvsp[-1].lineno); }
    break;

  case 48:
#line 397 "grammar.y"
    { yyval.node = new_lined_node(DIVIDE, yyvsp[-2].node, yyvsp[0].node, yyvsp[-1].lineno); }
    break;

  case 50:
#line 402 "grammar.y"
    { yyval.node = new_lined_node(PLUS, yyvsp[-2].node, yyvsp[0].node, yyvsp[-1].lineno); }
    break;

  case 51:
#line 403 "grammar.y"
    { yyval.node = new_lined_node(MINUS, yyvsp[-2].node, yyvsp[0].node, yyvsp[-1].lineno); }
    break;

  case 53:
#line 408 "grammar.y"
    { yyval.node = new_lined_node(MOD, yyvsp[-2].node, yyvsp[0].node, yyvsp[-1].lineno); }
    break;

  case 55:
#line 412 "grammar.y"
    { yyval.node = new_lined_node(LSHIFT, yyvsp[-2].node, yyvsp[0].node, yyvsp[-1].lineno); }
    break;

  case 56:
#line 413 "grammar.y"
    { yyval.node = new_lined_node(RSHIFT, yyvsp[-2].node, yyvsp[0].node, yyvsp[-1].lineno); }
    break;

  case 59:
#line 421 "grammar.y"
    { yyval.node = yyvsp[-1].node; }
    break;

  case 61:
#line 425 "grammar.y"
    {yyval.node = new_lined_node(UNION, yyvsp[-2].node, yyvsp[0].node, yyvsp[-1].lineno);}
    break;

  case 63:
#line 430 "grammar.y"
    { yyval.node = new_lined_node(UNION, yyvsp[-2].node, yyvsp[0].node, yyvsp[-1].lineno); }
    break;

  case 65:
#line 434 "grammar.y"
    { yyval.node = new_lined_node(SETIN, yyvsp[-2].node, yyvsp[0].node, yyvsp[-1].lineno); }
    break;

  case 67:
#line 439 "grammar.y"
    { yyval.node = new_lined_node(EQUAL, yyvsp[-2].node, yyvsp[0].node, yyvsp[-1].lineno); }
    break;

  case 68:
#line 440 "grammar.y"
    { yyval.node = new_lined_node(NOTEQUAL, yyvsp[-2].node, yyvsp[0].node, yyvsp[-1].lineno); }
    break;

  case 69:
#line 441 "grammar.y"
    { yyval.node = new_lined_node(LT, yyvsp[-2].node, yyvsp[0].node, yyvsp[-1].lineno); }
    break;

  case 70:
#line 442 "grammar.y"
    { yyval.node = new_lined_node(GT, yyvsp[-2].node, yyvsp[0].node, yyvsp[-1].lineno); }
    break;

  case 71:
#line 443 "grammar.y"
    { yyval.node = new_lined_node(LE, yyvsp[-2].node, yyvsp[0].node, yyvsp[-1].lineno); }
    break;

  case 72:
#line 444 "grammar.y"
    { yyval.node = new_lined_node(GE, yyvsp[-2].node, yyvsp[0].node, yyvsp[-1].lineno); }
    break;

  case 75:
#line 452 "grammar.y"
    { yyval.node = new_lined_node(EX, yyvsp[0].node, Nil, yyvsp[-1].lineno); }
    break;

  case 76:
#line 453 "grammar.y"
    { yyval.node = new_lined_node(AX, yyvsp[0].node, Nil, yyvsp[-1].lineno); }
    break;

  case 77:
#line 454 "grammar.y"
    { yyval.node = new_lined_node(EF, yyvsp[0].node, Nil, yyvsp[-1].lineno); }
    break;

  case 78:
#line 455 "grammar.y"
    { yyval.node = new_lined_node(AF, yyvsp[0].node, Nil, yyvsp[-1].lineno); }
    break;

  case 79:
#line 456 "grammar.y"
    { yyval.node = new_lined_node(EG, yyvsp[0].node, Nil, yyvsp[-1].lineno); }
    break;

  case 80:
#line 457 "grammar.y"
    { yyval.node = new_lined_node(AG, yyvsp[0].node, Nil, yyvsp[-1].lineno); }
    break;

  case 81:
#line 459 "grammar.y"
    { yyval.node = new_lined_node(AU, yyvsp[-3].node, yyvsp[-1].node, yyvsp[-5].lineno); }
    break;

  case 82:
#line 461 "grammar.y"
    { yyval.node = new_lined_node(EU, yyvsp[-3].node, yyvsp[-1].node, yyvsp[-5].lineno); }
    break;

  case 83:
#line 463 "grammar.y"
    { yyval.node = new_lined_node(ABU, new_lined_node(AU, yyvsp[-4].node, yyvsp[-1].node, yyvsp[-6].lineno), yyvsp[-2].node, yyvsp[-6].lineno); }
    break;

  case 84:
#line 465 "grammar.y"
    { yyval.node = new_lined_node(EBU, new_lined_node(EU, yyvsp[-4].node, yyvsp[-1].node, yyvsp[-6].lineno), yyvsp[-2].node, yyvsp[-6].lineno); }
    break;

  case 85:
#line 466 "grammar.y"
    { yyval.node = new_lined_node(EBF, yyvsp[0].node, yyvsp[-1].node, yyvsp[-2].lineno); }
    break;

  case 86:
#line 467 "grammar.y"
    { yyval.node = new_lined_node(ABF, yyvsp[0].node, yyvsp[-1].node, yyvsp[-2].lineno); }
    break;

  case 87:
#line 468 "grammar.y"
    { yyval.node = new_lined_node(EBG, yyvsp[0].node, yyvsp[-1].node, yyvsp[-2].lineno); }
    break;

  case 88:
#line 469 "grammar.y"
    { yyval.node = new_lined_node(ABG, yyvsp[0].node, yyvsp[-1].node, yyvsp[-2].lineno); }
    break;

  case 89:
#line 472 "grammar.y"
    { yyval.node = new_lined_node(NOT, yyvsp[0].node, Nil, yyvsp[-1].lineno); }
    break;

  case 91:
#line 479 "grammar.y"
    { yyval.node = new_lined_node(AND, yyvsp[-2].node, yyvsp[0].node, yyvsp[-1].lineno); }
    break;

  case 93:
#line 483 "grammar.y"
    { yyval.node = new_lined_node(OR,yyvsp[-2].node, yyvsp[0].node, yyvsp[-1].lineno); }
    break;

  case 94:
#line 484 "grammar.y"
    { yyval.node = new_lined_node(XOR,yyvsp[-2].node, yyvsp[0].node, yyvsp[-1].lineno); }
    break;

  case 95:
#line 485 "grammar.y"
    { yyval.node = new_lined_node(XNOR,yyvsp[-2].node, yyvsp[0].node, yyvsp[-1].lineno); }
    break;

  case 97:
#line 489 "grammar.y"
    { yyval.node = new_lined_node(IFF, yyvsp[-2].node, yyvsp[0].node, yyvsp[-1].lineno); }
    break;

  case 99:
#line 494 "grammar.y"
    { yyval.node = new_lined_node(IMPLIES, yyvsp[-2].node, yyvsp[0].node, yyvsp[-1].lineno); }
    break;

  case 103:
#line 507 "grammar.y"
    {yyval.node = new_lined_node(OP_NEXT, yyvsp[0].node, Nil, yyvsp[-1].lineno);}
    break;

  case 104:
#line 508 "grammar.y"
    {yyval.node = new_lined_node(OP_PREC, yyvsp[0].node, Nil, yyvsp[-1].lineno);}
    break;

  case 105:
#line 509 "grammar.y"
    {yyval.node = new_lined_node(OP_NOTPRECNOT, yyvsp[0].node, Nil, yyvsp[-1].lineno);}
    break;

  case 106:
#line 510 "grammar.y"
    {yyval.node = new_lined_node(OP_GLOBAL, yyvsp[0].node, Nil, yyvsp[-1].lineno);}
    break;

  case 107:
#line 511 "grammar.y"
    {yyval.node = new_lined_node(OP_HISTORICAL, yyvsp[0].node, Nil, yyvsp[-1].lineno);}
    break;

  case 108:
#line 512 "grammar.y"
    {yyval.node = new_lined_node(OP_FUTURE, yyvsp[0].node, Nil, yyvsp[-1].lineno);}
    break;

  case 109:
#line 513 "grammar.y"
    {yyval.node = new_lined_node(OP_ONCE, yyvsp[0].node, Nil, yyvsp[-1].lineno);}
    break;

  case 110:
#line 515 "grammar.y"
    { yyval.node = new_lined_node(NOT, yyvsp[0].node, Nil, yyvsp[-1].lineno); }
    break;

  case 112:
#line 522 "grammar.y"
    {yyval.node = new_lined_node(UNTIL, yyvsp[-2].node, yyvsp[0].node, yyvsp[-1].lineno);}
    break;

  case 113:
#line 524 "grammar.y"
    {yyval.node = new_lined_node(SINCE, yyvsp[-2].node, yyvsp[0].node, yyvsp[-1].lineno);}
    break;

  case 114:
#line 526 "grammar.y"
    {yyval.node = new_lined_node(NOT, 
                           new_lined_node(UNTIL,
			     new_lined_node(NOT, yyvsp[-2].node, Nil, node_get_lineno(yyvsp[-2].node)), 
			     new_lined_node(NOT, yyvsp[0].node, Nil, node_get_lineno(yyvsp[0].node)),
			     yyvsp[-1].lineno), Nil, yyvsp[-1].lineno);
		  }
    break;

  case 115:
#line 533 "grammar.y"
    {yyval.node = new_lined_node(NOT,
			  new_lined_node(SINCE,
			      new_lined_node(NOT, yyvsp[-2].node, Nil, node_get_lineno(yyvsp[-2].node)),
			      new_lined_node(NOT, yyvsp[0].node, Nil, node_get_lineno(yyvsp[0].node)),
			      yyvsp[-1].lineno), Nil, yyvsp[-1].lineno);
		  }
    break;

  case 117:
#line 543 "grammar.y"
    { yyval.node = new_lined_node(AND, yyvsp[-2].node, yyvsp[0].node, yyvsp[-1].lineno); }
    break;

  case 119:
#line 548 "grammar.y"
    { yyval.node = new_lined_node(OR,yyvsp[-2].node, yyvsp[0].node, yyvsp[-1].lineno); }
    break;

  case 120:
#line 549 "grammar.y"
    { yyval.node = new_lined_node(XOR,yyvsp[-2].node, yyvsp[0].node, yyvsp[-1].lineno); }
    break;

  case 121:
#line 550 "grammar.y"
    { yyval.node = new_lined_node(XNOR,yyvsp[-2].node, yyvsp[0].node, yyvsp[-1].lineno); }
    break;

  case 123:
#line 555 "grammar.y"
    { yyval.node = new_lined_node(IFF, yyvsp[-2].node, yyvsp[0].node, yyvsp[-1].lineno); }
    break;

  case 125:
#line 560 "grammar.y"
    { yyval.node = new_lined_node(IMPLIES, yyvsp[-2].node, yyvsp[0].node, yyvsp[-1].lineno); }
    break;

  case 127:
#line 569 "grammar.y"
    {if (!isCorrectExp(yyval.node, EXP_SIMPLE)) YYABORT;}
    break;

  case 128:
#line 572 "grammar.y"
    {if (!isCorrectExp(yyval.node, EXP_NEXT)) YYABORT;}
    break;

  case 129:
#line 575 "grammar.y"
    {if (!isCorrectExp(yyval.node, EXP_CTL)) YYABORT;}
    break;

  case 130:
#line 578 "grammar.y"
    {if (!isCorrectExp(yyval.node, EXP_LTL)) YYABORT;}
    break;

  case 131:
#line 588 "grammar.y"
    {yyval.node = new_node(BOOLEAN, Nil, Nil);}
    break;

  case 132:
#line 589 "grammar.y"
    {
#if HAVE_MATHSAT 
                yyval.node = new_node(INTEGER, Nil, Nil);
#else
                yyerror("given token is not supported.");
                YYABORT;
#endif
              }
    break;

  case 133:
#line 597 "grammar.y"
    {
#if HAVE_MATHSAT 
                yyval.node = new_node(REAL, Nil, Nil);
#else
                yyerror("given token is not supported.");
                YYABORT;
#endif 
}
    break;

  case 134:
#line 606 "grammar.y"
    {yyval.node = new_lined_node(WORD, yyvsp[-1].node, Nil, yyvsp[-3].lineno);}
    break;

  case 136:
#line 609 "grammar.y"
    {yyval.node = new_lined_node(SCALAR, yyvsp[-1].node, Nil, yyvsp[-2].lineno);}
    break;

  case 137:
#line 611 "grammar.y"
    {yyval.node = new_lined_node(WORDARRAY, yyvsp[-6].node, yyvsp[-1].node, yyvsp[-9].lineno);}
    break;

  case 138:
#line 613 "grammar.y"
    {yyval.node = new_lined_node(ARRAY, yyvsp[-2].node, yyvsp[0].node, yyvsp[-3].lineno);}
    break;

  case 141:
#line 620 "grammar.y"
    {yyval.node = new_lined_node(PROCESS, yyvsp[0].node, Nil, yyvsp[-1].lineno);}
    break;

  case 142:
#line 623 "grammar.y"
    {yyval.node = cons(find_atom(yyvsp[0].node), Nil);}
    break;

  case 143:
#line 624 "grammar.y"
    {yyval.node = cons(find_atom(yyvsp[0].node), yyvsp[-2].node);}
    break;

  case 149:
#line 634 "grammar.y"
    {yyval.node = new_lined_node(DOT, yyvsp[-2].node, yyvsp[0].node, yyvsp[-1].lineno);}
    break;

  case 150:
#line 637 "grammar.y"
    {yyval.node = new_node(MODTYPE, yyvsp[0].node, Nil);}
    break;

  case 151:
#line 638 "grammar.y"
    {yyval.node = new_node(MODTYPE, yyvsp[-2].node, Nil);}
    break;

  case 152:
#line 640 "grammar.y"
    {yyval.node = new_lined_node(MODTYPE, yyvsp[-3].node, yyvsp[-1].node, node_get_lineno(yyvsp[-3].node));}
    break;

  case 153:
#line 642 "grammar.y"
    {yyval.node = new_lined_node(ARRAY, yyvsp[-2].node, yyvsp[0].node, yyvsp[-3].lineno);}
    break;

  case 154:
#line 646 "grammar.y"
    {yyval.node = cons(yyvsp[0].node,Nil);}
    break;

  case 155:
#line 647 "grammar.y"
    {yyval.node = cons(yyvsp[0].node, yyvsp[-2].node);}
    break;

  case 156:
#line 662 "grammar.y"
    {yyval.node = cons(yyvsp[0].node, Nil);}
    break;

  case 157:
#line 663 "grammar.y"
    {yyval.node = Nil;}
    break;

  case 158:
#line 664 "grammar.y"
    {yyval.node = cons(yyvsp[0].node, yyvsp[-1].node);}
    break;

  case 159:
#line 665 "grammar.y"
    {yyval.node = yyvsp[-1].node;}
    break;

  case 160:
#line 669 "grammar.y"
    {yyval.node = new_lined_node(MODULE, yyvsp[-1].node, yyvsp[0].node, yyvsp[-2].lineno);}
    break;

  case 161:
#line 671 "grammar.y"
    {yyval.node = new_node(MODTYPE, yyvsp[0].node, Nil);}
    break;

  case 162:
#line 672 "grammar.y"
    {yyval.node = new_node(MODTYPE, yyvsp[-2].node, Nil);}
    break;

  case 163:
#line 674 "grammar.y"
    {yyval.node = new_node(MODTYPE, yyvsp[-3].node, yyvsp[-1].node);}
    break;

  case 164:
#line 676 "grammar.y"
    {yyval.node = cons(find_atom(yyvsp[0].node), Nil);}
    break;

  case 165:
#line 677 "grammar.y"
    {yyval.node = cons(find_atom(yyvsp[0].node), yyvsp[-2].node);}
    break;

  case 166:
#line 682 "grammar.y"
    {yyval.node = Nil;}
    break;

  case 167:
#line 683 "grammar.y"
    {yyval.node = cons(yyvsp[0].node, yyvsp[-1].node);}
    break;

  case 187:
#line 714 "grammar.y"
    {
		       /* check that the game is not redeclared */
		       if (opt_mbp_game(options)) {
			 yyerror_lined("redefinition of a GAME", yyvsp[-1].lineno);
		       }
		       /* set that this input file is a game declaration */
		       else {
#if !HAVE_MBP
             yyerror_lined("GAME declaration cannot be processes "
                           "because MBP package is not set up\n"
                           "Check --enable-mbp option of "
                           "the configure script\n", yyvsp[-1].lineno);
             YYABORT;
#else
             set_mbp_game(options);
#endif
		       }

		       mbp_parser_spec_list = yyvsp[0].node;
		       yyval.node = Nil;
		     }
    break;

  case 188:
#line 738 "grammar.y"
    {yyval.node=Nil;}
    break;

  case 189:
#line 740 "grammar.y"
    {if (Nil != yyvsp[-1].node) yyval.node = cons(yyvsp[-1].node,yyvsp[0].node); else yyval.node = yyvsp[0].node;}
    break;

  case 190:
#line 748 "grammar.y"
    {
		       /* a player definition is converted to a module
			  definitino with a special name. This is done
			  to simplify the further flattening
		       */
		       if (mbp_parser_player_1 != Nil) {
			 yyerror_lined("redefinition of a PLAYER_1", yyvsp[-1].lineno);
			 YYABORT;
		       }
		       mbp_parser_player_1 = new_lined_node(MODULE,
			   new_node(MODTYPE, 
			     new_node(ATOM,(node_ptr)find_string("PLAYER_1"),
				      Nil), Nil),  yyvsp[0].node, yyvsp[-1].lineno);
		       yyval.node=Nil;
		     }
    break;

  case 191:
#line 764 "grammar.y"
    {
		       /* a player definition is converted to a module
			  definitino with a special name. This is done
			  to simplify the further flattening
		       */
		       if (mbp_parser_player_2 != Nil) {
			 yyerror_lined("redefinition of a PLAYER_2", yyvsp[-1].lineno);
			 YYABORT;
		       }
		       mbp_parser_player_2 = new_lined_node(MODULE,
			   new_node(MODTYPE, 
			     new_node(ATOM,(node_ptr)find_string("PLAYER_2"),
				      Nil), Nil), yyvsp[0].node, yyvsp[-1].lineno);
		       yyval.node=Nil;
		     }
    break;

  case 198:
#line 795 "grammar.y"
    { yyval.node = Nil; }
    break;

  case 199:
#line 796 "grammar.y"
    { yyval.node = cons(yyvsp[0].node, yyvsp[-1].node); }
    break;

  case 206:
#line 818 "grammar.y"
    {yyval.node = new_lined_node(VAR, Nil, Nil, yyvsp[0].lineno);}
    break;

  case 207:
#line 819 "grammar.y"
    {yyval.node = new_lined_node(VAR, yyvsp[0].node, Nil, yyvsp[-1].lineno);}
    break;

  case 208:
#line 821 "grammar.y"
    {yyval.node = cons(yyvsp[0].node, Nil);}
    break;

  case 209:
#line 822 "grammar.y"
    {yyval.node = cons(yyvsp[0].node, yyvsp[-1].node);}
    break;

  case 210:
#line 824 "grammar.y"
    {yyval.node = new_lined_node(COLON, yyvsp[-3].node, yyvsp[-1].node, yyvsp[-2].lineno);}
    break;

  case 211:
#line 826 "grammar.y"
    {yyval.node = new_lined_node(IVAR, Nil, Nil, yyvsp[0].lineno);}
    break;

  case 212:
#line 827 "grammar.y"
    {yyval.node = new_lined_node(IVAR, yyvsp[0].node, Nil, yyvsp[-1].lineno);}
    break;

  case 213:
#line 829 "grammar.y"
    {yyval.node = cons(yyvsp[0].node, Nil);}
    break;

  case 214:
#line 830 "grammar.y"
    {yyval.node = cons(yyvsp[0].node, yyvsp[-1].node);}
    break;

  case 215:
#line 832 "grammar.y"
    {yyval.node = new_lined_node(COLON, yyvsp[-3].node, yyvsp[-1].node, yyvsp[-2].lineno);}
    break;

  case 216:
#line 836 "grammar.y"
    {yyval.node = new_lined_node(DEFINE, yyvsp[0].node, Nil, yyvsp[-1].lineno);}
    break;

  case 217:
#line 838 "grammar.y"
    {yyval.node = Nil;}
    break;

  case 218:
#line 839 "grammar.y"
    {yyval.node = cons(new_lined_node(EQDEF, yyvsp[-3].node, yyvsp[-1].node, yyvsp[-2].lineno), yyvsp[-4].node);}
    break;

  case 219:
#line 843 "grammar.y"
    {yyval.node = new_lined_node(ASSIGN, yyvsp[0].node, Nil, yyvsp[-1].lineno);}
    break;

  case 220:
#line 845 "grammar.y"
    {yyval.node = Nil;}
    break;

  case 221:
#line 846 "grammar.y"
    {yyval.node = new_node(AND, yyvsp[-1].node, yyvsp[0].node);}
    break;

  case 222:
#line 849 "grammar.y"
    {yyval.node = new_lined_node(EQDEF, yyvsp[-3].node, yyvsp[-1].node, yyvsp[-2].lineno);}
    break;

  case 223:
#line 851 "grammar.y"
    { yyval.node = new_lined_node(EQDEF,
					new_lined_node(SMALLINIT, yyvsp[-4].node, Nil, yyvsp[-6].lineno),
					yyvsp[-1].node, yyvsp[-2].lineno);
		  }
    break;

  case 224:
#line 856 "grammar.y"
    { yyval.node = new_lined_node(EQDEF,
					new_lined_node(NEXT, yyvsp[-4].node, Nil, yyvsp[-6].lineno),
					yyvsp[-1].node, yyvsp[-2].lineno);
		  }
    break;

  case 225:
#line 863 "grammar.y"
    {yyval.node = new_lined_node(INIT, yyvsp[-1].node, Nil, yyvsp[-2].lineno);}
    break;

  case 226:
#line 865 "grammar.y"
    {yyval.node = new_lined_node(INVAR, yyvsp[-1].node, Nil, yyvsp[-2].lineno);}
    break;

  case 227:
#line 867 "grammar.y"
    {yyval.node = new_lined_node(TRANS, yyvsp[-1].node, Nil, yyvsp[-2].lineno);}
    break;

  case 228:
#line 869 "grammar.y"
    {yyval.node = new_lined_node_agent(TRANS, yyvsp[-1].node, Nil, yyvsp[-3].lineno, yyvsp[-2].node);}
    break;

  case 229:
#line 874 "grammar.y"
    {yyval.node = new_lined_node(JUSTICE, yyvsp[-1].node, Nil, yyvsp[-2].lineno);}
    break;

  case 230:
#line 877 "grammar.y"
    {yyval.node = new_lined_node(JUSTICE, yyvsp[-1].node, Nil, yyvsp[-2].lineno);}
    break;

  case 231:
#line 882 "grammar.y"
    {yyval.node = new_lined_node(COMPASSION, cons(yyvsp[-4].node,yyvsp[-2].node), Nil, yyvsp[-6].lineno);}
    break;

  case 232:
#line 885 "grammar.y"
    {yyval.node = new_lined_node(INVARSPEC, yyvsp[-1].node, Nil, yyvsp[-2].lineno);}
    break;

  case 233:
#line 888 "grammar.y"
    {yyval.node = new_lined_node(SPEC, yyvsp[-1].node, Nil, yyvsp[-2].lineno);}
    break;

  case 234:
#line 889 "grammar.y"
    {yyval.node = new_lined_node(SPEC, yyvsp[-1].node, Nil, yyvsp[-2].lineno);}
    break;

  case 235:
#line 892 "grammar.y"
    {yyval.node = new_lined_node(LTLSPEC, yyvsp[-1].node, Nil, yyvsp[-2].lineno);}
    break;

  case 236:
#line 896 "grammar.y"
    { 
  if (nusmv_parse_psl() != 0) {
    YYABORT;
  }
  yyval.node = new_lined_node(PSLSPEC, psl_parsed_tree, Nil, yyvsp[0].lineno); 
}
    break;

  case 237:
#line 905 "grammar.y"
    {yyval.node = new_lined_node(CONSTANTS, yyvsp[-1].node, Nil, yyvsp[-2].lineno);}
    break;

  case 238:
#line 909 "grammar.y"
    {yyval.node = Nil;}
    break;

  case 239:
#line 910 "grammar.y"
    { yyval.node = cons(yyvsp[0].node, Nil);}
    break;

  case 240:
#line 911 "grammar.y"
    {yyval.node = cons(yyvsp[0].node, yyvsp[-2].node);}
    break;

  case 241:
#line 915 "grammar.y"
    { yyval.node = cons(yyvsp[0].node, Nil);}
    break;

  case 242:
#line 916 "grammar.y"
    { yyval.node = cons(yyvsp[0].node, yyvsp[-1].node);}
    break;

  case 243:
#line 920 "grammar.y"
    {
#if HAVE_MATHSAT 
                   yyval.node = new_lined_node(PRED, yyvsp[-1].node, 
                                       new_lined_node(NUMBER, 
                                                      (node_ptr) 0, Nil, yyvsp[-2].lineno), yyvsp[-2].lineno);
#else
                   yyerror("given token is not supported.");
                   YYABORT;
#endif                 
                 }
    break;

  case 244:
#line 931 "grammar.y"
    {
#if HAVE_MATHSAT 
                   yyval.node = new_lined_node(PRED, yyvsp[-1].node, yyvsp[-3].node, yyvsp[-5].lineno);
#else
                   yyerror("given token is not supported.");
                   YYABORT;
#endif                 
                 }
    break;

  case 245:
#line 942 "grammar.y"
    {
#if HAVE_MATHSAT 
                    yyval.node = new_lined_node(MIRROR, yyvsp[-1].node, Nil, yyvsp[-2].lineno);
#else
                    yyerror("given token is not supported.");
                    YYABORT;
#endif                 
                  }
    break;

  case 246:
#line 952 "grammar.y"
    {yyval.lineno=1;}
    break;

  case 247:
#line 953 "grammar.y"
    {yyval.lineno=2;}
    break;

  case 248:
#line 956 "grammar.y"
    {yyval.node = new_lined_node(REACHTARGET, NODE_FROM_INT(yyvsp[-2].lineno), yyvsp[-1].node, yyvsp[-3].lineno);}
    break;

  case 249:
#line 959 "grammar.y"
    {yyval.node = new_lined_node(AVOIDTARGET, NODE_FROM_INT(yyvsp[-2].lineno), yyvsp[-1].node, yyvsp[-3].lineno);}
    break;

  case 250:
#line 962 "grammar.y"
    {yyval.node = new_lined_node(REACHDEADLOCK, NODE_FROM_INT(yyvsp[-1].lineno), Nil, yyvsp[-2].lineno);}
    break;

  case 251:
#line 965 "grammar.y"
    {yyval.node = new_lined_node(AVOIDDEADLOCK, NODE_FROM_INT(yyvsp[-1].lineno), Nil, yyvsp[-2].lineno);}
    break;

  case 252:
#line 970 "grammar.y"
    {yyval.node = new_lined_node(BUCHIGAME, NODE_FROM_INT(yyvsp[-4].lineno),
					    new_lined_node(MBP_EXP_LIST,
							 reverse(yyvsp[-2].node), Nil, yyvsp[-3].lineno),
					    yyvsp[-5].lineno);}
    break;

  case 253:
#line 978 "grammar.y"
    {yyval.node = new_lined_node(GENREACTIVITY, NODE_FROM_INT(yyvsp[-8].lineno),
				          new_lined_node(MBP_TWO_EXP_LISTS,
						reverse(yyvsp[-6].node), reverse(yyvsp[-2].node), yyvsp[-4].lineno),
					    yyvsp[-9].lineno);}
    break;

  case 254:
#line 984 "grammar.y"
    {yyval.node = new_lined_node(COMPUTE, yyvsp[-1].node, Nil, yyvsp[-2].lineno);}
    break;

  case 255:
#line 988 "grammar.y"
    { yyval.node = new_lined_node(MINU, yyvsp[-3].node, yyvsp[-1].node, yyvsp[-5].lineno); }
    break;

  case 256:
#line 990 "grammar.y"
    { yyval.node = new_lined_node(MAXU, yyvsp[-3].node, yyvsp[-1].node, yyvsp[-5].lineno); }
    break;

  case 257:
#line 994 "grammar.y"
    {yyval.node = new_node(ISA, yyvsp[0].node, Nil);}
    break;

  case 259:
#line 998 "grammar.y"
    {}
    break;

  case 261:
#line 1007 "grammar.y"
    {yyval.node = new_node(DOT, yyvsp[-2].node, yyvsp[0].node);}
    break;

  case 262:
#line 1008 "grammar.y"
    {yyval.node = new_node(DOT, yyvsp[-2].node, yyvsp[0].node);}
    break;

  case 263:
#line 1009 "grammar.y"
    {yyval.node = new_node(ARRAY, yyvsp[-3].node, yyvsp[-1].node);}
    break;

  case 265:
#line 1013 "grammar.y"
    {yyval.node = new_node(SELF,Nil,Nil);}
    break;

  case 266:
#line 1014 "grammar.y"
    {yyval.node = new_node(DOT, yyvsp[-2].node, yyvsp[0].node);}
    break;

  case 267:
#line 1015 "grammar.y"
    {yyval.node = new_node(DOT, yyvsp[-2].node, yyvsp[0].node);}
    break;

  case 268:
#line 1016 "grammar.y"
    {yyval.node = new_node(ARRAY, yyvsp[-3].node, yyvsp[-1].node);}
    break;

  case 269:
#line 1024 "grammar.y"
    {yyval.node = yyvsp[0].node;}
    break;

  case 270:
#line 1025 "grammar.y"
    {return(1);}
    break;

  case 271:
#line 1026 "grammar.y"
    {return(1);}
    break;

  case 272:
#line 1030 "grammar.y"
    {yyval.node = new_lined_node(GOTO, yyvsp[-1].node, Nil, yyvsp[-2].lineno);}
    break;

  case 273:
#line 1032 "grammar.y"
    {yyval.node = new_lined_node(INIT, yyvsp[-1].node, Nil, yyvsp[-2].lineno);}
    break;

  case 274:
#line 1034 "grammar.y"
    {yyval.node = new_lined_node(JUSTICE, yyvsp[-1].node, Nil, yyvsp[-2].lineno);}
    break;

  case 275:
#line 1036 "grammar.y"
    {yyval.node = new_lined_node(TRANS, yyvsp[-1].node, Nil, yyvsp[-2].lineno);}
    break;

  case 276:
#line 1038 "grammar.y"
    {yyval.node = new_lined_node(CONSTRAINT, yyvsp[-1].node, Nil, yyvsp[-2].lineno);}
    break;

  case 277:
#line 1040 "grammar.y"
    {yyval.node = new_lined_node(SIMPWFF,
                             new_node(CONTEXT, Nil, yyvsp[-1].node), Nil, yyvsp[-2].lineno);
                 }
    break;

  case 278:
#line 1044 "grammar.y"
    {yyval.node = new_lined_node(SIMPWFF,
                       new_node(CONTEXT, context2maincontext(yyvsp[-1].node), yyvsp[-3].node), Nil, yyvsp[-4].lineno);
                 }
    break;

  case 279:
#line 1048 "grammar.y"
    {yyval.node = new_lined_node(CTLWFF,
                             new_node(CONTEXT, Nil, yyvsp[-1].node), Nil, yyvsp[-2].lineno);
                 }
    break;

  case 280:
#line 1052 "grammar.y"
    {yyval.node = new_lined_node(CTLWFF,
                       new_node(CONTEXT, context2maincontext(yyvsp[-1].node), yyvsp[-3].node), Nil, yyvsp[-4].lineno);
                 }
    break;

  case 281:
#line 1056 "grammar.y"
    {yyval.node = new_lined_node(LTLWFF,
                             new_node(CONTEXT, Nil, yyvsp[-1].node), Nil, yyvsp[-2].lineno);
                 }
    break;

  case 282:
#line 1060 "grammar.y"
    {yyval.node = new_lined_node(LTLWFF,
                       new_node(CONTEXT, context2maincontext(yyvsp[-1].node), yyvsp[-3].node), Nil, yyvsp[-4].lineno);
                 }
    break;

  case 283:
#line 1064 "grammar.y"
    {yyval.node = new_lined_node(COMPWFF,
                             new_node(CONTEXT, Nil, yyvsp[-1].node), Nil, yyvsp[-2].lineno);
                 }
    break;

  case 284:
#line 1068 "grammar.y"
    {yyval.node = new_lined_node(COMPWFF,
                       new_node(CONTEXT, context2maincontext(yyvsp[-1].node), yyvsp[-3].node), Nil, yyvsp[-4].lineno);
                 }
    break;

  case 285:
#line 1072 "grammar.y"
    {
#if HAVE_MATHSAT 
                  yyval.node = new_lined_node(PREDS_LIST, yyvsp[0].node, Nil, yyvsp[-1].lineno);
#else
                  yyerror("given token is not supported.");
                  YYABORT;
#endif
                }
    break;

  case 286:
#line 1083 "grammar.y"
    {yyval.node = find_atom(yyvsp[0].node);}
    break;

  case 287:
#line 1084 "grammar.y"
    {yyval.node = find_node(DOT, yyvsp[-2].node, yyvsp[0].node);}
    break;

  case 288:
#line 1085 "grammar.y"
    {yyval.node = find_node(ARRAY, yyvsp[-3].node, yyvsp[-1].node);}
    break;

  case 289:
#line 1088 "grammar.y"
    {yyval.node = (node_ptr)find_atom(yyvsp[0].node);}
    break;

  case 290:
#line 1089 "grammar.y"
    {yyval.node = find_node(DOT, yyvsp[-2].node, find_atom(yyvsp[0].node));}
    break;

  case 291:
#line 1092 "grammar.y"
    {parser_parse_real_unset(); }
    break;

  case 292:
#line 1094 "grammar.y"
    {
                     parser_parse_real_set(); 
                     yyval.node = find_node(DOT, yyvsp[-2].node, find_atom(yyvsp[0].node));
                   }
    break;


    }

/* Line 991 of yacc.c.  */
#line 3296 "grammar.c"

  yyvsp -= yylen;
  yyssp -= yylen;


  YY_STACK_PRINT (yyss, yyssp);

  *++yyvsp = yyval;


  /* Now `shift' the result of the reduction.  Determine what state
     that goes to, based on the state we popped back to and the rule
     number reduced by.  */

  yyn = yyr1[yyn];

  yystate = yypgoto[yyn - YYNTOKENS] + *yyssp;
  if (0 <= yystate && yystate <= YYLAST && yycheck[yystate] == *yyssp)
    yystate = yytable[yystate];
  else
    yystate = yydefgoto[yyn - YYNTOKENS];

  goto yynewstate;


/*------------------------------------.
| yyerrlab -- here on detecting error |
`------------------------------------*/
yyerrlab:
  /* If not already recovering from an error, report this error.  */
  if (!yyerrstatus)
    {
      ++yynerrs;
#if YYERROR_VERBOSE
      yyn = yypact[yystate];

      if (YYPACT_NINF < yyn && yyn < YYLAST)
	{
	  YYSIZE_T yysize = 0;
	  int yytype = YYTRANSLATE (yychar);
	  char *yymsg;
	  int yyx, yycount;

	  yycount = 0;
	  /* Start YYX at -YYN if negative to avoid negative indexes in
	     YYCHECK.  */
	  for (yyx = yyn < 0 ? -yyn : 0;
	       yyx < (int) (sizeof (yytname) / sizeof (char *)); yyx++)
	    if (yycheck[yyx + yyn] == yyx && yyx != YYTERROR)
	      yysize += yystrlen (yytname[yyx]) + 15, yycount++;
	  yysize += yystrlen ("syntax error, unexpected ") + 1;
	  yysize += yystrlen (yytname[yytype]);
	  yymsg = (char *) YYSTACK_ALLOC (yysize);
	  if (yymsg != 0)
	    {
	      char *yyp = yystpcpy (yymsg, "syntax error, unexpected ");
	      yyp = yystpcpy (yyp, yytname[yytype]);

	      if (yycount < 5)
		{
		  yycount = 0;
		  for (yyx = yyn < 0 ? -yyn : 0;
		       yyx < (int) (sizeof (yytname) / sizeof (char *));
		       yyx++)
		    if (yycheck[yyx + yyn] == yyx && yyx != YYTERROR)
		      {
			const char *yyq = ! yycount ? ", expecting " : " or ";
			yyp = yystpcpy (yyp, yyq);
			yyp = yystpcpy (yyp, yytname[yyx]);
			yycount++;
		      }
		}
	      yyerror (yymsg);
	      YYSTACK_FREE (yymsg);
	    }
	  else
	    yyerror ("syntax error; also virtual memory exhausted");
	}
      else
#endif /* YYERROR_VERBOSE */
	yyerror ("syntax error");
    }



  if (yyerrstatus == 3)
    {
      /* If just tried and failed to reuse lookahead token after an
	 error, discard it.  */

      /* Return failure if at end of input.  */
      if (yychar == YYEOF)
        {
	  /* Pop the error token.  */
          YYPOPSTACK;
	  /* Pop the rest of the stack.  */
	  while (yyss < yyssp)
	    {
	      YYDSYMPRINTF ("Error: popping", yystos[*yyssp], yyvsp, yylsp);
	      yydestruct (yystos[*yyssp], yyvsp);
	      YYPOPSTACK;
	    }
	  YYABORT;
        }

      YYDSYMPRINTF ("Error: discarding", yytoken, &yylval, &yylloc);
      yydestruct (yytoken, &yylval);
      yychar = YYEMPTY;

    }

  /* Else will try to reuse lookahead token after shifting the error
     token.  */
  goto yyerrlab2;


/*----------------------------------------------------.
| yyerrlab1 -- error raised explicitly by an action.  |
`----------------------------------------------------*/
yyerrlab1:

  /* Suppress GCC warning that yyerrlab1 is unused when no action
     invokes YYERROR.  */
#if defined (__GNUC_MINOR__) && 2093 <= (__GNUC__ * 1000 + __GNUC_MINOR__) \
    && !defined __cplusplus
  __attribute__ ((__unused__))
#endif


  goto yyerrlab2;


/*---------------------------------------------------------------.
| yyerrlab2 -- pop states until the error token can be shifted.  |
`---------------------------------------------------------------*/
yyerrlab2:
  yyerrstatus = 3;	/* Each real token shifted decrements this.  */

  for (;;)
    {
      yyn = yypact[yystate];
      if (yyn != YYPACT_NINF)
	{
	  yyn += YYTERROR;
	  if (0 <= yyn && yyn <= YYLAST && yycheck[yyn] == YYTERROR)
	    {
	      yyn = yytable[yyn];
	      if (0 < yyn)
		break;
	    }
	}

      /* Pop the current state because it cannot handle the error token.  */
      if (yyssp == yyss)
	YYABORT;

      YYDSYMPRINTF ("Error: popping", yystos[*yyssp], yyvsp, yylsp);
      yydestruct (yystos[yystate], yyvsp);
      yyvsp--;
      yystate = *--yyssp;

      YY_STACK_PRINT (yyss, yyssp);
    }

  if (yyn == YYFINAL)
    YYACCEPT;

  YYDPRINTF ((stderr, "Shifting error token, "));

  *++yyvsp = yylval;


  yystate = yyn;
  goto yynewstate;


/*-------------------------------------.
| yyacceptlab -- YYACCEPT comes here.  |
`-------------------------------------*/
yyacceptlab:
  yyresult = 0;
  goto yyreturn;

/*-----------------------------------.
| yyabortlab -- YYABORT comes here.  |
`-----------------------------------*/
yyabortlab:
  yyresult = 1;
  goto yyreturn;

#ifndef yyoverflow
/*----------------------------------------------.
| yyoverflowlab -- parser overflow comes here.  |
`----------------------------------------------*/
yyoverflowlab:
  yyerror ("parser stack overflow");
  yyresult = 2;
  /* Fall through.  */
#endif

yyreturn:
#ifndef yyoverflow
  if (yyss != yyssa)
    YYSTACK_FREE (yyss);
#endif
  return yyresult;
}


#line 1101 "grammar.y"



/* Additional source code */
/* outputs the current token with the provided string and then may terminate */
void yyerror(char *s)
{
    extern options_ptr options;
    extern char yytext[];
    
    start_parsing_err();
    fprintf(nusmv_stderr, "at token \"%s\": %s\n", yytext, s);
    if (opt_batch(options)) finish_parsing_err();
}

/* the same as yyerror, except at first it sets the line number and does
 not output the current token 
*/
void yyerror_lined(const char *s, int line)
{
    extern options_ptr options;
    extern int yylineno;

    /*set the line number */
    yylineno = line;

    start_parsing_err();
    fprintf(nusmv_stderr, ": %s\n", s);
    if (opt_batch(options)) finish_parsing_err();
}

int yywrap()
{
  return(1);
}

/* This function is used to build the internal structure of the
   context (e.g. module instance name) from the parse tree. The
   function is needed since with the grammar it is not possible/simple
   to build directly the desired structure. */
static node_ptr context2maincontext(node_ptr context) {
  switch(node_get_type(context)) {
  case ATOM:
    return find_node(DOT, Nil, find_atom(context));
  case DOT: {
    node_ptr t = context2maincontext(car(context));
    return find_node(DOT, t, find_atom(cdr(context)));
  }
  case ARRAY: {
    node_ptr t = context2maincontext(car(context));
    return find_node(ARRAY, t, find_atom(cdr(context)));
  }
  default:
    fprintf(nusmv_stderr, "context2maincontext: undefined token \"%d\"\n",
	    node_get_type(context));
    nusmv_assert(false);
  }
}

/* this functions checks that the expression is formed syntactically correct.
   Takes the expression to be checked, the expected kind of the 
   expression. Returns true if the expression is formed correctly, and 
   false otherwise.
   See enum EXP_KIND for more info about syntactic well-formedness.
*/
static boolean isCorrectExp(node_ptr exp, enum EXP_KIND expectedKind)
{
  switch(node_get_type(exp))
    {
      /* atomic expression (or thier operands have been checked earlier) */
    case FAILURE:
    case FALSEEXP:
    case TRUEEXP:
    case NUMBER:
    case NUMBER_WORD:
    case NUMBER_FRAC: 
    case NUMBER_REAL:
    case NUMBER_EXP:
    case TWODOTS:
    case DOT:
    case ATOM:
    case SELF:
    case ARRAY:
    case BIT_SELECTION:
      return true;

      /* unary operators incompatible with LTL and CTL operator */
    case CAST_BOOL:
    case CAST_WORD1:
      if (EXP_LTL == expectedKind || EXP_CTL == expectedKind) {
	return isCorrectExp(car(exp), EXP_SIMPLE);
      } 
      /* unary operators compatible with LTL and CTL operator */
    case NOT: 
    case UMINUS:
      return isCorrectExp(car(exp), expectedKind);
      
      /* binary opertors incompatible with LTL and CTL operator */
    case CASE: case COLON:
    case CONCATENATION: 
    case TIMES: case DIVIDE: case PLUS :case MINUS: case MOD: 
    case LSHIFT: case RSHIFT: case LROTATE: case RROTATE: 
    case WAREAD: case WAWRITE: /* AC ADDED THESE */
    case UNION: case SETIN: 
    case EQUAL: case NOTEQUAL: case LT: case GT: case LE: case GE: 
      if (EXP_LTL == expectedKind || EXP_CTL == expectedKind) {
	return isCorrectExp(car(exp), EXP_SIMPLE)
	  && isCorrectExp(cdr(exp), EXP_SIMPLE);
      } 
      /* binary opertors compatible LTL and CTL operator */
    case AND: case OR: case XOR: case XNOR: case IFF: case IMPLIES: 
      return isCorrectExp(car(exp), expectedKind)
	&& isCorrectExp(cdr(exp), expectedKind);

      /* next expression */
    case NEXT: 
      if (EXP_NEXT != expectedKind) {
	yyerror_lined("unexpected 'next' operator", node_get_lineno(exp));
	return false;
      }
      return isCorrectExp(car(exp), EXP_SIMPLE); /* NEXT cannot contain NEXT */

      /* CTL unary expressions */
    case EX: case AX: case EF: case AF: case EG: case AG: 
    case ABU: case EBU:
    case EBF: case ABF: case EBG: case ABG: 
      if (EXP_CTL != expectedKind) {
	yyerror_lined("unexpected CTL operator", node_get_lineno(exp));
	return false;
      }
      return isCorrectExp(car(exp), EXP_CTL); /* continue to check CTL */
      
      /* CTL binary expressions */
    case AU: case EU: 
      if (EXP_CTL != expectedKind) {
	yyerror_lined("unexpected CTL operator", node_get_lineno(exp));
	return false;
      }
      return isCorrectExp(car(exp), EXP_CTL)
	&& isCorrectExp(cdr(exp), EXP_CTL); /* continue to check CTL */


      /* LTL unary expressions */
    case OP_NEXT: case OP_PREC: case OP_NOTPRECNOT: case OP_GLOBAL:
    case OP_HISTORICAL: case OP_FUTURE: case OP_ONCE:
      if (EXP_LTL != expectedKind) {
	yyerror_lined("unexpected LTL operator", node_get_lineno(exp));
	return false;
      }
      return isCorrectExp(car(exp), EXP_LTL); /* continue to check LTL */


      /* LTL binary expressions */
    case UNTIL: case SINCE: 
      if (EXP_LTL != expectedKind) {
	yyerror_lined("unexpected LTL operator", node_get_lineno(exp));
	return false;
      }
      return isCorrectExp(car(exp), EXP_LTL)
	&& isCorrectExp(cdr(exp), EXP_LTL); /* continue to check LTL */

    default: nusmv_assert(false); /* unknown expression */
    }
  return false; /* should never be invoked */
}


static int nusmv_parse_psl() 
{
  int res;
  res = psl_yyparse();
  return res;  
}

