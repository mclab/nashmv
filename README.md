# README #

By suitably modifying the NuSMV model checker, NashMV implements the algorithms described in [SSS2009, FMCAD2008].

[SSS2009] F. Mari, I. Melatti, I. Salvo, E. Tronci, L. Alvisi, A. Clement, and H. Li. "Model Checking Coalition Nash Equilibria in MAD Distributed Systems." In Stabilization, Safety, and Security of Distributed Systems, 11th International Symposium, SSS 2009, Lyon, France, November 3-6, 2009. Proceedings, edited by R. Guerraoui and F. Petit, 531-546. Lecture Notes in Computer Science 5873. Springer, 2009. DOI: 10.1007/978-3-642-05118-0_37.

[FMCAD2008] F. Mari, I. Melatti, I. Salvo, E. Tronci, L. Alvisi, A. Clement, and H. Li. "Model Checking Nash Equilibria in MAD Distributed Systems." In FMCAD '08: Proceedings of the 2008 International Conference on Formal Methods in Computer-Aided Design, edited by A. Cimatti and R. Jones, 1-8. Piscataway, NJ, USA: IEEE Press, 2008. ISSN: 978-1-4244-2735-2. DOI: 10.1109/FMCAD.2008.ECP.16.


### Participants ###

* F. Mari
* I. Melatti
* I. Salvo
* E. Tronci